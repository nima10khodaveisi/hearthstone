package logger;

import java.io.*;
import java.util.Date;

public class Logger {
    private File file ;

    public Logger(String path) {
        file = new File(path) ;
    }

    public void appendEvent(String description) {
        Date eventDate = new Date() ;
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter) ;
            bufferedWriter.write('\n') ;
            bufferedWriter.write("at " + eventDate + " : " + description) ;
            bufferedWriter.close() ;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String read() {
        try {
            String res = "" ;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file)) ;
            while(true) {
                String str = bufferedReader.readLine() ;
                if(str == null)
                    break ;
                res += str ;
                res += '\n' ;
            }
            return res ;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null ;
    }
}
