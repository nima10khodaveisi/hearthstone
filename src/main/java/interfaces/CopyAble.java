package interfaces;

import Models.Cards.Card;

public interface CopyAble {
    public Card copy() ;
}
