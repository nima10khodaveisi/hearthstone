package interfaces;

public abstract class Request {
    public abstract void execute() ;
}
