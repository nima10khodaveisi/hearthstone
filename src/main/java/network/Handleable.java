package network;

import network.server.ClientHandler;
import org.json.simple.JSONObject;

public interface Handleable {
    public void handleMessage(ClientHandler clientHandler, String message) ;

    public void sendMessage(ClientHandler clientHandler, String message) ;

    public void sendMessage(ClientHandler clientHandler, JSONObject jsonObject) ;
}
