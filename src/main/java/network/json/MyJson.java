package network.json;

import org.json.simple.JSONObject;

public class MyJson {

    public static JSONObject create_json(Object... args) {
        JSONObject jsonObject = new JSONObject() ;
        for(int i = 0 ; i < args.length; i += 2) {
            jsonObject.put((String) args[i], args[i + 1]) ;
        }
        return jsonObject;
    }

}
