package network.client;

import World.World;
import graphic.getServerData.Controller;
import graphic.images.Images;
import graphic.login.Login;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class ClientMain extends Application {

    private String serverIP ;
    private int serverPort ;

    public static Stage stage ;

    public static void main(String[] args) {
        launch(args) ;
    }

    @Override
    public void start(Stage stage) throws Exception {
        ClientMain.stage = stage;
        stage.show();

        stage.setTitle("Hearthstone");
        Images.getInstance().init();


        try {
            URL url = Paths.get("./src/main/java/graphic/getServerData/getServerData.fxml").toUri().toURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url) ;
            Parent root = fxmlLoader.load() ;
            Controller controller = fxmlLoader.getController() ;
            controller.setClientMain(this) ;
            Scene scene = new Scene(root, 407, 208) ;
            stage.setScene(scene) ;
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setServerData(String serverIP, int serverPort) {
        this.serverIP = serverIP ;
        this.serverPort = serverPort ;
        try {
            new Client(serverIP, serverPort, stage).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
