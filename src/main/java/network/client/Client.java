package network.client;

import Player.Player;
import chatroom.client.ChatRoom;
import chatroom.client.group.Group;
import com.google.gson.Gson;
import game.players.gamePlayer.GamePlayer;
import graphic.collections.Collections;
import graphic.friendship.Friendship;
import graphic.game.GameGraphic;
import graphic.login.Login;
import graphic.menu.Menu;
import graphic.register.Register;
import graphic.selectPassiveDeck.Select;
import graphic.shop.Shop;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import network.Handleable;
import network.json.MyJson;
import network.server.ClientHandler;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.*;

public class Client extends Thread {

    private Socket socket;
    private Sender sender;
    private Receiver receiver;
    private Stage stage;
    private String username;

    private Socket gameSocket ;
    private Sender gameSender ;
    private Receiver gameReceiver ;
    private GameGraphic gameGraphic ;

    private Socket chatSocket ;
    private Sender chatSender ;
    private Receiver chatReceiver ;
    private Stage chatStage ;

    public Client(String serverIP, int serverPort, Stage stage) throws IOException {
        socket = new Socket(serverIP, serverPort);
        this.stage = stage;
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                if(username != null && !username.isEmpty()) {
                    sendMessage(MyJson.create_json(
                            "type", "logout",
                            "username", username).toJSONString());
                }

                System.exit(0);
            }
        });
    }

    @Override
    public void run() {
        try {
            sender = new Sender(new PrintStream(socket.getOutputStream()));
            receiver = new Receiver(socket.getInputStream());
            sender.start();
            receiver.start();
            Login.show(this, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleMessage(String message) {
        System.out.println("client handle message\n" + message);
        try {
            JSONObject json = (JSONObject) new JSONParser().parse(message);
            Method method = this.getClass().getMethod((String) json.get("type"), JSONObject.class);
            method.invoke(this, json);
        } catch (ParseException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println("==========================================");
    }

    public void sendMessage(String message) {
        System.out.println("client is sending message\n" + message);
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) new JSONParser().parse(message);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        sender.sendMessage(jsonObject.toJSONString());
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
    }

    public void sendMessage(JSONObject jsonObject) {
        sendMessage(jsonObject.toJSONString());
    }

    public void gameSendMessage(String message) {
        System.out.println("game client is sending message\n" + message);
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) new JSONParser().parse(message);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        gameSender.sendMessage(jsonObject.toJSONString());
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
    }

    public void gameSendMessage(JSONObject jsonObject) {
        gameSendMessage(jsonObject.toJSONString()) ;
    }


    public void chatSendMessage(String message) {
        System.out.println("chat client is sending message\n" + message);
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) new JSONParser().parse(message);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        chatSender.sendMessage(jsonObject.toJSONString());
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
    }

    public void chatSendMessage(JSONObject jsonObject) {
        chatSendMessage(jsonObject.toJSONString()) ;
    }




    //handle message classes


    public void login(JSONObject jsonObject) {
        String status = (String) jsonObject.get("status");
        if (status.equals("ok")) {
            //go to menu
            String username = (String) jsonObject.get("username");
            this.username = username;
            Menu.show(this, MyJson.create_json(
                    "username", username));
        } else {
            //show error in login
            Login.show(this, jsonObject);
        }
    }

    public void register(JSONObject jsonObject) {
        String status = (String) jsonObject.get("status");
        if (status.equals("ok")) {
            //nothing show menu in login
        } else {
            Register.show(this, jsonObject);
        }
    }

    public void menu(JSONObject json) {
        JSONObject jsonObject = new JSONObject() ;
        jsonObject.put("username", username);
        Menu.show(this, jsonObject) ;
    }

    public void logout(JSONObject jsonObject) {
        setUsername(null);
        Login.show(this, null);
    }

    public void collections(JSONObject jsonObject) {
        Collections.show(this, jsonObject) ;
    }

    public void shop(JSONObject jsonObject) {
        Shop.show(stage.getScene(), null, jsonObject, this) ;
    }

    public void selectPage(JSONObject jsonObject) {
        Select.show(this, jsonObject, null) ;
    }

    public void connectToGameServer(JSONObject jsonObject) {
        long portLL = (long) jsonObject.get("port") ;
        int port = (int) portLL ;
        String gameServerIP = (String) jsonObject.get("serverIP") ;
        boolean watcher = (boolean) jsonObject.get("watcher") ;
        try {
            gameSocket = new Socket(gameServerIP, port) ;
            gameSender = new Sender(new PrintStream(gameSocket.getOutputStream())) ;
            gameSender.start();
            gameReceiver = new Receiver(gameSocket.getInputStream()) ;
            gameReceiver.start();
            gameGraphic = new GameGraphic(this) ;
            if(!watcher) {
                Platform.runLater(() -> {
                    gameSender.sendMessage(MyJson.create_json(
                            "type", "recognize",
                            "username", username).toJSONString());
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateGameGraphic(JSONObject jsonObject) {
        Gson gson = new Gson() ;
        GamePlayer myPlayer = gson.fromJson(((String) jsonObject.get("myPlayer")), GamePlayer.class) ;
        GamePlayer enemyPlayer = gson.fromJson(((String) jsonObject.get("enemyPlayer")), GamePlayer.class) ;
        myPlayer.setEnemy(enemyPlayer) ;
        if(enemyPlayer != null)
            enemyPlayer.setEnemy(myPlayer) ;
        boolean isMyTurn = (boolean) jsonObject.get("isMyTurn") ;
        boolean watcher = (boolean) jsonObject.get("watcher") ;
        gameGraphic.updateGameGraphic(myPlayer, enemyPlayer, isMyTurn, watcher) ;
    }

    public void chatroom(JSONObject jsonObject) {
        Platform.runLater(() -> {
            String query = (String) jsonObject.get("query");
            switch (query) {
                case "join": {
                    long portLL = (long) jsonObject.get("port");
                    int port = (int) portLL;
                    String serverIP = (String) jsonObject.get("serverIP");
                    try {
                        chatSocket = new Socket(serverIP, port);
                        chatSender = new Sender(new PrintStream(chatSocket.getOutputStream()));
                        chatReceiver = new Receiver(chatSocket.getInputStream());
                        chatReceiver.start();
                        chatSender.start();

                        if (chatStage != null) {
                            chatStage.close();
                        }

                        chatStage = new Stage();
                        chatStage.setTitle("chat room");
                        chatStage.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "update": {
                    Gson gson = new Gson();
                    Group group = (Group) gson.fromJson((String) jsonObject.get("group"), Group.class);
                    ChatRoom.show(this, false, group) ;
                    break;
                }
            }
        }) ;
    }

    public void friendship(JSONObject jsonObject) {
        Friendship.show(this, jsonObject) ;
    }

    public class Receiver extends Thread {
        private InputStream inputStream;

        public Receiver(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            Scanner scanner = new Scanner(inputStream);

            while (!isInterrupted()) {
                if (!scanner.hasNext()) continue;
                String str = scanner.nextLine();
                if (str.equals("start message")) {
                    String message = "";
                    while (!str.equals("end message")) {
                        str = scanner.nextLine();
                        if (!str.equals("end message")) {
                            message += str;
                        }
                    }
                    Client.this.handleMessage(message);
                }
            }
        }
    }

    public class Sender extends Thread {
        private PrintStream printStream;
        private List<String> messages;

        public Sender(PrintStream printStream) {
            this.printStream = printStream;
            messages = new ArrayList<>();
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                String message = getMessage();
                if (message == null) continue;
                printStream.println("start message");
                printStream.println(message);
                printStream.println("end message");
            }
        }

        private String getMessage() {
            if (messages.isEmpty())
                return null;
            String message = messages.get(0);
            messages.remove(0);
            return message;
        }

        public void sendMessage(String message) {
            messages.add(message);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Sender getGameSender() {
        return gameSender;
    }

    public void setGameSender(Sender gameSender) {
        this.gameSender = gameSender;
    }

    public Receiver getGameReceiver() {
        return gameReceiver;
    }

    public void setGameReceiver(Receiver gameReceiver) {
        this.gameReceiver = gameReceiver;
    }

    public Stage getChatStage() {
        return chatStage;
    }

    public void setChatStage(Stage chatStage) {
        this.chatStage = chatStage;
    }
}
