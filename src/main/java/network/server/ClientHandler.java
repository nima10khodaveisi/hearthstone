package network.server;

import Player.Player;
import javafx.application.Platform;
import network.Handleable;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClientHandler extends Thread {

    private Handleable server;
    private Socket socket;
    private Receiver receiver;
    private Sender sender;
    private Player player;

    public ClientHandler(Handleable server, Socket socket) {
        this.server = server;
        this.socket = socket;
        try {
            receiver = new Receiver(socket.getInputStream());
            sender = new Sender(new PrintStream(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        receiver.start();
        sender.start();
    }

    public class Receiver extends Thread {
        private InputStream inputStream;

        public Receiver(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            Scanner scanner = new Scanner(inputStream);

            while (!isInterrupted()) {
                try {
                    if (!scanner.hasNext()) continue;
                    String str = scanner.nextLine();
                    if (str.equals("start message")) {
                        String message = "";
                        while (!str.equals("end message")) {
                            str = scanner.nextLine();
                            if (!str.equals("end message")) {
                                message += str;
                            }
                        }
                        server.handleMessage(ClientHandler.this, message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class Sender extends Thread {
        private PrintStream printStream;
        private List<String> messages;

        public Sender(PrintStream printStream) {
            this.printStream = printStream;
            messages = new ArrayList<>();
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                String message = getMessage();
                if (message == null) continue;
                printStream.println("start message");
                printStream.println(message);
                printStream.println("end message");
            }
        }

        private String getMessage() {
            if (messages.isEmpty())
                return null;
            String message = messages.get(0);
            messages.remove(0);
            return message;
        }

        public void sendMessage(String message) {
            messages.add(message);
        }
    }


    public Handleable getServer() {
        return server;
    }

    public void setServer(Handleable server) {
        this.server = server;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
        this.player.setOnline(true) ;
    }
}
