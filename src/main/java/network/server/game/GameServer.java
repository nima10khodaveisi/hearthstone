package network.server.game;

import Player.Player;
import World.World;
import game.GamePanel;
import game.GameState;
import network.Handleable;
import network.client.Client;
import network.json.MyJson;
import network.server.ClientHandler;
import network.server.Server;
import network.server.ServerMain;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class GameServer extends Thread implements Handleable {
    private ClientHandler player0;
    private ClientHandler player1;
    private ArrayList<ClientHandler> watchers;
    private ServerSocket serverSocket;
    private GameState gameState;
    private String type ;
    private int serverPort ;
    private GamePanel gamePanel ;

    public GameServer(String type, ClientHandler clientHandler0, ClientHandler clientHandler1, GamePanel gamePanel) {
        this.gamePanel = gamePanel ;
        this.type = type ;
        watchers = new ArrayList<>() ;
        try {
            serverSocket = new ServerSocket(0);
            this.serverPort = serverSocket.getLocalPort();
            String message = MyJson.create_json(
                    "type", "connectToGameServer",
                    "port", serverPort,
                    "serverIP", ServerMain.serverIP,
                    "watcher", false
            ).toJSONString();
            clientHandler0.getSender().sendMessage(message);
            clientHandler1.getSender().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();
                if (player0 == null) {
                    player0 = new ClientHandler(this, socket);
                    player0.start();
                } else if(player1 == null){
                    player1 = new ClientHandler(this, socket);
                    player1.start();
                } else {
                    ClientHandler watcher = new ClientHandler(this, socket) ;
                    watcher.start();
                    watchers.add(watcher) ;
                    gameState.addWatcher(watcher) ;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void startGame() {
        gameState = new GameState(this, type, player0, player1);
        gameState.start();
        gamePanel.setGameState(gameState) ;
    }

    @Override
    public void handleMessage(ClientHandler clientHandler, String message) {
        System.out.println("game server handle message :\n" + message);
        try {
            JSONObject json = (JSONObject) new JSONParser().parse(message);
            Method method = this.getClass().getMethod((String) json.get("type"), ClientHandler.class, JSONObject.class);
            method.invoke(this, clientHandler, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("===================================");
    }

    @Override
    public void sendMessage(ClientHandler clientHandler, String message) {
        System.out.println("game server is sending message\n" + message);
        clientHandler.getSender().sendMessage(message);
        System.out.println("++++++++++++++++++++++++++++++++");
    }

    @Override
    public void sendMessage(ClientHandler clientHandler, JSONObject jsonObject) {
        sendMessage(clientHandler, jsonObject.toJSONString());
    }

    public void recognize(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        Player gamePlayer = World.getWorld().getPlayerByUsername(username);
        clientHandler.setPlayer(gamePlayer);
        if((player1.getPlayer() != null && player0.getPlayer() != null) || type.equals("MultiPlayer"))
            startGame();
    }

    //handle actions
    public void attackAction(ClientHandler clientHandler, JSONObject jsonObject) {
        long attackerId = (long) jsonObject.get("attacker");
        long defenderId = (long) jsonObject.get("defender");
        gameState.attackAction((int) attackerId, (int) defenderId);
    }

    public void cardAction(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        String className = (String) jsonObject.get("actionCard");
        long charcterId = (long) jsonObject.get("character");
        gameState.selectCardAction(username, className, (int) charcterId);
    }

    public void summonCard(ClientHandler clientHandler, JSONObject jsonObject) {
        long ind = (long) jsonObject.get("ind");
        double dropX = (double) jsonObject.get("dropX");
        gameState.summonCard(clientHandler.getPlayer().getUsername(), (int) ind, dropX);
    }

    public void endTurn(ClientHandler clientHandler, JSONObject jsonObject) {
        gameState.start_turn();
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }
}
