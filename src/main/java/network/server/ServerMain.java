package network.server;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class ServerMain extends Application{
    public static String serverIP = "localhost" ;
    public static int serverPort = 8000 ;

    public static void main(String[] args) throws IOException {
        launch(args) ;
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.close();
        new Server(serverPort).start();
    }
}
