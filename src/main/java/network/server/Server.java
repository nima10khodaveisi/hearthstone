package network.server;

import Models.Cards.Card;
import Models.Heros.Hero;
import Player.Player;
import Player.Deck;
import World.World;
import chatroom.client.group.Group;
import chatroom.server.ChatServer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import game.GamePanel;
import graphic.images.Images;
import network.Handleable;
import network.json.MyJson;
import network.server.game.GameServer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server extends Thread implements Handleable {

    private ServerSocket serverSocket;
    private ArrayList<ClientHandler> clientHandlers;
    private ArrayList<ClientHandler> waitingList;
    private ArrayList<GamePanel> gamePanels;

    public Server(int serverPort) throws IOException {
        serverSocket = new ServerSocket(serverPort);
        clientHandlers = new ArrayList<>();
        waitingList = new ArrayList<>();
        gamePanels = new ArrayList<>() ;
        World.getWorld().init();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();
                ClientHandler clientHandler = new ClientHandler(this, socket);
                clientHandler.start();
                clientHandlers.add(clientHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleMessage(ClientHandler clientHandler, String message) {
        System.out.println("server handle message :\n" + message);
        try {
            JSONObject json = (JSONObject) new JSONParser().parse(message);
            Method method = this.getClass().getMethod((String) json.get("type"), ClientHandler.class, JSONObject.class);
            method.invoke(this, clientHandler, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("===================================");
    }

    @Override
    public void sendMessage(ClientHandler clientHandler, String message) {
        System.out.println("server is sending message\n" + message);
        clientHandler.getSender().sendMessage(message);
        System.out.println("++++++++++++++++++++++++++++++++");
    }

    @Override
    public void sendMessage(ClientHandler clientHandler, JSONObject jsonObject) {
        sendMessage(clientHandler, jsonObject.toJSONString());
    }

    //handle message classes
    public void login(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        String password = (String) jsonObject.get("password");
        if (World.getWorld().login(username, password)) {
            sendMessage(clientHandler, MyJson.create_json(
                    "type", "login",
                    "status", "ok",
                    "username", username));
            clientHandler.setPlayer(World.getWorld().getPlayerByUsername(username));
            World.getWorld().getPlayerByUsername(username).setOnline(true) ;
        } else {
            sendMessage(clientHandler, MyJson.create_json(
                    "type", "login",
                    "status", "fail"));
        }
    }

    public void register(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        String password = (String) jsonObject.get("password");
        if (World.getWorld().register(username, password)) {
            sendMessage(clientHandler, MyJson.create_json(
                    "type", "login",
                    "status", "ok",
                    "username", username));
            clientHandler.setPlayer(World.getWorld().getPlayerByUsername(username));
            World.getWorld().getPlayerByUsername(username).setOnline(true) ;
        } else {
            sendMessage(clientHandler, MyJson.create_json(
                    "type", "register",
                    "status", "fail"));
        }
    }

    public void logout(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        World.getWorld().logout(username);
        sendMessage(clientHandler, MyJson.create_json(
                "type", "logout"));
        clientHandler.setPlayer(null);
    }

    public void sendInitCollectionsData(ClientHandler clientHandler, String username, String error) {
        Gson gson = new Gson();
        sendMessage(clientHandler, MyJson.create_json(
                "type", "collections",
                "player", gson.toJson(World.getWorld().getPlayerByUsername(username)),
                "allCards", gson.toJson(World.getWorld().getCards()),
                "allHeroes", gson.toJson(World.getWorld().getHeroes()),
                "error", error));
    }

    public void collections(ClientHandler clientHandler, JSONObject jsonObject) {
        String query = (String) jsonObject.get("query");
        String username = (String) jsonObject.get("username");
        Player player = World.getWorld().getPlayerByUsername(username);
        switch (query) {
            case "addToDeck": {
                String deckName = (String) jsonObject.get("deckName");
                String cardName = (String) jsonObject.get("cardName");
                Card card = World.getWorld().getCardByName(cardName).copy();
                if (player.canAddToDeck(deckName, card)) {
                    player.addToDeck(deckName, card);
                    //sendInitCollectionsData(clientHandler, username, cardName + " has been added to " + deckName);
                } else {
                    //sendInitCollectionsData(clientHandler, username, "You can't add this card to selected deck");
                }
                break;
            }
            case "removeFromDeck": {
                String deckName = (String) jsonObject.get("deckName");
                String cardName = (String) jsonObject.get("cardName");
                Card card = World.getWorld().getCardByName(cardName).copy();
                Deck deck = player.getDeckByName(deckName);
                if (deck.contain(card)) {
                    player.removeFromDeck(deckName, card);
                    //sendInitCollectionsData(clientHandler, username, "removed!");
                } else {
                    //sendInitCollectionsData(clientHandler, username, "You can't remove this card from selected deck");
                }
                break;
            }
            case "updateDeck": {
                String oldDeckName = (String) jsonObject.get("oldDeckName");
                String newDeckName = (String) jsonObject.get("newDeckName");
                String heroName = (String) jsonObject.get("heroName");
                player.changeNameOfDeck(oldDeckName, newDeckName);
                player.changeHeroOfDeck(newDeckName, new Hero(heroName));
                //sendInitCollectionsData(clientHandler, username, "");
                break;
            }
            case "createNewDeck": {
                String deckName = (String) jsonObject.get("deckName");
                String heroName = (String) jsonObject.get("heroName");
                player.addNewDeck(deckName, heroName);
            }
            case "show": {
                sendInitCollectionsData(clientHandler, username, "");
            }
        }

    }

    public void shop(ClientHandler clientHandler, JSONObject jsonObject) {
        String query = (String) jsonObject.get("query");
        String username = (String) jsonObject.get("username");
        Player player = World.getWorld().getPlayerByUsername(username);
        switch (query) {
            case "show": {
                Gson gson = new Gson();
                sendMessage(clientHandler, MyJson.create_json(
                        "type", "shop",
                        "player", gson.toJson(World.getWorld().getPlayerByUsername(username)),
                        "allCards", gson.toJson(World.getWorld().getCards())
                ));
            }
            case "sell": {
                Card card = World.getWorld().getCardByName((String) jsonObject.get("card"));
                player.sell(card);
            }
            case "buy": {
                Card card = World.getWorld().getCardByName((String) jsonObject.get("card"));
                player.buy(card);
            }
        }
    }

    public void selectPage(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        Player player = World.getWorld().getPlayerByUsername(username);
        String query = (String) jsonObject.get("query");
        switch (query) {
            case "show": {
                Gson gson = new Gson();
                sendMessage(clientHandler, MyJson.create_json(
                        "type", "selectPage",
                        "gameType" , (String) jsonObject.get("gameType"),
                        "player", gson.toJson(World.getWorld().getPlayerByUsername(username))
                ));
            }
        }
    }

    private ClientHandler getBestOpponent(Player player) {
        //TODO
        if (waitingList.isEmpty())
            return null;
        return waitingList.get(0);
    }

    public void onlinePlay(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        Player player = clientHandler.getPlayer();
        ClientHandler opponent = getBestOpponent(player);
        if (opponent != null) {
            waitingList.remove(opponent);
            GamePanel gamePanel = new GamePanel("OnlineGame", opponent, clientHandler);
            gamePanels.add(gamePanel) ;
            gamePanel.start();
        } else {
            waitingList.add(clientHandler);
        }
    }

    public void singlePlay(ClientHandler clientHandler, JSONObject jsonObject) {
        Player player = clientHandler.getPlayer() ;
        new GamePanel("MultiPlayer", clientHandler, clientHandler).start(); ;
    }

    public void openGroup(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        Gson gson = new Gson();
        Group group = (Group) gson.fromJson((String) jsonObject.get("group"), Group.class);
        ChatServer chatServer = new ChatServer(group, false);
        chatServer.start();
        int port = group.getServerPort();
        sendMessage(clientHandler, MyJson.create_json(
                "type", "chatroom",
                "query", "join",
                "port", port).toJSONString());
    }

    public void friendship(ClientHandler clientHandler, JSONObject jsonObject) {
        Player player = clientHandler.getPlayer();
        String query = (String) jsonObject.get("query");
        switch (query) {
            case "show": {
                Gson gson = new Gson();
                ArrayList<Player> friends = new ArrayList<Player>();
                for (String name : player.getFriends()) {
                    friends.add(World.getWorld().getPlayerByUsername(name));
                }
                ArrayList<String> gameTexts = new ArrayList<>() ;
                ArrayList<Long> gamePorts = new ArrayList<>() ;
                ArrayList<Long> chatPorts = new ArrayList<>() ;
                for(GamePanel gamePanel : gamePanels) {
                    if(!gamePanel.getGameState().isHasEnded()) {
                        gameTexts.add(gamePanel.getPlayer1Username() + " vs " + gamePanel.getPlayer2Username()) ;
                        gamePorts.add((long )gamePanel.getServerPort()) ;
                        chatPorts.add((long) gamePanel.getChatPort()) ;
                    }
                }
                sendMessage(clientHandler, MyJson.create_json(
                        "type", "friendship",
                        "player", gson.toJson(player),
                        "friends", gson.toJson(friends),
                        "gameTexts", gson.toJson(gameTexts),
                        "gamePorts", gson.toJson(gamePorts),
                        "chatPorts", gson.toJson(chatPorts)
                        )
                );
                break;
            }
            case "addFriend": {
                String username = (String) jsonObject.get("username");
                Player friend = World.getWorld().getPlayerByUsername(username);
                if (friend != null) {
                    if (!player.isFriend(username)) {
                        player.addFriend(username);
                    }
                } else {
                }
                Gson gson = new Gson();
                ArrayList<Player> friends = new ArrayList<Player>();
                for (String name : player.getFriends()) {
                    friends.add(World.getWorld().getPlayerByUsername(name));
                }

                sendMessage(clientHandler, MyJson.create_json(
                        "type", "friendship",
                        "player", gson.toJson(player),
                        "friends", gson.toJson(friends)));
                break;
            }
            case "removeFriend": {
                String username = (String) jsonObject.get("username");
                if (player.isFriend(username)) {
                    player.removeFriend(username);
                }
                Gson gson = new Gson();
                ArrayList<Player> friends = new ArrayList<Player>();
                for (String name : player.getFriends()) {
                    friends.add(World.getWorld().getPlayerByUsername(name));
                }
                sendMessage(clientHandler, MyJson.create_json(
                        "type", "friendship",
                        "player", gson.toJson(player),
                        "friends", gson.toJson(friends)));
                break;
            }
            case "addGroup": {
                String name = (String) jsonObject.get("name");
                Group group = new Group(name);
                player.addGroup(group);
                group.addUser(player.getUsername());
                ArrayList<Player> friends = new ArrayList<Player>();
                for (String namee : player.getFriends()) {
                    friends.add(World.getWorld().getPlayerByUsername(namee));
                }
                Gson gson = new Gson();
                sendMessage(clientHandler, MyJson.create_json(
                        "type", "friendship",
                        "player", gson.toJson(player),
                        "friends", gson.toJson(friends)));
                break;
            }
        }
    }
}
