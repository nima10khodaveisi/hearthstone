package Models.Heros ;

import Models.Cards.Card;
import Player.Player;
import Player.Deck ;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Hero {
    //TODO implement hero powers
    private String name ;
    private long hp ;

    public Hero() { }

    public Hero(String name) {
        JSONObject jsonObject = getHeroJSON(name) ;
        this.name = (String) jsonObject.get("name") ;
        this.hp = (long) jsonObject.get("hp") ;
    }

    //functions

    public Hero copy() {
        return new Hero(this.getName()) ;
    }

    private JSONObject getHeroJSON(String name) {
        try {
            String path = "Heroes/" + name + ".json" ;
            Object object = new JSONParser().parse(new FileReader(path)) ;
            return (JSONObject) object ;
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return null ;
    }


    //getter and setter
    public String getName() {
        return name;
    }

    public long getHp() {
        return hp;
    }

    public void setHp(long hp) {
        this.hp = hp;
    }

    public void setName(String name) {
        this.name = name;
    }
}
