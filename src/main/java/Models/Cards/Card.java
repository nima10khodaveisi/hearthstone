package Models.Cards;

import Models.Cards.Minions.Minion;
import Models.Cards.Quest.Quest;
import Models.Cards.Spells.Spell;
import Models.Cards.Weapon.Weapon;
import World.World;
import interfaces.CopyAble;

//TODO set prices for cards in json
public class Card implements CopyAble {
    private String name ;
    private long mana ;
    private CardType type ;
    private String heroClass ;
    private String description ;
    private String rarity ;
    private long price ;
    private long hp ;
    private long attack ;

    public Card() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return this.name.equals(card.getName()) ;
    }

    //getter and setter


    public long getAttack() {
        return attack;
    }

    public void setAttack(long attack) {
        this.attack = attack;
    }

    public long getHp() {
        return hp;
    }

    public void setHp(long hp) {
        this.hp = hp;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public long getMana() {
        return mana;
    }

    public void setMana(long mana) {
        this.mana = mana;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    @Override
    public Card copy() {
        if(this.getType() == CardType.MINION)
            return new Minion(this.getName()) ;
        else if(this.getType() == CardType.SPELL)
            return new Spell(this.getName()) ;
        else if(this.getType() == CardType.QUEST)
            return new Quest(this.getName()) ;
        else if(this.getType() == CardType.WEAPON)
            return new Weapon(this.getName()) ;
        System.exit(0) ;
        return null ;
    }
}
