package Models.Cards.Weapon;

import Models.Cards.Card;
import Models.Cards.CardType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Weapon extends Card {
    //constructor
    public Weapon(String name) {
        JSONObject jsonObject = getWeaponJSON(name) ;
        setName(name) ;
        setMana((long)jsonObject.get("mana")) ;
        setDescription((String) jsonObject.get("description")) ;
        setPrice((long) jsonObject.get("price")) ;
        setRarity((String) jsonObject.get("rarity")) ;
        setType(CardType.WEAPON) ;
        setHeroClass((String) jsonObject.get("heroClass")) ;
        setAttack((long) jsonObject.get("attack")) ;
        setHp((long) jsonObject.get("hp")) ;
    }

    public Weapon() { }

    @Override
    public Card copy() {
        return new Weapon(this.getName()) ;
    }


    //functions

    private JSONObject getWeaponJSON(String name) {
        try {
            String path = "Cards" + File.separator + "Weapon" + File.separator + name + ".json" ;
            Object object = new JSONParser().parse(new FileReader(path)) ;
            return (JSONObject) object ;
        } catch (IOException | ParseException e) {
        }
        return null ;
    }

    //getter and setter ;

}