package Models.Cards.Minions ;

import Models.Cards.Card;
import Models.Cards.CardType;
import Models.Cards.Spells.Spell;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Minion extends Card {
    private Spell spell ;
    private boolean taunt ;
    private boolean rush ;

    //constructor
    public Minion(String name) {
        JSONObject jsonObject = getMinionJSON(name) ;
        setName(name) ;
        setType(CardType.MINION) ;
        setMana((long)jsonObject.get("mana")) ;
        setHeroClass((String)jsonObject.get("heroClass")) ;
        setRarity((String)jsonObject.get("rarity")) ;
        setDescription((String)jsonObject.get("description")) ;
        setSpell(new Spell((JSONObject)jsonObject.get("spell"))) ;
        setPrice((long) jsonObject.get("price")) ;
        setAttack((long)jsonObject.get("attack"));
        setHp((long)jsonObject.get("hp"));
        setTaunt((boolean)jsonObject.get("taunt")) ;
        setRush((boolean)jsonObject.get("rush")) ;
    }

    public Minion(Minion minion) {
        this(minion.getName()) ;
    }

    public Minion() { }

    @Override
    public Card copy() {
        return new Minion(this.getName()) ;
    }

    //functions


    private JSONObject getMinionJSON(String name) {
        try {
            String path = "Cards" + File.separator + "Minions" + File.separator + name + ".json" ;
            Object object = new JSONParser().parse(new FileReader(path)) ;
            return (JSONObject) object ;
        } catch (IOException | ParseException e) {
        }
        return null ;
    }


    public void damage(int value) {
        setHp(getHp() - value) ;
    }


    //getter and setter


    public Spell getSpell() {
        return spell;
    }

    public void setSpell(Spell spell) {
        this.spell = spell;
    }

    public boolean isTaunt() {
        return taunt;
    }

    public void setTaunt(boolean taunt) {
        this.taunt = taunt;
    }

    public boolean isRush() {
        return rush;
    }

    public void setRush(boolean rush) {
        this.rush = rush;
    }
}
