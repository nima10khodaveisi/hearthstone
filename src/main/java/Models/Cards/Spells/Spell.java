package Models.Cards.Spells ;

import Models.Cards.Card;
import Models.Cards.CardType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Spell extends Card {
    JSONObject spell = new JSONObject() ;

    //constructor
    public Spell(String name) {
        JSONObject jsonObject = getSpellJSON(name) ;
        setName(name) ;
        setMana((long)jsonObject.get("mana")) ;
        setDescription((String) jsonObject.get("description")) ;
        setPrice((long) jsonObject.get("price")) ;
        setRarity((String) jsonObject.get("rarity")) ;
        setType(CardType.SPELL) ;
        setHeroClass((String) jsonObject.get("heroClass")) ;
        setSpell((JSONObject) jsonObject.get("spell")) ;
    }

    public Spell(JSONObject spell) {
        setSpell(spell) ;
    }

    public Spell() { }

    @Override
    public Card copy() {
        return new Spell(this.getName()) ;
    }


    //functions

    private JSONObject getSpellJSON(String name) {
        try {
            String path = "Cards" + File.separator + "Spells" + File.separator + name + ".json" ;
            Object object = new JSONParser().parse(new FileReader(path)) ;
            return (JSONObject) object ;
        } catch (IOException | ParseException e) {
        }
        return null ;
    }

    //getter and setter ;

    public JSONObject getSpell() {
        return spell;
    }

    public void setSpell(JSONObject spell) {
        this.spell = spell;
    }
}
