package Models.Cards.Quest;

import Models.Cards.Card;
import Models.Cards.CardType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Quest extends Card {
    //constructor
    public Quest(String name) {
        JSONObject jsonObject = getQuestJSON(name) ;
        setName(name) ;
        setMana((long)jsonObject.get("mana")) ;
        setDescription((String) jsonObject.get("description")) ;
        setPrice((long) jsonObject.get("price")) ;
        setRarity((String) jsonObject.get("rarity")) ;
        setType(CardType.QUEST) ;
        setHeroClass((String) jsonObject.get("heroClass")) ;
    }

    public Quest() { }

    @Override
    public Card copy() {
        return new Quest(this.getName()) ;
    }


    //functions

    private JSONObject getQuestJSON(String name) {
        try {
            String path = "Cards" + File.separator + "Quest" + File.separator + name + ".json" ;
            Object object = new JSONParser().parse(new FileReader(path)) ;
            return (JSONObject) object ;
        } catch (IOException | ParseException e) {
        }
        return null ;
    }
}
