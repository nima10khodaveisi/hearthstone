package audio;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MyMedia {
    private MediaPlayer mediaPlayer ;

    public MyMedia(Media media, int count) {
        mediaPlayer = new MediaPlayer(media) ;
        //mediaPlayer.setCycleCount(count) ;
    }

    public void play() {
        mediaPlayer.play();
    }

    public void stop() {
        mediaPlayer.stop();
    }

    public void setVolume(double volume) {
        mediaPlayer.setVolume(volume) ;
    }
}
