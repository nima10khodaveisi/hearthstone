package audio;

import javax.sound.sampled.*;
import javax.swing.undo.AbstractUndoableEdit;
import java.io.File;
import java.io.IOException;

public class AudioPlayer {
    private static AudioPlayer instance;
    private Clip menu ;
    private Clip battle ;
    private float volume ;
    private float vol ;

    public static AudioPlayer getInstance() {
        if (instance == null) {
            instance = new AudioPlayer();
        }
        return instance;
    }

    private AudioPlayer() {
        try {
            menu = AudioSystem.getClip() ;
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("sounds" + File.separator + "menu.wav")) ;
            menu.open(audioInputStream) ;
            menu.loop(Clip.LOOP_CONTINUOUSLY) ;

            battle = AudioSystem.getClip() ;
            AudioInputStream audioInputStream1 = AudioSystem.getAudioInputStream(new File("sounds" + File.separator + "battleGround.wav")) ;
            battle.open(audioInputStream1) ;
            battle.loop(Clip.LOOP_CONTINUOUSLY) ;
        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
        setVolume(1) ;
    }

    public void playMenu() {
        menu.start();
    }

    public void setVolume(float volume) {
        vol = volume ;
        this.volume = (float) ((float) Math.log(volume) / Math.log(10) * 20.0);
        FloatControl floatControl = (FloatControl)menu.getControl(FloatControl.Type.MASTER_GAIN) ;
        floatControl.setValue((float)this.volume) ;

        floatControl = (FloatControl)battle.getControl(FloatControl.Type.MASTER_GAIN) ;
        floatControl.setValue((float)this.volume) ;
    }

    public void playBattle() {
        battle.start();
    }

    public void stopMenu() {
        menu.stop();
    }

    public void stopBattle() {
        battle.stop() ;
    }

    public void playEndTurn() {
        try {
            Clip endTurn ;
            endTurn = AudioSystem.getClip() ;
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("sounds" + File.separator + "endTurn.wav")) ;
            endTurn.open(audioInputStream) ;
            endTurn.loop(1) ;
            FloatControl floatControl = (FloatControl)endTurn.getControl(FloatControl.Type.MASTER_GAIN) ;
            floatControl.setValue((float)this.volume) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playPutCard() {
        try {
            Clip clip ;
            clip = AudioSystem.getClip() ;
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("sounds" + File.separator + "putCardFromHand.wav")) ;
            clip.open(audioInputStream) ;
            clip.loop(1) ;
            FloatControl floatControl = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN) ;
            floatControl.setValue((float)this.volume) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public float getVolume() { return vol ; }
}
