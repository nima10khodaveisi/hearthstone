package Player;

public class Password {
    private static final long mod = 1000 * 1000 * 1000 + 7;
    private static final long base = 465 ;

    public static long hash(String str) {
        long pw = 1 ;
        long ret = 0 ;
        for(int i = 0 ; i < str.length() ; ++i) {
            long c = (int)(str.charAt(i)) ;
            ret = (ret + (c * pw) % mod) % mod ;
            pw = pw * base % mod ;
        }
        return ret ;
    }
}
