package Player;

import Models.Cards.Card;
import Models.Heros.Hero;
import World.World;

import javax.crypto.AEADBadTagException;
import java.util.ArrayList;

public class Deck implements Comparable<Deck> {
    private String name ;
    private ArrayList<Card> cards = new ArrayList<>() ;
    private ArrayList<Integer> used = new ArrayList<>() ;
    private int wins ;
    private int numberOfGames ;
    private Hero hero ;
    private final int MAX_DECK_SIZE = 30 ;

    public Deck() { }


    public Deck(String name , Hero hero) {
        this.name = name ;
        this.hero = hero ;
        this.wins = 0 ;
        this.numberOfGames = 0 ;
    }


    public static Deck randomDeck(Hero hero) {
        ArrayList<Card> cards = World.getWorld().getCards() ;
        int cnt = 0 ;
        ArrayList<Card> ret = new ArrayList<>() ;
        for(Card card : cards) {
            if(cnt < 30 && (card.getHeroClass().equals(hero.getName()) || card.getHeroClass().equals("Natural"))) {
                cnt++ ;
                ret.add(card) ;
            }
        }
        Deck deck = new Deck("Random", hero) ;
        deck.setCards(ret) ;
        return deck ;
    }

    public Deck copy() {
        Deck ret = new Deck() ;
        ret.name = this.name ;
        for(Card card : this.cards)
            ret.addCard(card.copy()) ;
        ret.wins = this.wins ;
        ret.numberOfGames = this.numberOfGames ;
        ret.hero = new Hero(this.hero.getName()) ;
        return ret ;
    }

    public boolean canChangeHero(Hero hero) {
        for(Card card : cards) {
            if(card.getHeroClass().equals("Natural")) continue ;
            if(!card.getHeroClass().equals(hero.getName()))
                return false ;
        }
        return true ;
    }


    public int count(Card card) {
        int cnt = 0 ;
        for(Card x : cards) {
            if(x.getName().equals(card.getName()))
                cnt++ ;
        }
        return cnt ;
    }

    public boolean contain(Card card) {
        return count(card) > 0 ;
    }

    public boolean canAdd(Card card) {
        return (card.getHeroClass().equals(hero.getName()) || card.getHeroClass().equals("Natural")) && count(card) < 2 && cards.size() < MAX_DECK_SIZE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCard(Card card) {
       cards.add(card) ;
       used.add(0) ;
    }

    public void removeCard(Card card) {
        for(int i = 0 ; i < cards.size() ; ++i) {
            Card x = cards.get(i) ;
            if(x.getName().equals(card.getName())) {
                cards.remove(x) ;
              //  used.remove(i) ;
                break ;
            }
        }
    }

    public Card get(int ind) {
        return cards.get(ind) ;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public ArrayList<Integer> getUsed() {
        return used;
    }

    public void setUsed(ArrayList<Integer> used) {
        this.used = used;
    }

    public int size() { return cards.size() ; }


    public double averageMana() {
        if(cards.isEmpty())
            return 0 ;
        double av = 0 ;
        for(Card card : cards)
            av += (double)card.getMana() ;
        av /= (double)cards.size() ;
        return av ;
    }


    @Override
    public int compareTo(Deck deck) {
        if(wins * deck.numberOfGames == deck.wins * numberOfGames) {
            if(wins == deck.wins) {
                if(numberOfGames == deck.numberOfGames) {
                    if(averageMana() == deck.averageMana()) {
                        return name.compareTo(deck.name) ;
                    }
                    return (averageMana() > deck.averageMana() ? 1 : -1) ;
                }
                return (numberOfGames > deck.numberOfGames ? 1 : -1) ;
            }
            return (wins > deck.wins ? 1 : -1) ;
        }

        if(wins * deck.numberOfGames > deck.wins * numberOfGames)
            return 1 ;
        else
            return -1 ;
    }
}
