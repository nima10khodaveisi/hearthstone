package Player;

import Models.Cards.Card;
import Models.Cards.CardType;
import Models.Cards.Minions.Minion;
import Models.Cards.Spells.Spell;
import Models.Heros.Hero;
import World.World;
import chatroom.client.group.Group;
import game.InfoPassive;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.google.gson.Gson;

import java.io.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class Player {
    public static int numberOfUsers = 0;
    private String username;
    private String password;
    private long coins;
    private int userId;
    private ArrayList<Card> cards = new ArrayList<>();
    private ArrayList<Hero> heroes = new ArrayList<>();
    private ArrayList<Deck> decks = new ArrayList<>();
    private Deck curDeck = new Deck("Mage", new Hero("Mage"));
    private final long INIT_COINS = 50;
    private boolean isDeleted;
    private transient InfoPassive infoPassive;
    private boolean online;
    private ArrayList<String> friends;
    private ArrayList<Group> groups;
    private int cup ;

    private File folder = null;
    private File logFile = null;
    private File jsonFile = null;

    //constructor
    public Player(String username, String password) {
        numberOfUsers++;
        this.cup = 0 ;
        this.username = username;
        this.password = String.valueOf(Password.hash(password));
        this.coins = INIT_COINS;
        this.userId = numberOfUsers;
        this.isDeleted = false;
        friends = new ArrayList<>();
        groups = new ArrayList<>();

        String path = "Users";
        path += File.separator + username + "-" + userId;
        folder = new File(path);
        folder.mkdir();
        logFile = new File(path + File.separator + "Log.txt");
        try {
            logFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        jsonFile = new File(path + File.separator + "UserData.json");
        try {
            jsonFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        init();
        updateJsonFile();
        appendEventWithDate("user created");
        appendEvent("username  : " + username);
        appendEvent("password  : " + this.password);
        appendEvent("\n");
        appendEvent("\n");
    }

    public Player() {
    }

    //functions

    public static Player getPlayerByJSON(File file) {
        Gson gson = new Gson();
        try {
            Player ret = gson.fromJson(new FileReader(file.getPath() + File.separator + "UserData.json"), Player.class);
            return ret;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void init() {
        //initial heroes
        heroes.add(new Hero("Mage"));
        //initial cards
        int cnt = 0;
        boolean spec = false;
        for (Card card : World.getWorld().getCards()) {
            if (!spec && card.getHeroClass().equals("Mage") && card.getType().equals("Minion")) {
                cards.add(new Minion(card.getName()));
                spec = true;
            }
            if (cnt < 10 && card.getHeroClass().equals("Natural")) {
                cnt++;
                cards.add(card);
            }
        }
        //initial decks
        for (Hero hero : heroes) {
            Deck deck = new Deck(hero.getName(), hero);
            for (Card card : cards) {
                if (deck.canAdd(card)) {
                    deck.addCard(card);
                }
            }
            if (hero.getName().equals("Mage"))
                curDeck = deck;
            decks.add(deck);
        }
    }

    private ArrayList<String> getCardsName(ArrayList<Card> curCards) {
        ArrayList<String> ret = new ArrayList<>();
        for (Card loopCard : curCards) {
            ret.add(loopCard.getName());
        }
        return ret;
    }

    private ArrayList<String> getHeroesName(ArrayList<Hero> curHeroes) {
        ArrayList<String> ret = new ArrayList<>();
        for (Hero loopHero : curHeroes) {
            ret.add(loopHero.getName());
        }
        return ret;
    }

    public Deck getDeckByName(String nameOfDeck) {
        for (Deck deck : decks) {
            if (deck.getName().equals(nameOfDeck))
                return deck;
        }
        return null;
    }

    public void changeNameOfDeck(String before, String after) {
        Deck deck = getDeckByName(before);
        if (deck == null) return;
        deck.setName(after);
        appendEventWithDate("change name of " + before + " to " + after);
        updateJsonFile();
    }

    public void changeHeroOfDeck(String nameOfDeck, Hero hero) {
        Deck deck = getDeckByName(nameOfDeck);
        deck.setHero(hero);
        appendEventWithDate("change hero of " + nameOfDeck + " to " + hero.getName());
        updateJsonFile();
    }

    public boolean canAddNewDeck(String nameOfDeck) {
        for (Deck deck : decks) {
            if (deck.getName().equals(nameOfDeck))
                return false;
        }
        return true;
    }

    public void addNewDeck(String nameOfDeck, String heroName) {
        decks.add(new Deck(nameOfDeck, new Hero(heroName)));
        appendEventWithDate("create new deck " + nameOfDeck);
        updateJsonFile();
    }

    public void addToDeck(String nameOfDeck, Card card) {
        if (freeCards().contains(card.getName())) {
            Deck deck = getDeckByName(nameOfDeck);
            if (deck.canAdd(card)) {
                deck.addCard(card);
                appendEventWithDate("add " + card.getName() + " to " + nameOfDeck);
            }
        }
        updateJsonFile();
    }

    public void removeFromDeck(String nameOfDeck, Card card) {
        Deck deck = getDeckByName(nameOfDeck);
        deck.removeCard(card);
        appendEventWithDate("remove " + card.getName() + " from " + nameOfDeck);
        updateJsonFile();
    }

    public void deleteDeck(String nameOfDeck) {
        for (Deck deck : decks) {
            if (deck.getName().equals(nameOfDeck)) {
                decks.remove(deck);
                break;
            }
        }
        updateJsonFile();
    }


    public boolean buy(Card card) {
        if (coins >= card.getPrice()) {
            coins -= card.getPrice();
            cards.add(card);
            appendEventWithDate("buy " + card.getName());
            updateJsonFile();
            return true;
        }
        return false;
    }

    public ArrayList<String> freeCards() {
        ArrayList<String> ret = new ArrayList<>();
        for (Card card : cards) {
            ret.add(card.getName());
        }
        for (Deck deck : decks)
            for (Card card : deck.getCards()) {
                ret.remove(card.getName());
            }
        return ret;
    }

    public boolean sell(Card card) {
        if (freeCards().contains(card.getName())) {
            cards.remove(card);
            coins += card.getPrice();
            appendEventWithDate("sell " + card.getName());
            updateJsonFile();
            return true;
        }
        return false;
    }


    public boolean canAddToDeck(String nameOfDeck, Card card) {
        Deck deck = getDeckByName(nameOfDeck);
        return freeCards().contains(card.getName()) && deck.canAdd(card);
    }

    public boolean canSell(Card card) {
        return freeCards().contains(card.getName());
    }

    public boolean canBuy(Card card) {
        return coins >= card.getPrice();
    }

    public ArrayList<Card> canSellCards() {
        ArrayList<Card> ret = new ArrayList<>();
        for (Card card : cards) {
            if (canSell(card)) {
                ret.add(card);
            }
        }
        return ret;
    }

    //print
    public void printAllHeroes() {
        appendEventWithDate("print all heroes");
        System.out.print("all heroes : " + "\n");
        for (Hero hero : World.getWorld().getHeroes()) {
            System.out.print(hero.getName() + "\n");
        }
        System.out.println();
    }

    /*
    public void printCurHero() {
        appendEventWithDate("print selected hero");
        System.out.print("selected hero : " + "\n");
        System.out.println(getCurHero().getName() + "\n");
    }*/

    public void printAllPlayerCards() {
        appendEventWithDate("print all player's card");
        System.out.print("all cards : " + "\n");
        for (Card card : getCards()) {
            System.out.print(card.getName() + "\n");
        }
        System.out.println();
    }

    public void printCurDeck() {
        appendEventWithDate("print current deck");
        HashMap<String, Integer> cnt = new HashMap<>();
        for (Card card : getCurDeck().getCards()) {
            if (cnt.get(card.getName()) == null) {
                cnt.put(card.getName(), 1);
            } else {
                int number = cnt.get(card.getName());
                cnt.remove(card.getName());
                cnt.put(card.getName(), number + 1);
            }
        }
        HashMap<String, Boolean> mark = new HashMap<>();
        for (Card card : getCurDeck().getCards()) {
            if (mark.get(card.getName()) == null) {
                mark.put(card.getName(), true);
            }
        }
        System.out.println();
    }

    public boolean containCard(Card card) {
        for (Card card1 : getCards()) {
            if (card.getName().equals(card1.getName()))
                return true;
        }
        return false;
    }

    /*
    public void printCanAddToDeck() {
        appendEventWithDate("print cads that player can add to deck");
        System.out.print("you can add these cards to your cur deck : " + "\n");
        HashMap<String, Boolean> mark = new HashMap<>();
        for (Card card : getCards()) {
            if (getCurHero().canAddToDeck(card) && freeCards().contains(card.getName()) && (mark.get(card.getName()) == null || !mark.get(card.getName()))) {
                System.out.print(card.getName() + "\n");
                mark.put(card.getName(), true);
            }
        }
        System.out.println();
    }*/

    public void printWallet() {
        appendEventWithDate("print wallet");
        System.out.println("you have " + getCoins() + " coin");
    }

    public void printCanSellCards() {
        appendEventWithDate("print the cards that player can sell");
        HashSet<String> nameOfCanSellCards = new HashSet<>();
        ArrayList<Card> canSellCards = canSellCards();
        for (Card card : canSellCards) {
            nameOfCanSellCards.add(card.getName());
        }
        System.out.print("you can sell these cards : " + "\n");
        for (String name : nameOfCanSellCards) {
            System.out.print(name + "\n");
        }
        System.out.println();
    }

    public void printCanBuyCards() {
        appendEventWithDate("print the cards that player can buy");
        System.out.print("you can buy these cards : " + "\n");
        for (Card card : World.getWorld().getCards()) {
            if (canBuy(card)) {
                System.out.print(card.getName() + "\n");
            }
        }
        System.out.println();
    }

    //LOG

    public void updateJsonFile() {
        Gson gson = new Gson();
        String json = gson.toJson(Player.this);
        try {
            FileWriter fileWriter = new FileWriter(jsonFile);
            fileWriter.write(json);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendEventWithDate(String eventDescription) {
        Date eventDate = new Date();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(logFile, true));
            bufferedWriter.write("\n");
            bufferedWriter.write("at " + eventDate + "  :  " + eventDescription);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void appendEvent(String eventDescription) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(logFile, true));
            bufferedWriter.write("\n");
            bufferedWriter.write(eventDescription);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Player getJson() {
        try {
            Gson gson = new Gson();
            return gson.fromJson(new FileReader(jsonFile), Player.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    //getter and setter
    public String getUsername() {
        return getJson().username;
    }

    public String getPassword() {
        return (String) getJson().password;

    }

    public void setPassword(String password) {
        this.password = password;
        updateJsonFile();
    }

    public long getCoins() {
        return getJson().coins;
    }

    public void setCoins(long coins) {
        this.coins = coins;
        updateJsonFile();
    }

    public Deck getCurDeck() {
        return curDeck;
    }

    public void setCurDeck(Deck curDeck) {
        this.curDeck = curDeck;
    }

    public ArrayList<Card> getCards() {
        return getJson().cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
        updateJsonFile();
    }

    public ArrayList<Hero> getHeroes() {
        return getJson().getHeroes();
    }

    public void setHeroes(ArrayList<Hero> heroes) {
        this.heroes = heroes;
        updateJsonFile();
    }

    public int getUserId() {
        return userId;
    }

    public ArrayList<String> getCardsName() {
        ArrayList<String> ret = new ArrayList<>();
        for (Card card : cards)
            ret.add(card.getName());
        return ret;
    }

    public ArrayList<String> getHeroesName() {
        ArrayList<String> ret = new ArrayList<>();
        for (Hero hero : heroes)
            ret.add(hero.getName());
        return ret;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
        appendEventWithDate("Player deleted");
        updateJsonFile();
    }

    public ArrayList<Deck> getDecks() {
        return decks;
    }

    public void setDecks(ArrayList<Deck> decks) {
        this.decks = decks;
    }

    public boolean isOnline() {

        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public InfoPassive getInfoPassive() {
        return infoPassive;
    }

    public void setInfoPassive(InfoPassive infoPassive) {
        this.infoPassive = infoPassive;
    }


    public void addFriend(String username) {
        friends.add(username);
        updateJsonFile();
    }

    public void removeFriend(String username) {
        friends.remove(username);
        updateJsonFile();
    }

    public boolean isFriend(String username) {
        return friends.contains(username);
    }

    public void addGroup(Group group) {
        for(Group group1 : groups) {
            if(group1.getName().equals(group.getName()))
                return;
        }
        groups.add(group);
        updateJsonFile();
    }

    public Group getGroupByName(String groupName) {
        for (Group group : groups) {
            if (group.getName().equals(groupName)) {
                return group;
            }
        }
        return null;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<String> friends) {
        this.friends = friends;
        updateJsonFile();
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
        updateJsonFile();
    }

    public void addCup(int value) {
        setCup(getCup() + value) ;
    }

    public int getCup() {
        return cup;
    }

    public void setCup(int cup) {
        this.cup = cup;
        if(this.cup < 0)
            this.cup = 0 ;
    }
}
