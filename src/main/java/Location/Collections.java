package Location;

import Models.Cards.Card;
import Models.Cards.CardType;
import Models.Cards.Minions.Minion;
import Models.Heros.Hero;
import Player.Player;
import World.World;

import java.util.Scanner;

public class Collections extends Location  {
    private static Collections collections = null ;

    public static Collections getCollections() {
        if(collections == null)
            collections = new Collections() ;
        return collections ;
    }

   /* public void commandHandler() {
        Player player = World.getWorld().getCurPlayer() ;
        System.out.print(ANSI_BLUE + player.getUsername() + "@hearthstone:" + ANSI_PURPLE + "~/menu/collections$ " + ANSI_RESET);
        Scanner sc = new Scanner(System.in) ;
        String command = sc.nextLine() ;
        command = fix(command) ;
        if(command.equals("ls -a heroes")) {
            player.printAllHeroes() ;
        } else if(command.equals("ls -m heroes")) {
            player.printCurHero() ;
        } else if(command.length() >= 6 && command.substring(0 , 6).equals("select")) {
            String[] str = command.split(" ") ;
            boolean find = false ;
            for(Hero hero : player.getHeroes()) {
                if(hero.getName().equals(str[1])) {
                    find = true ;
                    break ;
                }
            }
            if(!find) {
                str[1] = World.getWorld().findTheBestMatch(player.getHeroesName() , str[1]) ;
                System.out.println("did you mean " + str[1] + " ?");
                String answer = sc.nextLine() ;
                if(answer.equals("n")) {
                    commandHandler();
                    return ;
                }
            }
            player.setCurHero(str[1]) ;
        } else if(command.equals("ls -a cards")) {
            player.printAllPlayerCards() ;
        } else if(command.equals("ls -m cards")) {
            player.printCurDeck() ;
        } else if(command.equals("ls -n cards")) {
            player.printCanAddToDeck() ;
        } else if(command.length() >= 3 && command.substring(0 , 3).equals("add")) {
            String[] str = command.split(" ") ;
            for(int i = 2 ; i < str.length ; ++i)
                str[1] += ' ' + str[i] ;
            if(World.getWorld().getCardByName(str[1]) == null) {
                str[1] = World.getWorld().findTheBestMatch(player.getCardsName(), str[1]);
                System.out.println("did you mean " + str[1] + " ?");
                String answer = sc.nextLine() ;
                if(answer.equals("n")) {
                    commandHandler();
                    return ;
                }
            }
            if(player.canAddToDeck(World.getWorld().getCardByName(str[1]))) {
                player.addToDeck(World.getWorld().getCardByName(str[1])) ;
                player.appendEventWithDate("add to deck : " + str[1]) ;
            } else {
                System.out.println("you can not add this card to your deck");
                player.appendEventWithDate("error add to deck : " + str[1]) ;
            }
        } else if(command.length() >= 6 && command.substring(0 , 6).equals("remove")) {
            String[] str = command.split(" ") ;
            for(int i = 2 ; i < str.length ; ++i)
                str[1] += ' ' + str[i] ;
            if(World.getWorld().getCardByName(str[1]) == null) {
                str[1] = World.getWorld().findTheBestMatch(player.getCardsName(), str[1]);
                System.out.println("did you mean " + str[1] + " ?");
                String answer = sc.nextLine() ;
                if(answer.equals("n")) {
                    commandHandler();
                    return ;
                }
            }
            player.removeFromDeck(World.getWorld().getCardByName(str[1]))  ;
        } else if(command.equals("cd ..")) {
            player.appendEventWithDate("back to menu from collections") ;
            Menu.getMenu().commandHandler();
        } else if(command.equals("--help")) {
            System.out.println(Location.ANSI_GREEN + "you can use these commands here : " + Location.ANSI_RESET);
            System.out.println(Location.ANSI_CYAN + "   ls [option] heroes : \n" +
                    "\t\tls -a heroes : see list of all of your heroes \n" +
                    "\t\tls -m heroes : see the name of your selected hero \n" +
                    "\tls [option] cards : \n" +
                    "\t\tls -a cards : see list of all of your cards \n" +
                    "\t\tls -m cards : see the list of cards in your current deck \n" +
                    "\t\tls -n cards : see the list of cards that you can add to your current deck \n" +
                    "\tadd [Card's name] : add [Card's name] to your current deck \n" +
                    "\tdescription [Card's name] : get description of a card\n" +
                    "\tremove [Card's name] : remove [Card's name] from your current deck ;" + Location.ANSI_RESET);
        }else if(command.length() >= 11 && command.substring(0 , 11).equals("description")) {
            String[] str = command.split(" ") ;
            for(int i = 2 ; i < str.length ; ++i)
                str[1] += ' ' + str[i] ;
            if(World.getWorld().getCardByName(str[1]) == null) {
                str[1] = World.getWorld().findTheBestMatch(player.getCardsName(), str[1]);
                System.out.println("did you mean " + str[1] + " ?");
                String answer = sc.nextLine() ;
                if(answer.equals("n")) {
                    commandHandler();
                    return ;
                }
            }
            Card card = World.getWorld().getCardByName(str[1]) ;
            System.out.println(Location.ANSI_RED +"Name : "+ Location.ANSI_RESET + card.getName());
            System.out.println(Location.ANSI_RED +"Mana : "+ Location.ANSI_RESET + card.getMana());
            System.out.println(Location.ANSI_RED +"Hero class : "+ Location.ANSI_RESET + card.getHeroClass());
            System.out.println(Location.ANSI_RED +"Description : "+ Location.ANSI_RESET + card.getDescription());
            System.out.println(Location.ANSI_RED +"Type : "+ Location.ANSI_RESET + card.getType());
            if(card.getType() == CardType.MINION){
                System.out.println(Location.ANSI_RED +"Attack damage : "+ Location.ANSI_RESET + card.getAttack() );
                System.out.println(Location.ANSI_RED +"HP : "+ Location.ANSI_RESET + card.getHp());
            }
         }else {
            player.appendEventWithDate("error invalid command : " + command) ;
            System.out.println("invalid command");
        }
        Collections.getCollections().commandHandler();
    }*/
}
