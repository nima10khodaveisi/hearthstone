package Location;

import Player.Player;
import World.World ;
import java.util.Scanner;

public class Menu extends Location {
    private static Menu menu = null ;

    public static Menu getMenu() {
        if(menu == null)
            menu = new Menu() ;
        return menu ;
    }

   /* public void commandHandler() {
        Player player = World.getWorld().getCurPlayer();
        System.out.print(ANSI_BLUE + player.getUsername() + "@hearthstone:" + ANSI_PURPLE + "~/menu$ " + ANSI_RESET);
        Scanner sc = new Scanner(System.in);
        String command = sc.nextLine();
        command = fix(command);
        if (command.equals("cd store")) {
            player.appendEventWithDate("cd store");
            Store.getStore().commandHandler();
        } else if (command.equals("cd collections")) {
            player.appendEventWithDate("cd collections");
            Collections.getCollections().commandHandler();
        } else if (command.equals("logout")) {
            player.appendEventWithDate("logout");
            World.getWorld().start();
        } else if (command.equals("exit")) {
            player.appendEventWithDate("exit");
            System.exit(0);
        } else if(command.equals("delete")) {
            player.setDeleted(true) ;
            World.getWorld().setCurPlayer(null) ;
            World.getWorld().start();
        }  else if(command.equals("--help")) {
            System.out.println(Location.ANSI_GREEN + "you can use these commands here : " + Location.ANSI_RESET);
            System.out.println(Location.ANSI_CYAN + "   cd [dir] : \n" +
                    "\t\tcd store : go to store \n" +
                    "\t\tcd collections : go to collections \n" +
                    "\tlogout : logout from your account \n" +
                    "\texit : exit from game \n" +
                    "\tdelete : delete your account " + Location.ANSI_RESET);
            commandHandler();
        }else {
            player.appendEventWithDate("invalid command : " + command) ;
            World.getWorld().error("invalid command") ;
            commandHandler();
        }
    }*/
}
