package Location;

import Models.Cards.Card;
import Player.Player;
import World.World;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class Store extends Location {
    private static Store store = null ;

    public static Store getStore() {
        if(store == null)
            store = new Store() ;
        return store ;
    }

 /*   public void commandHandler() {
        Player player = World.getWorld().getCurPlayer() ;
        System.out.print(ANSI_BLUE + player.getUsername() + "@hearthstone:" + ANSI_PURPLE + "~/menu/store$ " + ANSI_RESET);
        Scanner sc = new Scanner(System.in) ;
        String command = sc.nextLine() ;
        command = fix(command) ;
        if(command.length() >= 3 && command.substring(0 , 3).equals("buy")) {
            String[] str = command.split(" ") ;
            for(int i = 2 ; i < str.length ; ++i)
                str[1] += ' ' + str[i] ;
            if(World.getWorld().getCardByName(str[1]) == null) {
                str[1] = World.getWorld().findTheBestMatch(player.getCardsName(), str[1]);
                System.out.println("did you mean " + str[1] + " ?");
                String answer = sc.nextLine() ;
                if(answer.equals("n")) {
                    commandHandler();
                    return ;
                }
            }
            boolean done = player.buy(World.getWorld().getCardByName(str[1])) ;
            if(!done) {
                World.getWorld().error("you can't buy this card :((") ;
                player.appendEventWithDate("error buy " + str[1]) ;
            } else {
                player.appendEventWithDate("buy " + str[1]) ;
            }
        } else if(command.length() >= 4 && command.substring(0 , 4).equals("sell")) {
            String[] str = command.split(" ") ;
            for(int i = 2 ; i < str.length ; ++i)
                str[1] += ' ' + str[i] ;
            if(World.getWorld().getCardByName(str[1]) == null) {
                str[1] = World.getWorld().findTheBestMatch(player.getCardsName(), str[1]);
                System.out.println("did you mean " + str[1] + " ?");
                String answer = sc.nextLine() ;
                if(answer.equals("n")) {
                    commandHandler();
                    return ;
                }
            }
            boolean done = player.sell(World.getWorld().getCardByName(str[1])) ;
            if(!done) {
                World.getWorld().error("you can't sell this card :((") ;
                player.appendEventWithDate("error sell " + str[1]) ;
            } else {
                player.appendEventWithDate("sell " + str[1]) ;
            }
        } else if(command.equals("wallet")) {
            player.printWallet() ;
        } else if(command.equals("ls -s")) {
            player.printCanSellCards() ;

        } else if(command.equals("ls -b")) {
            player.printCanBuyCards() ;
        } else if(command.equals("cd ..")) {
            player.appendEventWithDate("back to menu from store") ;
            Menu.getMenu().commandHandler();
        } else if(command.equals("--help")) {
            System.out.println(Location.ANSI_GREEN + "you can use these commands here : " + Location.ANSI_RESET);
            System.out.println(Location.ANSI_CYAN + "   buy [Card's name] : buy [Card's name] card \n" +
                    "\tsell [Card's name] : sell [Card's name] card \n" +
                    "\twallet : see the amount of your coins \n" +
                    "\tls -s : see the list of cards that you can sell \n" +
                    "\tls -b : see the list of cards that you can buy \n" +
                    "\tcd .. : back to menu " + Location.ANSI_RESET);
        } else {
            player.appendEventWithDate("error invalid command " + command) ;
            System.out.println("invalid command");
        }
        Store.getStore().commandHandler();
    }*/

}
