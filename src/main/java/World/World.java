package World;

import Location.Location;
import Location.Menu;
import Models.Cards.Card;
import Models.Cards.CardType;
import Models.Cards.Minions.Minion;
import Models.Cards.Quest.Quest;
import Models.Cards.Spells.Spell;
import Models.Cards.Weapon.Weapon;
import Models.Heros.*;
import Player.Player;
import Player.Password ;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class World {
    private static World world = null;
    private Player curPlayer = null;
    private ArrayList<Player> players = new ArrayList<Player>();
    private ArrayList<Card> cards = new ArrayList<Card>();
    private ArrayList<Hero> heroes = new ArrayList<Hero>();

    //functions

    public ArrayList<Card> getSpells() {
        ArrayList<Card> ret = new ArrayList<>() ;
        for(Card card : cards) {
            if(card.getType() == CardType.SPELL)
                ret.add(card) ;
        }
        return ret ;
    }

    public ArrayList<Card> getMinions() {
        ArrayList<Card> ret = new ArrayList<>() ;
        for(Card card : cards) {
            if(card.getType() == CardType.MINION)
                ret.add(card) ;
        }
        return ret ;
    }

    public String findTheBestMatch(ArrayList<String> arr , String str) {
        int mn = 1000 * 1000 * 1000 ;
        String ret = "" ;
        for(String match : arr) {
            int n = match.length() ;
            int m = str.length() ;
            int [][] dis = new int [n + 1][m + 1] ;
            for(int i = 0 ; i < n + 1 ; ++i)
                for(int j = 0 ; j < m + 1 ; ++j)
                    dis[i][j] = 0 ;
            for(int i = 0 ; i <= n ; ++i) {
                for(int j = 0 ; j <= m ; ++j) {
                    if(Math.min(i , j) == 0) {
                        dis[i][j] = Math.max(i , j) ;
                        continue ;
                    }
                    dis[i][j] = Math.min(dis[i - 1][j] + 1 , dis[i][j - 1] + 1) ;
                    if(match.charAt(i - 1) != str.charAt(j - 1))
                        dis[i][j] = Math.min(dis[i][j] , dis[i - 1][j - 1] + 1) ;
                    else
                        dis[i][j] = Math.min(dis[i][j] , dis[i - 1][j - 1]) ;
                }
            }
            if(mn > dis[n][m]) {
                mn = dis[n][m] ;
                ret = match ;
            }
        }
        return ret ;
    }

    public Player getPlayerByUsername(String username) {
        for(Player player : players) {
            if(player.getUsername().equals(username))
                return player ;
        }
        return null ;
    }

    public Card getCardByName(String nameOfCard) {
        for (Card loopCard : cards) {
            if (loopCard.getName().equals(nameOfCard)) {
                return loopCard;
            }
        }
        return null;
    }

    public ArrayList<Card> getCardByName(ArrayList<String> nameOfCards) {
        ArrayList<Card> ret = new ArrayList<>();
        if (nameOfCards.isEmpty()) return ret;
        for (String nameOfCard : nameOfCards) {
            ret.add(getCardByName(nameOfCard));
        }
        return ret;
    }

    private String ask(String question) {
        System.out.println(Location.ANSI_GREEN + question + Location.ANSI_RESET);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    public void error(String error) {
        System.out.println(error);
    }

    private String removeJSONFromFile(String name) {
        String ret = "";
        for (int i = 0; i < name.length(); ++i) {
            if (name.charAt(i) == '.') {
                break;
            }
            ret += name.charAt(i);
        }
        return ret;
    }

    public void init() {
        heroes.clear();
        players.clear();
        cards.clear();
        //add heroes
        File heroesFolder = new File("Heroes");
        heroesFolder.mkdir() ;
        File[] listOfHeroesFiles = heroesFolder.listFiles();
        for (File heroFile : listOfHeroesFiles) {
            if (heroFile.isFile()) {
                String name = heroFile.getName();
                name = removeJSONFromFile(name);
                heroes.add(new Hero(name));
            }
        }

        //add minions
        File minionsFolder = new File("Cards" + File.separator + "Minions");
        minionsFolder.mkdir() ;
        File[] listOfMinionsFiles = minionsFolder.listFiles();
        for (File minionFile : listOfMinionsFiles) {
            if (minionFile.isFile()) {
                String name = minionFile.getName();
                name = removeJSONFromFile(name);
                cards.add(new Minion(name));
            }
        }

        //add spells
        File spellsFolder = new File("Cards" + File.separator + "Spells");
        spellsFolder.mkdir() ;
        File[] listOfSpellsFiles = spellsFolder.listFiles();
        for (File spellFile : listOfSpellsFiles) {
            if (spellFile.isFile()) {
                String name = spellFile.getName();
                name = removeJSONFromFile(name);
                cards.add(new Spell(name));
            }
        }

        //add weapons
        File weaponFolder = new File("Cards" + File.separator + "Weapon");
        weaponFolder.mkdir() ;
        File[] listOfWeaponFiles = weaponFolder.listFiles();
        for (File weaponFile : listOfWeaponFiles) {
            if (weaponFile.isFile()) {
                String name = weaponFile.getName();
                name = removeJSONFromFile(name);
                cards.add(new Weapon(name));
            }
        }

        //add quests
        File questFolder = new File("Cards" + File.separator + "Quest");
        questFolder.mkdir() ;
        File[] listOfQuestFiles = questFolder.listFiles();
        for (File questFile : listOfQuestFiles) {
            if (questFile.isFile()) {
                String name = questFile.getName();
                name = removeJSONFromFile(name);
                cards.add(new Quest(name));
            }
        }

        //Players
        File playersFile = new File("Users");
        playersFile.mkdir() ;
        File[] listOfPlayersFiles = playersFile.listFiles();
        Player.numberOfUsers = listOfPlayersFiles.length ;
        for (File playerFile : listOfPlayersFiles) {
            Player player = Player.getPlayerByJSON(playerFile) ;
            if(player.isDeleted()) continue ;
            players.add(player);
        }
    }

    public boolean login(String username , String password) {
        password = String.valueOf(Password.hash(password));
        for (Player player : players) {
            if (player.getUsername().equals(username) && player.getPassword().equals(password)) {
                getWorld().setCurPlayer(player);
            }
        }
        if (curPlayer == null) {
            return false ;
        } else {
            curPlayer.appendEventWithDate("login") ;
            curPlayer.setOnline(true) ;
            return true ;
        }
    }

    public boolean register(String username, String password) {
        for (Player player : players) {
            if (player.getUsername().equals(username)) {
                return false ;
            }
        }
        Player player = new Player(username, password);
        getWorld().setCurPlayer(player);
        players.add(player);
        return true ;
    }


    public void logout(String username) {
        for(Player player : players) {
            if(player.getUsername().equals(username)) {
                player.setOnline(false) ;
            }
        }
    }


   /* public void start() {
        System.out.println(Location.ANSI_RED + "welcome to hearthstone!" + Location.ANSI_RESET);
        init();
        curPlayer = null;
        if (ask("already have an account?").equals("y")) {
            String username = ask("Please enter your username : ");
            String password = ask("please enter your password : ");
            password = String.valueOf(Password.hash(password));
            for (Player player : players) {
                if (player.getUsername().equals(username) && player.getPassword().equals(password)) {
                    getWorld().setCurPlayer(player);
                }
            }
            if (curPlayer == null) {
                error("wrong username or password");
                start();
                return ;
            } else {
                curPlayer.appendEventWithDate("login") ;
            }
        } else {
            String username = ask("Please enter your username : ");
            String password = ask("please enter your password : ");
            for (Player player : players) {
                if (player.getUsername().equals(username)) {
                    error("this username is already exists");
                    start();
                    return;
                }
            }
            Player player = new Player(username, password);
            getWorld().setCurPlayer(player);
            players.add(player);
        }
        Menu.getMenu().commandHandler();
    }*/

    //inner class

    //getter and setter
    public static World getWorld() {
        if (world == null)
            world = new World();
        return world;
    }

    public ArrayList<Player> getPlayers() {
        players.clear();
        //Players
        File playersFile = new File("Users");
        File[] listOfPlayersFiles = playersFile.listFiles();
        Player.numberOfUsers = listOfPlayersFiles.length ;
        for (File playerFile : listOfPlayersFiles) {
            Player player = Player.getPlayerByJSON(playerFile) ;
            if(player.isDeleted()) continue ;
            players.add(player);
        }
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public ArrayList<Card> getCards() {
        cards.clear();
        //add minions
        File minionsFolder = new File("Cards/Minions");
        File[] listOfMinionsFiles = minionsFolder.listFiles();
        for (File minionFile : listOfMinionsFiles) {
            if (minionFile.isFile()) {
                String name = minionFile.getName();
                name = removeJSONFromFile(name);
                cards.add(new Minion(name));
            }
        }

        //add spells
        File spellsFolder = new File("Cards/Spells");
        File[] listOfSpellsFiles = spellsFolder.listFiles();
        for (File spellFile : listOfSpellsFiles) {
            if (spellFile.isFile()) {
                String name = spellFile.getName();
                name = removeJSONFromFile(name);
                cards.add(new Spell(name));
            }
        }
        return cards;
    }

    public void setCard(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public ArrayList<Hero> getHeroes() {
        heroes.clear();
        //add heroes
        File heroesFolder = new File("Heroes/");
        File[] listOfHeroesFiles = heroesFolder.listFiles();
        for (File heroFile : listOfHeroesFiles) {
            if (heroFile.isFile()) {
                String name = heroFile.getName();
                name = removeJSONFromFile(name);
                heroes.add(new Hero(name));
            }
        }
        return heroes;
    }

    public void setHeroes(ArrayList<Hero> heroes) {
        this.heroes = heroes;
    }

    public Player getCurPlayer() {
        return curPlayer;
    }

    public void setCurPlayer(Player curPlayer) {
        this.curPlayer = curPlayer;
    }
}
