package graphic.selectPassiveDeck;

import Player.Deck;
import Player.Player;
import World.World;
import com.google.gson.Gson;
import com.jfoenix.controls.JFXButton;
import game.GamePanel;
import game.InfoPassive;
import graphic.images.Images;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.ResourceBundle;

public class Controller  {
    private Player player ;
    private Client client;
    private String type ;

    @FXML
    private JFXButton twiceDrawButton;

    @FXML
    private AnchorPane deckScrollPane;

    @FXML
    private JFXButton offCardsButton;

    @FXML
    private JFXButton warriorsButton;

    @FXML
    private JFXButton nurseButton;

    @FXML
    private JFXButton freePowerButton;

    @FXML
    private JFXButton manaJumpButton;

    private ArrayList<ImageView> images = new ArrayList<>() ;

    private void setSelectedDeck(int ind) {
        ArrayList<Deck> decks = player.getDecks() ;
        for(int i = 0 ; i < decks.size() ; ++i) {
            if(i == ind) {
                GaussianBlur effect = new GaussianBlur(0) ;
                images.get(i).setEffect(effect) ;
            } else {
                GaussianBlur effect = new GaussianBlur(10) ;
                images.get(i).setEffect(effect) ;
            }
        }
    }

    final InfoPassive[] infoPassive = {null};

    @FXML
    void play(ActionEvent event) throws Exception {
        if(infoPassive[0] == null || player.getCurDeck() == null || player.getCurDeck().size() == 0)
            return ;
        player.setInfoPassive(infoPassive[0]) ;
        String gameType = "onlinePlay" ;
        if(type.equals("MultiPlayer"))
            gameType = "singlePlay" ;
        client.sendMessage(MyJson.create_json(
                "type", gameType,
                "username", client.getUsername())) ;
        //TODO start game panel
    }


    public void init() {
        player.setCurDeck(null) ;
        ArrayList<Deck> decks = player.getDecks() ;
        for(int i = 0 ; i < decks.size() ; ++i) {
            Deck deck = decks.get(i) ;
            ImageView deckImage = Images.getInstance().getHeroImageView(deck.getHero()) ;
            deckImage.setFitWidth(565) ;
            deckImage.setFitHeight(124) ;
            deckImage.setLayoutY(i * 124) ;
            deckImage.setLayoutX(0) ;
            deckScrollPane.getChildren().add(deckImage) ;
            final int finalI = i ;
            images.add(deckImage) ;
            deckImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                player.setCurDeck(deck) ;
                setSelectedDeck(finalI) ;
            }) ;
            Text nameOfDeck = new Text(deck.getName());
            nameOfDeck.setLayoutX(39);
            nameOfDeck.setLayoutY(63 * (2 * i + 1));
            nameOfDeck.setFont(Font.font("Verdana", FontPosture.ITALIC, 32));
            nameOfDeck.setFill(Color.WHITE);
            deckScrollPane.getChildren().add(nameOfDeck);
        }


        //add buttons for passive
        ArrayList<JFXButton> infoPassives = new ArrayList<>() ;
        infoPassives.add(twiceDrawButton) ;
        infoPassives.add(offCardsButton) ;
        infoPassives.add(warriorsButton) ;
        infoPassives.add(nurseButton) ;
        infoPassives.add(freePowerButton
        ) ;
        infoPassives.add(manaJumpButton) ;
        Collections.shuffle(infoPassives) ;
        for(int i = 3 ; i < 6 ; ++i)
            infoPassives.get(i).setDisable(true) ;
        for(int i = 0 ; i < 3 ; ++i)
            infoPassives.get(i).setDisableVisualFocus(true) ;
        twiceDrawButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            infoPassive[0] = InfoPassive.TWICE_DRAW ;
            twiceDrawButton.setDisableVisualFocus(true) ;

            for(JFXButton jfxButton : infoPassives)
                jfxButton.setStyle("") ;
            twiceDrawButton.setStyle("-fx-background-color: red ;") ;

        }) ;

        offCardsButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            infoPassive[0] = InfoPassive.OFF_CARDS ;
            offCardsButton.setDisableVisualFocus(true) ;

            for(JFXButton jfxButton : infoPassives)
                jfxButton.setStyle("") ;
            offCardsButton.setStyle("-fx-background-color: red ;") ;

        }) ;

        warriorsButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            infoPassive[0] = InfoPassive.WARRIORS ;
            warriorsButton.setDisableVisualFocus(true) ;

            for(JFXButton jfxButton : infoPassives)
                jfxButton.setStyle("") ;
            warriorsButton.setStyle("-fx-background-color: red ;") ;

        }) ;

        nurseButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            infoPassive[0] = InfoPassive.NURSE ;
            nurseButton.setDisableVisualFocus(true) ;

            for(JFXButton jfxButton : infoPassives)
                jfxButton.setStyle("") ;
            nurseButton.setStyle("-fx-background-color: red ;") ;

        }) ;

        freePowerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            infoPassive[0] = InfoPassive.FREE_POWER ;
            freePowerButton.setDisableVisualFocus(true) ;

            for(JFXButton jfxButton : infoPassives)
                jfxButton.setStyle("") ;
            freePowerButton.setStyle("-fx-background-color: red ;") ;

        }) ;

        manaJumpButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            infoPassive[0] = InfoPassive.MANA_JUMP ;
            manaJumpButton.setDisableVisualFocus(true) ;

            for(JFXButton jfxButton : infoPassives)
                jfxButton.setStyle("") ;
            manaJumpButton.setStyle("-fx-background-color: red ;") ;

        }) ;
    }

    public void setJson(JSONObject jsonObject) {
        if(jsonObject == null)
            return ;
        Gson gson = new Gson() ;
        this.player = (Player) gson.fromJson(((String) jsonObject.get("player")), Player.class);
        this.type = (String) jsonObject.get("gameType") ;
        init();
    }

    public void setClient(Client client) {
        this.client = client ;
    }
}
