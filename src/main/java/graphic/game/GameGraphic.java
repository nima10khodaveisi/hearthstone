package graphic.game;

import game.players.gamePlayer.GamePlayer;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.Main;
import network.client.Client;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

public class GameGraphic {
    private Drawer drawer;
    private Client client;
    private Stage stage;

    public GameGraphic(Client client) {
        //load graphic
        Platform.runLater(() -> {
            stage = client.getStage();
            this.client = client;
            try {
                URL url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "game" + File.separator + "game.fxml").toUri().toURL();
                FXMLLoader loader = new FXMLLoader(url);
                Parent root = loader.load();
                Scene scene = new Scene(root, 1280, 720);
                drawer = loader.getController();
                drawer.setClient(client);
                stage.setScene(scene);
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void updateGameGraphic(GamePlayer myPlayer, GamePlayer enemyPlayer, boolean isMyTurn, boolean watcher) {
        drawer.drawPlayer(myPlayer, enemyPlayer, isMyTurn, watcher);
    }


    public Drawer getDrawer() {
        return drawer;
    }

    public void setDrawer(Drawer drawer) {
        this.drawer = drawer;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
