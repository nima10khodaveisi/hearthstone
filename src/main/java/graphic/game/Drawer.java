package graphic.game;

import Models.Cards.Card;
import game.actions.Attack;
import game.models.Hand;
import game.models.HeroGame;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;
import game.utilities.Timer;
import graphic.images.Images;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.util.Duration;
import network.client.Client;
import network.json.MyJson;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Drawer implements Initializable {

    private ArrayList<ImageView> manaImages = new ArrayList<>();
    private ArrayList<ImageView> handCardImages = new ArrayList<>();
    private ArrayList<ImageView> otherHandImages = new ArrayList<>();
    private Hand curPlayerHand;
    private Hand otherPlayerHand;
    private GamePlayer curPlayer;
    private GamePlayer otherPlayer;
    private Client client;
    private boolean isMyTurn ;
    private boolean ended ;

    //FXML


    @FXML
    private Pane pane;

    @FXML
    private ImageView backgroundImage;

    @FXML
    private ImageView handImage0;

    @FXML
    private ImageView handImage1;

    @FXML
    private ImageView handImage2;

    @FXML
    private ImageView handImage3;

    @FXML
    private ImageView rightArrowImage;

    @FXML
    private ImageView leftArrowImage;

    @FXML
    private ImageView bigHandImage2;

    @FXML
    private ImageView bigHandImage3;

    @FXML
    private ImageView bigHandImage1;

    @FXML
    private ImageView bigHandImage0;

    @FXML
    private Text manaText;

    @FXML
    private ScrollPane logScrollPane;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private Text errorText;

    @FXML
    private ImageView heroPowerImage;

    @FXML
    private Text playerId;

    @FXML
    private ImageView otherHandImage0;

    @FXML
    private ImageView otherHandImage1;

    @FXML
    private ImageView otherHandImage2;

    @FXML
    private ImageView otherHandImage3;

    @FXML
    private ImageView otherRightArrowImage;

    @FXML
    private ImageView otherLeftArrowImage;

    @FXML
    private HBox otherSummonedBox;

    @FXML
    private HBox summonedBox;

    @FXML
    private ImageView otherHeroPowerImage;

    @FXML
    private Text timerText;

    @FXML
    private Pane curPlayerHero;

    @FXML
    private Pane otherPlayerHero;

    @FXML
    private Pane weaponPane;

    @FXML
    private Pane otherWeaponPane;


    @FXML
    private ImageView discover1;

    @FXML
    private ImageView discover2;

    @FXML
    private ImageView discover3;

    private Attack attack ;
    private boolean watcher ;

    //functions

    public void drawPlayer(GamePlayer player, GamePlayer otherPlayer, boolean isMyTurn, boolean watcher) {
        this.isMyTurn = isMyTurn ;
        this.watcher = watcher ;
        this.curPlayer = player;
        this.otherPlayer = otherPlayer;
        setEnded(false) ;
        Platform.runLater(() -> {
            attack = new Attack(player, client) ;
            otherPlayer.setAttack(attack) ;
            player.setAttack(attack) ;
            player.setGraphical(true);
            otherPlayer.setGraphical(true);
            player.setCharacters(new ArrayList<>());
            otherPlayer.setCharacters(new ArrayList<>());
            for (SummonCard summonCard : player.getSummoned().getCards())
                player.getCharacters().add(summonCard);
            player.getCharacters().add(player.getHero()) ;

            for(SummonCard summonCard : otherPlayer.getSummoned().getCards()) {
                otherPlayer.getCharacters().add(summonCard) ;
            }
            otherPlayer.getCharacters().add(otherPlayer.getHero()) ;
            player.getSummoned().setPlayer(player);
            otherPlayer.getSummoned().setPlayer(otherPlayer);
            player.getHero().setPlayer(player);
            otherPlayer.getHero().setPlayer(otherPlayer);
            updateHand(player.getHand());
            setHero(player.getHero());
            setMana(player.getMana(), player.getTurn());
            player.getSummoned().sethBox(summonedBox);
            otherPlayer.getSummoned().sethBox(otherSummonedBox);
            //TODO set hero power for both of players

            //other player data
            otherShowHand(otherPlayer.getHand(), this.watcher);
            otherSetHero(otherPlayer.getHero());

            for (ImageView imageView : handCardImages)
                imageView.setDisable(!isMyTurn);
            player.initHeropwer(player.getHero()) ;
            otherPlayer.initHeropwer(otherPlayer.getHero()) ;
            if (otherPlayer.getHeroPower() != null)
                otherPlayer.getHeroPower().setPane(otherHeroPowerImage);

            if (player.getHeroPower() != null)
                player.getHeroPower().setPane(heroPowerImage);


            this.playerId.setText(player.getPlayer().getUsername()) ;

            /*
            if(isMyTurn) {
                Timer timer = new Timer(this);
                timer.start();
            }*/
        });
    }

    public void setTimerText(int timer) {
        if (timer == 60) {
            timerText.setText(null);
            return;
        }
        String str = String.valueOf(timer);
        if (str.length() == 1)
            str = "00:0" + str;
        else
            str = "00:" + str;
        timerText.setText(str);
    }

    public void setErrorText(String message) {
        errorText.setText(message);
        errorText.setVisible(true);
        PauseTransition wait = new PauseTransition(Duration.seconds(1));
        wait.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                errorText.setText(null);
                errorText.setVisible(false);
            }
        });
        wait.play();
    }

    public void setHero(HeroGame hero) {
        hero.setPane(curPlayerHero);
        //TODO set weapon pane
        hero.setWeaponPane(weaponPane) ;
    }

    public void otherSetHero(HeroGame hero) {
        hero.setPane(otherPlayerHero);
        //TODO set weapon pane
        hero.setWeaponPane(otherWeaponPane) ;
    }

    public void setMana(int mana, int turn) {
        if (turn > 10)
            turn = 10;
        manaText.setText(mana + "/" + turn);
        for (int i = 1; i <= turn; ++i) {
            String name = "";
            if (i > mana) {
                name = "offMana";
            } else {
                name = "Mana";
            }
            ImageView imageView = new ImageView(Images.getInstance().getManaImageView(name));
            imageView.setLayoutX(i * 25 + 905);
            imageView.setLayoutY(655);
            imageView.setFitHeight(25);
            imageView.setFitWidth(25);
            pane.getChildren().add(imageView);
        }
    }

    public void updateHand(ArrayList<Card> hand) {
        curPlayerHand = new Hand(hand, false);
        showHand();
    }

    private void showHand() {
        Platform.runLater(() -> {
            int i = 0;
            for (ImageView imageView : handCardImages)
                imageView.setImage(null);
            for (ImageView imageView : handCardImages) {
                if (i >= curPlayerHand.getImages().size())
                    break;
                imageView.setImage(curPlayerHand.getImages().get(i).getImage());
                i++;
            }
        });
    }

    public void otherShowHand(ArrayList<Card> otherHand, boolean watcher) {
        if (otherHand != null)
            otherPlayerHand = new Hand(otherHand, true ^ watcher);
        for (ImageView imageView : otherHandImages)
            imageView.setImage(null);
        int i = 0;
        for (ImageView imageView : otherHandImages) {
            if (i >= otherPlayerHand.getImages().size())
                break;
            imageView.setImage(otherPlayerHand.getImages().get(i).getImage());
            i++;
        }
    }

    private Text textLogs = new Text();

    public void updateLogs(String logs) {
        String str = textLogs.getText();
        str += '\n' + logs;
        textLogs.setText(str);
        textLogs.setFont(Font.font("Verdana", FontPosture.ITALIC, 14));
        textLogs.setFill(Color.WHITE);
    }

    @FXML
    void showLogs(MouseEvent event) {
        logScrollPane.setVisible(!logScrollPane.isVisible());
    }


    public int discover(Card card1, Card card2, Card card3) {
        //pane.setDisable(true) ;
        discover1.setImage(Images.getInstance().getCardImage(card1));
        discover2.setImage(Images.getInstance().getCardImage(card2));
        discover3.setImage(Images.getInstance().getCardImage(card3));
        discover1.setDisable(false);
        discover2.setDisable(false);
        discover3.setDisable(false);
        final int[] ret = {0};
/*
        discover1.removeEventHandler(MouseEvent.MOUSE_CLICKED, discover1.getOnMouseClicked()) ;
        discover3.removeEventHandler(MouseEvent.MOUSE_CLICKED, discover2.getOnMouseClicked()) ;
        discover3.removeEventHandler(MouseEvent.MOUSE_CLICKED, discover3.getOnMouseClicked()) ;
*/

        discover1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ret[0] = 1;

                discover1.setDisable(true);
                discover2.setDisable(true);
                discover3.setDisable(true);
                //pane.setDisable(false) ;
                discover1.setImage(null);
                discover2.setImage(null);
                discover3.setImage(null);
            }
        });
        discover2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ret[0] = 2;
                discover1.setDisable(true);
                discover2.setDisable(true);
                discover3.setDisable(true);
                //pane.setDisable(false) ;
                discover1.setImage(null);
                discover2.setImage(null);
                discover3.setImage(null);
            }
        });
        discover3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ret[0] = 3;
                discover1.setDisable(true);
                discover2.setDisable(true);
                discover3.setDisable(true);
                //pane.setDisable(false) ;
                discover1.setImage(null);
                discover2.setImage(null);
                discover3.setImage(null);
            }
        });

        return ret[0];
    }


    public void drawCard(Card card) {
    }

    private void moveByDrag(ImageView imageView, ImageView bigHandImage, int ind) {

        final double[] orgSceneX = {0};
        final double[] orgSceneY = {0};
        final double[] orgTranslateX = {0};
        final double[] orgTranslateY = {0};
        final double[] initX = {imageView.getLayoutX()};
        final double[] initY = {imageView.getLayoutY()};

        EventHandler<MouseEvent> drag = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                bigHandImage.setVisible(false);
                double offX = event.getSceneX() - orgSceneX[0];
                double offY = event.getSceneY() - orgSceneY[0];
                double newTX = orgTranslateX[0] + offX;
                double newTY = orgTranslateY[0] + offY;
                ((ImageView) (event.getSource())).setTranslateX(newTX);
                ((ImageView) (event.getSource())).setTranslateY(newTY);
            }
        };

        EventHandler<MouseEvent> press = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                orgSceneX[0] = event.getSceneX();
                orgSceneY[0] = event.getSceneY();
                orgTranslateX[0] = ((ImageView) (event.getSource())).getTranslateX();
                orgTranslateY[0] = ((ImageView) (event.getSource())).getTranslateY();
            }
        };

        EventHandler<MouseEvent> drop = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int curInd = (ind + curPlayerHand.getInd());
                if (event.getSceneX() >= 68 && event.getSceneX() <= 68 + 1172 && event.getSceneY() <= 343 + 199 && event.getSceneY() >= 343) {
                    try {
                        String json = MyJson.create_json(
                                "type", "summonCard",
                                "ind", curInd,
                                "dropX", event.getSceneX()).toJSONString();
                        //curPlayer.getClientHandler().getSender().sendMessage(json);
                        client.getGameSender().sendMessage(json);
                    } catch (Exception e) {
                        //summon size if full or there is no enough mana
                    }
                }
                imageView.setTranslateX(0);
                imageView.setTranslateY(0);
            }
        };

        imageView.setOnMousePressed(press);
        imageView.setOnMouseDragged(drag);
        imageView.setOnMouseReleased(drop);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initialize_hand();
        initialize_summon();
        anchorPane.getChildren().add(textLogs);
        backgroundImage.setImage(Images.getInstance().getGameBackground().getImage());
    }

    private void initialize_summon() {
        summonedBox.getChildren().clear();
        otherSummonedBox.getChildren().clear();
    }

    private void initialize_hand() {
        //set hand card images
        handCardImages.add(handImage0);
        handCardImages.add(handImage1);
        handCardImages.add(handImage2);
        handCardImages.add(handImage3);

        for (ImageView imageView : handCardImages)
            imageView.setImage(null);

        otherHandImages.add(otherHandImage0);
        otherHandImages.add(otherHandImage1);
        otherHandImages.add(otherHandImage2);
        otherHandImages.add(otherHandImage3);

        for (ImageView imageView : otherHandImages)
            imageView.setImage(null);

        //set big images of hand's cards
        setBigImage(handImage0, bigHandImage0);
        setBigImage(handImage1, bigHandImage1);
        setBigImage(handImage2, bigHandImage2);
        setBigImage(handImage3, bigHandImage3);

        //drag
        moveByDrag(handImage0, bigHandImage0, 0);
        moveByDrag(handImage1, bigHandImage1, 1);
        moveByDrag(handImage2, bigHandImage2, 2);
        moveByDrag(handImage3, bigHandImage3, 3);
        //set hand pages
        rightArrowImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            curPlayerHand.addInd();
            showHand();
        });
        leftArrowImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            curPlayerHand.redInd();
            showHand();
        });
        otherLeftArrowImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            otherPlayerHand.redInd();
            otherShowHand(null, this.watcher);
        });
        otherRightArrowImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            otherPlayerHand.addInd();
            otherShowHand(null, this.watcher);
        });

    }

    private void setBigImage(ImageView imageView, ImageView bigImage) {
        imageView.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
            bigImage.setImage(imageView.getImage());
            bigImage.setVisible(true);
        });

        imageView.addEventHandler(MouseEvent.MOUSE_EXITED, event -> {
            bigImage.setImage(null);
            bigImage.setVisible(false);
        });
    }


    public void changeCurPlayerText() {
        if (playerId.getText().equals("Player 1"))
            playerId.setText("Player 2");
        else
            playerId.setText("Player 1");
    }


    @FXML
    void end_turn(MouseEvent event) {
    /*    System.out.println(curPlayer.getPlayer().getUsername());
        System.out.println(curPlayer.getClientHandler());
        System.out.println(curPlayer.getClientHandler().getSender());
        curPlayer.getClientHandler().getSender().sendMessage(MyJson.create_json(
                "type", "endTurn").toJSONString());
        //    Mapper.getInstance().addRequest(new Mapper.EndTurn());
     */
        if(!isMyTurn)
            return;
        setEnded(true) ;
        client.getGameSender().sendMessage(MyJson.create_json(
                "type", "endTurn").toJSONString());
    }

    public void endTimeTurn() {
        if(!isMyTurn)
            return;
        setEnded(true) ;
        client.getGameSender().sendMessage(MyJson.create_json(
                "type", "endTurn").toJSONString());
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Attack getAttack() {
        return attack;
    }

    public void setAttack(Attack attack) {
        this.attack = attack;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }
}

/*
TODO
3 - show messages
4 - maybe add animations
5 - set hero hp
 */