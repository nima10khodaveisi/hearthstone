package graphic.shop;

import Models.Cards.Card;
import Player.Player;
import World.World;
import graphic.images.Images;
import graphic.menu.Menu;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import graphic.shop.Shop;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.SocketHandler;

public class Controller implements Initializable {

    public int curPage = 0;
    public int curCardNumber = 0;
    private Player player ;
    private Client client;

    @FXML
    private ImageView imageCard1;

    @FXML
    private ImageView imageCard2;

    @FXML
    private ImageView imageCard3;

    @FXML
    private ImageView imageCard4;

    @FXML
    private ImageView imageCard5;

    @FXML
    private ImageView imageCard6;

    @FXML
    private ImageView rightArrow;

    @FXML
    private ImageView leftArrow;

    @FXML
    private ImageView imageCurCard;

    @FXML
    private Text textNumberOfCurCard;

    @FXML
    private ImageView imagePriceCurCard;

    @FXML
    private Text textPriceOfCurCard;

    @FXML
    private Text textHeroCurCard;

    @FXML
    private Text textHeroOfCurCard;

    @FXML
    private Button buyButton;

    @FXML
    private Button sellButton;

    @FXML
    private Text textCurPlayerCoins;

    public void drawPage(int pageNumber) throws MalformedURLException {
        ArrayList<Card> cards = Shop.cardsOfPage.get(pageNumber);
        int size = Shop.cardsOfPage.size();
        ArrayList<ImageView> imageViews = new ArrayList<>();
        imageViews.add(imageCard1);
        imageViews.add(imageCard2);
        imageViews.add(imageCard3);
        imageViews.add(imageCard4);
        imageViews.add(imageCard5);
        imageViews.add(imageCard6);
        for (ImageView image : imageViews) {
            image.setDisable(true);
            image.setImage(null);
        }
        for (int i = 0; i < cards.size(); ++i) {
            Card card = cards.get(i);
            ImageView image = imageViews.get(i);
            image.setDisable(false);
            image.setImage(Images.getInstance().getCardImage(card));
        }
        if (pageNumber == 0) {
            leftArrow.setDisable(true);
            leftArrow.setVisible(false);
        } else {
            leftArrow.setDisable(false);
            leftArrow.setVisible(true);
        }
        if (pageNumber == size - 1) {
            rightArrow.setDisable(true);
            rightArrow.setVisible(false);
        } else {
            rightArrow.setDisable(false);
            rightArrow.setVisible(true);
        }
        if (player != null)
            textCurPlayerCoins.setText(String.valueOf(player.getCoins()));
    }


    @FXML
    void drawNextPage(MouseEvent event) throws MalformedURLException {
        curPage++;
        drawPage(curPage);
    }

    @FXML
    void drawPreviousPage(MouseEvent event) throws MalformedURLException {
        curPage--;
        drawPage(curPage);
    }

    @FXML
    void buyCard(MouseEvent event) throws MalformedURLException {
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        player.buy(card);
        drawPage(curPage);
        drawCard(card);

        client.sendMessage(MyJson.create_json(
                "type", "shop",
                "query", "buy",
                "username", client.getUsername(),
                "card", card.getName())) ;
    }

    @FXML
    void sellCard(MouseEvent event) throws MalformedURLException {
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        player.sell(card);
        drawPage(curPage);
        drawCard(card);

        client.sendMessage(MyJson.create_json(
                "type", "shop",
                "query", "sell",
                "username", client.getUsername(),
                "card", card.getName())) ;
    }


    public void drawCard(Card card) throws MalformedURLException {
        imageCurCard.setImage(Images.getInstance().getCardImage(card));
        textHeroOfCurCard.setText(card.getHeroClass());
        textPriceOfCurCard.setText(String.valueOf(card.getPrice()));

        if (player.canSell(card)) {
            sellButton.setDisable(false);
        } else {
            sellButton.setDisable(true);
        }

        if (player.canBuy(card)) {
            buyButton.setDisable(false);
        } else {
            buyButton.setDisable(true);
        }
        int cnt = 0;
        for (Card x : player.getCards()) {
            if (x.getName().equals(card.getName())) {
                cnt++;
            }
        }
        textNumberOfCurCard.setText(String.valueOf(cnt) + "X");
    }

    @FXML
    void drawCurImage1(MouseEvent event) throws MalformedURLException {
        curCardNumber = 0;
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        drawCard(card);
    }

    @FXML
    void drawCurImage2(MouseEvent event) throws MalformedURLException {
        curCardNumber = 1;
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        drawCard(card);
    }


    @FXML
    void drawCurImage3(MouseEvent event) throws MalformedURLException {
        curCardNumber = 2;
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        drawCard(card);
    }


    @FXML
    void drawCurImage4(MouseEvent event) throws MalformedURLException {
        curCardNumber = 3;
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        drawCard(card);
    }


    @FXML
    void drawCurImage5(MouseEvent event) throws MalformedURLException {
        curCardNumber = 4;
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        drawCard(card);
    }


    @FXML
    void drawCurImage6(MouseEvent event) throws MalformedURLException {
        curCardNumber = 5;
        ArrayList<Card> cards = Shop.cardsOfPage.get(curPage);
        Card card = cards.get(curCardNumber);
        drawCard(card);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        player = Shop.curPlayer ;
        if(Shop.selectedCard != null) {
            for(int i = 0 ; i < Shop.cards.size() ; ++i) {
                Card x = Shop.cards.get(i) ;
                if(x.getName().equals(Shop.selectedCard.getName())) {
                    try {
                        curPage = i / 6 ;
                        drawPage(i / 6) ;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    try {
                        curCardNumber = i % 6 ;
                        drawCard(Shop.selectedCard) ;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    break ;
                }
            }
        } else {
            try {
                drawPage(0);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    public void setClient(Client client) { this.client = client; }
}
