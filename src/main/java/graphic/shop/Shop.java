package graphic.shop;

import Models.Cards.Card;
import Player.Player;
import World.World;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import main.Main;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Shop {
    public static Stage stage;
    public static Card selectedCard = null;
    public static Player curPlayer;
    public static ArrayList<Card> cards;
    public static ArrayList<ArrayList<Card>> cardsOfPage = new ArrayList<ArrayList<Card>>();
    public static ArrayList<Card> cur = new ArrayList<>();

    public static void show(Scene backScene, Card selectedCar, JSONObject jsonObject, Client client) {
        Platform.runLater(() -> {

            if (jsonObject == null) {
                client.sendMessage(MyJson.create_json(
                        "type", "shop",
                        "username", client.getUsername(),
                        "query", "show"));

                return;
            }

            Gson gson = new Gson();
            curPlayer = (Player) gson.fromJson(((String) jsonObject.get("player")), Player.class);
            cards = (ArrayList<Card>) gson.fromJson((String) jsonObject.get("allCards"), new TypeToken<ArrayList<Card>>(){}.getType()) ;
            stage = client.getStage();


            selectedCard = selectedCar;
            for (Card card : cards) {
                if (cur.size() == 6) {
                    ArrayList<Card> add = new ArrayList<>(cur);
                    cardsOfPage.add(add);
                    cur.clear();
                }
                cur.add(card);
            }
            if (!cur.isEmpty()) {
                ArrayList<Card> add = new ArrayList<>(cur);
                cardsOfPage.add(add);
                cur.clear();
            }

            try {

                URL url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "shop" + File.separator + "shop.fxml").toUri().toURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                Parent root = fxmlLoader.load() ;
                Controller controller = fxmlLoader.getController() ;
                controller.setClient(client) ;
                Scene scene = new Scene(root, 1280, 720);
                scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent keyEvent) {
                        if (keyEvent.getCode() == KeyCode.ESCAPE) {
                            curPlayer.appendEventWithDate("back to menu");
                            stage.setScene(backScene);
                        }
                    }
                });
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
