package graphic.getServerData;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import network.client.ClientMain;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private ClientMain clientMain ;

    @FXML
    private TextField serverIPText ;

    @FXML
    private TextField serverPortText ;

    @FXML
    private Button submitButton ;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        submitButton.setOnMouseClicked(event -> {
            String serverIP = serverIPText.getText() ;
            if(serverIP == null || serverIP.isEmpty())
                return;
            String serverPort = serverPortText.getText() ;
            if(serverPort == null || serverPort.isEmpty())
                return;
            int port = Integer.valueOf(serverPort) ;
            clientMain.setServerData(serverIP, port) ;
        }) ;
    }

    public void setClientMain(ClientMain clientMain) {
        this.clientMain = clientMain;
    }
}
