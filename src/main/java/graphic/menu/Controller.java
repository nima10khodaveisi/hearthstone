package graphic.menu;


import Player.Player;
import World.World;
import game.GameState;
import graphic.collections.Collections;
import graphic.friendship.Friendship;
import graphic.login.Login;
import graphic.selectPassiveDeck.Select;
import graphic.setting.Setting;
import graphic.shop.Shop;
import graphic.status.Status;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.Main;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller {

    private Client client;
    private JSONObject jsonObject;
    private Stage stage ;

    @FXML
    private Text username;


    @FXML
    void collectionPage(ActionEvent event) throws IOException {
        Collections.show(client, null);
    }

    @FXML
    void botPlayer(ActionEvent event) throws IOException {
        Select.show(client, null, "MultiPlayer") ;
    }

    @FXML
    void playPage(ActionEvent event) throws IOException {
        Select.show(client, null, "OnlineGame");
    }

    @FXML
    void settingPage(ActionEvent event) throws IOException {
        Setting.show(client) ;
    }

    @FXML
    void shopPage(ActionEvent event) throws IOException {
        Shop.show(stage.getScene() , null, null, client);
    }

    @FXML
    void statusPage(ActionEvent event) throws IOException {
        Status.show();
    }


    @FXML
    void exitGame(ActionEvent event) {
        client.sendMessage(MyJson.create_json(
                "type", "logout",
                    "username", client.getUsername())) ;
        stage.close();
        System.exit(0) ;
    }

    @FXML
    void logoutUser(ActionEvent event) throws IOException {
        client.sendMessage(MyJson.create_json(
                "type", "logout",
                    "username", client.getUsername())) ;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
        this.stage = client.getStage();
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        if(jsonObject == null)
            return;
        String username = (String) jsonObject.get("username") ;
        this.username.setText(username) ;
        this.username.setOnMouseClicked(event -> {Friendship.show(client, null) ; }) ;
    }
}
