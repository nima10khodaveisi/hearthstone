package graphic.menu;

import audio.AudioPlayer;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import main.Main;
import network.client.Client;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class Menu {
    private static Stage stage;

    public static void show(Client client, JSONObject jsonObject) {
        Platform.runLater(() -> {
            Stage stage = client.getStage();
            URL url = null;
            try {
                url = Paths.get("./src/main/java/graphic/menu/menu.fxml").toUri().toURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                Parent root = fxmlLoader.load();
                Controller controller = fxmlLoader.getController();
                controller.setClient(client);
                controller.setJsonObject(jsonObject) ;
                Scene scene = new Scene(root, 1280, 720);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
            }
        });
    }
}
