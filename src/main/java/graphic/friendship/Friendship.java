package graphic.friendship;

import chatroom.client.group.Group;
import graphic.menu.Menu;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class Friendship {
    public static void show(Client client, JSONObject jsonObject) {
        Platform.runLater(()-> {
            if (jsonObject == null) {
                client.sendMessage(MyJson.create_json(
                        "type", "friendship",
                        "query", "show").toJSONString());
                return;
            }
            Stage stage = client.getStage();
            URL url = null;
            try {
                url = Paths.get("./src/main/java/graphic/friendship/friendship.fxml").toUri().toURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                Parent root = fxmlLoader.load();
                Controller controller = fxmlLoader.getController();
                controller.setClient(client);
                controller.setJson(jsonObject);
                controller.init();
                Scene scene = new Scene(root, 1280, 720);
                scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent keyEvent) {
                        if (keyEvent.getCode() == KeyCode.ESCAPE) {
                            Menu.show(client, MyJson.create_json("username", client.getUsername()));
                        }
                    }
                });
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
            }
        }) ;
    }
}
