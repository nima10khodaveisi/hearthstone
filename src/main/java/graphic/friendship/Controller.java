package graphic.friendship;

import Models.Heros.Hero;
import Player.Player;
import chatroom.client.ChatRoom;
import chatroom.client.group.Group;
import chatroom.server.ChatServer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import game.GamePanel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import network.client.Client;
import network.json.MyJson;
import network.server.ServerMain;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class Controller {
    private Client client ;
    private Player player ;
    private ArrayList<Player> friends ;
    private ArrayList<String> gameTexts ;
    private ArrayList<Long> gamePorts ;
    private ArrayList<Long> chatPorts ;


    @FXML
    private ScrollPane groupsScroll ;

    @FXML
    private ScrollPane friendsScroll ;

    @FXML
    private TextField addFriendText ;

    @FXML
    private TextField groupText ;

    @FXML
    private Button createGroupButton ;

    @FXML
    private Button sendFriendReqButton ;

    @FXML
    private ScrollPane gamesScroll ;

    private VBox groupsBox = new VBox(10) ;
    private VBox friendsBox = new VBox(10) ;
    private VBox gamesBox = new VBox(10) ;

    public void init() {
        groupsScroll.setContent(groupsBox) ;
        friendsScroll.setContent(friendsBox) ;
        gamesScroll.setContent(gamesBox) ;
        //groups
        for(Group group : player.getGroups()) {
            HBox hBox = new HBox(5) ;
            Label label = new Label(group.getName()) ;
            hBox.getChildren().add(label) ;
            Button button = new Button("open") ;
            button.setOnMouseClicked(event -> {
                //open group
                Gson gson = new Gson() ;
                client.sendMessage(MyJson.create_json(
                        "type", "openGroup",
                        "username", client.getUsername(),
                        "group", gson.toJson(group)).toJSONString()) ;
            }) ;
            hBox.getChildren().add(button) ;
            groupsBox.getChildren().add(hBox) ;
        }

        //friends
        for(Player friend : friends) {
            HBox hBox = new HBox(5) ;
            Label label = new Label(friend.getUsername()) ;
            hBox.getChildren().add(label) ;
            if(friend.isOnline()) {
                Label label1 = new Label("online") ;
                hBox.getChildren().add(label1) ;
            }
            Button deleteButton = new Button("delete friend") ;
            deleteButton.setOnMouseClicked(event -> {
                //todo
                client.sendMessage(MyJson.create_json(
                        "type", "friendship",
                        "query", "removeFriend",
                        "username", friend.getUsername()).toJSONString()) ;
            }) ;
            hBox.getChildren().add(deleteButton) ;
            friendsBox.getChildren().add(hBox) ;
        }

        //games
        for(int i = 0 ; i < gameTexts.size() ; ++i) {
            String text = gameTexts.get(i) ;
            long port = gamePorts.get(i) ;
            long chatPort = chatPorts.get(i) ;
            HBox hBox = new HBox(5) ;
            Label label = new Label(text) ;
            hBox.getChildren().add(label) ;
            Button button = new Button("watch") ;
            hBox.getChildren().add(button) ;
            button.setOnMouseClicked(event -> {
                JSONObject message = MyJson.create_json(
                        "type", "connectToGameServer",
                        "port", port,
                        "serverIP", ServerMain.serverIP,
                        "watcher", true
                );
                client.connectToGameServer(message) ;

                message = MyJson.create_json(
                        "type", "chatroom",
                        "query", "join",
                        "port", chatPort,
                        "serverIP", ServerMain.serverIP) ;
                client.chatroom(message) ;
            }) ;
            gamesBox.getChildren().add(hBox) ;
        }

        //add friend
        sendFriendReqButton.setOnMouseClicked(event -> {
            String username = addFriendText.getText() ;
            if(username != null && !username.isEmpty()) {
                //todo
                client.sendMessage(MyJson.create_json(
                        "type", "friendship",
                        "query", "addFriend",
                        "username", username
                ).toJSONString()) ;
            }
        }) ;

        //add group
        createGroupButton.setOnMouseClicked(event -> {
            String name = groupText.getText() ;
            if(name != null && !name.isEmpty()) {
                client.sendMessage(MyJson.create_json(
                        "type", "friendship",
                        "query", "addGroup",
                        "name", name
                ).toJSONString()) ;
            }
        }) ;
    }


    public void setJson(JSONObject jsonObject) {
        if(jsonObject == null)
            return;
        Gson gson = new Gson();
        this.player = (Player) gson.fromJson(((String) jsonObject.get("player")), Player.class);
        this.friends = (ArrayList<Player>) gson.fromJson((String) jsonObject.get("friends"), new TypeToken<ArrayList<Player>>(){}.getType()) ;
        this.gameTexts = (ArrayList<String >) gson.fromJson((String) jsonObject.get("gameTexts"), new TypeToken<ArrayList<String>>(){}.getType()) ;
        this.gamePorts = (ArrayList<Long>) gson.fromJson((String) jsonObject.get("gamePorts"), new TypeToken<ArrayList<Long>>(){}.getType()) ;
        this.chatPorts = (ArrayList<Long>) gson.fromJson((String) jsonObject.get("chatPorts"), new TypeToken<ArrayList<Long>>(){}.getType()) ;

    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
