package graphic.setting;

import audio.AudioPlayer;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXSlider;
import graphic.images.Images;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private JFXCheckBox day;

    @FXML
    private JFXCheckBox night;

    @FXML
    private JFXCheckBox fireBird;

    @FXML
    private JFXCheckBox basic;

    @FXML
    private JFXCheckBox magical;

    @FXML
    private JFXCheckBox lava;

    @FXML
    private JFXSlider volume;


    private void fix(JFXCheckBox[] checkBoxes, String[] names) {
        if(checkBoxes.length == 2) {
            for(int i = 0 ; i < 2 ; ++i) {
                if(names[i].equals(Images.getInstance().getGameBackgroundName()))
                    checkBoxes[i].selectedProperty().setValue(true) ;
            }
        } else {
            for(int i = 0 ; i < 4 ; ++i) {
                if(names[i].equals(Images.getInstance().getCardBackName()))
                    checkBoxes[i].selectedProperty().setValue(true) ;
            }
        }
        for (int i = 0; i < checkBoxes.length; ++i) {
            JFXCheckBox jfxCheckBox = checkBoxes[i];
            int finalI = i;
            int finalI1 = i;
            jfxCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observableValue, Boolean before, Boolean after) {
                    if (before) {
                    } else {
                        if(names.length == 2)
                            Images.getInstance().setGameBackground(names[finalI1]) ;
                        else
                            Images.getInstance().setCardBack(names[finalI1]) ;
                        jfxCheckBox.setDisable(true) ;
                        for(int j = 0 ; j < checkBoxes.length ; ++j) {
                            if(j != finalI) {
                                if(checkBoxes[j].selectedProperty().getValue()) {
                                    checkBoxes[j].selectedProperty().setValue(false) ;
                                    checkBoxes[j].setDisable(false) ;
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fix(new JFXCheckBox[]{day, night}, new String[]{"day", "night"});
        fix(new JFXCheckBox[]{basic, fireBird, magical, lava},new String[]{"basic", "fireBird" , "magical", "lava"});
        volume.setValue(AudioPlayer.getInstance().getVolume() * 100) ;
        volume.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                BigDecimal bigDecimal = new BigDecimal((Double) t1) ;
                float myFloat = bigDecimal.floatValue() ;
                AudioPlayer.getInstance().setVolume((float)((float)myFloat / (float)100)) ;
            }
        }) ;
    }

}
