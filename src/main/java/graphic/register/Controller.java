package graphic.register;

import Player.Player;
import World.World;
import graphic.menu.Menu;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import graphic.login.Login ;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.IOException;

public class Controller {

    private Client client;
    private JSONObject jsonObject;


    @FXML
    private Button registerButton;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Text login;

    @FXML
    private Text error;

    @FXML
    private Text emptyField;

    @FXML
    void loginPage(MouseEvent event) throws IOException {
        Login.show(client, null);
    }

    @FXML
    void register(ActionEvent event) throws IOException {
        String user = username.getText() ;
        String pass = password.getText() ;
        if(user.isEmpty() || pass.isEmpty()) {
            error.setVisible(false) ;
            emptyField.setVisible(true) ;
        } else {
            client.sendMessage(MyJson.create_json(
                    "type", "register",
                            "username", user,
                            "password", pass)) ;
        }
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        if(jsonObject == null)
            return;
        String status = (String) jsonObject.get("status") ;
        if(status.equals("fail")) {
            error.setVisible(true);
            emptyField.setVisible(false);
        }
    }
}
