package graphic.login;

import World.World;
import graphic.menu.Menu;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import graphic.register.Register;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.IOException;

public class Controller {

    private Client client;
    private JSONObject jsonObject;

    @FXML
    private Button loginButton;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Text register;

    @FXML
    private Text error;

    @FXML
    void login(ActionEvent event) throws IOException {
        // check if username and password is correct
        String user = username.getText();
        String pass = password.getText();
        client.sendMessage(MyJson.create_json(
                "type", "login",
                "username", user,
                "password", pass));
        /*
        if(World.getWorld().login(user,pass)) {
            Menu.show();
        } else {
            error.setVisible(true) ;
        }*/
    }

    @FXML
    void registerPage(MouseEvent event) throws IOException {
        Register.show(client, null);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        if(jsonObject == null)
            return;
        String status = (String) jsonObject.get("status") ;
        if(status.equals("fail"))
            error.setVisible(true) ;
    }
}
