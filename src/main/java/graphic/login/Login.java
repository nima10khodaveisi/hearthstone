package graphic.login;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.Main;
import network.client.Client;
import network.client.ClientMain;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

public class Login {

    public static void show(Client client, JSONObject jsonObject) {
        Platform.runLater(() -> {
            Stage stage = client.getStage();
            URL url = null;
            try {
                url = Paths.get("./src/main/java/graphic/login/login.fxml").toUri().toURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                Parent root = fxmlLoader.load();
                Controller controller = fxmlLoader.getController();
                controller.setClient(client);
                Scene scene = new Scene(root, 1280, 720);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
            }
        });
    }

}
