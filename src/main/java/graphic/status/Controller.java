package graphic.status;

import Models.Cards.Card;
import Player.Deck;
import graphic.images.Images;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private AnchorPane scrollPane;

    @FXML
    private ImageView bestCardImage;

    @FXML
    private Text nameText;

    @FXML
    private Text winsText;

    @FXML
    private Text rateText;

    @FXML
    private Text gamesText;

    @FXML
    private Text manaAverageText;

    @FXML
    private Text heroClassText;

    private void setSelectedDeck(Deck deck) {
        nameText.setText(deck.getName()) ;
        winsText.setText(String.valueOf(deck.getWins())) ;
        if(deck.getNumberOfGames() > 0)
            rateText.setText(String.valueOf((deck.getWins() * 100) / deck.getNumberOfGames())) ;
        else
            rateText.setText(String.valueOf(0)) ;
        gamesText.setText(String.valueOf(deck.getNumberOfGames())) ;
        double manaAverage = deck.averageMana() ;
        BigDecimal bd = new BigDecimal(manaAverage).setScale(2, RoundingMode.HALF_UP);
        double newInput = bd.doubleValue();
        manaAverageText.setText(String.valueOf(newInput)) ;
        heroClassText.setText(deck.getHero().getName()) ;

        if(deck.getCards().size() > 0) {
            Card card = deck.get(0) ;
            //TODO get best card instead of 0-th card
            bestCardImage.setImage(Images.getInstance().getCardImage(card));
        } else
            bestCardImage.setImage(null) ;
    }


    private Image getImage(String name) throws MalformedURLException {
        URL url = Paths.get("./src/main/resources/" + name).toUri().toURL();
        return new Image(String.valueOf(url));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ArrayList<Deck> decks = Status.topDecks;
        for(int i = 0; i < decks.size() ; ++i) {
            Deck deck = decks.get(i) ;
            ImageView deckImage = Images.getInstance().getHeroImageView(deck.getHero()) ;
            deckImage.setFitWidth(443) ;
            deckImage.setFitHeight(140) ;
            deckImage.setLayoutX(0) ;
            deckImage.setLayoutY(i * 140) ;
            int finalI = i ;
            deckImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                setSelectedDeck(deck) ;
            }) ;
            scrollPane.getChildren().add(deckImage) ;

            Text nameOfDeck = new Text(deck.getName()) ;
            nameOfDeck.setLayoutX(0) ;
            nameOfDeck.setLayoutY(i * 140 + 30) ;
            nameOfDeck.setFont(Font.font("Verdana", FontPosture.ITALIC, 32));
            nameOfDeck.setFill(Color.WHITE);
            scrollPane.getChildren().add(nameOfDeck) ; ;
        }
    }
}
