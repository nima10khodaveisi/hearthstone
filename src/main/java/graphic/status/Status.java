package graphic.status;

import Player.Deck;
import World.World;
import graphic.menu.Menu;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import main.Main;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

public class Status {
    private static Stage stage = Main.stage;
    public static ArrayList<Deck> topDecks = new ArrayList<>();

    public static void show() throws IOException {


        ArrayList<Deck> decks = World.getWorld().getCurPlayer().getDecks();
        Collections.sort(decks);
        Collections.reverse(decks);
        for (int i = 0; i < Math.min(10, decks.size()); ++i) {
            topDecks.add(decks.get(i));
        }


        URL url = Paths.get("./src/main/java/graphic/status/status.fxml").toUri().toURL();
        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root, 1280, 720);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    Menu.show(null, null);
                }
            }
        });
        stage.setScene(scene);
        stage.show();

    }
}
