package graphic.images;

import Models.Heros.Hero;
import game.models.HeroGame;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class HeroGameController {

    @FXML
    private ImageView heroImage;

    @FXML
    private Text hpText;

    @FXML
    private ImageView attackImage;

    @FXML
    private Text attackText;


    public void getHero(Hero hero) {
        try {
            heroImage.setImage(Images.getInstance().getHeroPlaceImage(hero)) ;
        } catch (Exception e) {
        }
        hpText.setText(String.valueOf(hero.getHp())) ;
    }

    public void getHero(Hero hero, int hp) {
        try {
            heroImage.setImage(Images.getInstance().getHeroPlaceImage(hero)) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        hpText.setText(String.valueOf(hp)) ;
    }

    public void get(HeroGame heroGame) {
        try {
            heroImage.setImage(Images.getInstance().getHeroPlaceImage(heroGame.getHero())) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        hpText.setText(String.valueOf(heroGame.getHp())) ;
        if(heroGame.getAttack() > 0) {
            attackText.setVisible(true) ;
            attackImage.setVisible(true) ;
            attackText.setText(String.valueOf(heroGame.getAttack())) ;
        } else {
            attackImage.setVisible(false) ;
            attackText.setVisible(false) ;
        }
    }
}
