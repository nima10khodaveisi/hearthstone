package graphic.images;

import Models.Heros.Hero;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

public class HeroPowerController {
    @FXML
    private ImageView heroPowerImage;


    public void setHero(Hero hero) {
        heroPowerImage.setImage(Images.getInstance().getHeroPowerImage(hero)) ;
    }
}
