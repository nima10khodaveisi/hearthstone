package graphic.images;

import Models.Cards.Card;
import Models.Cards.Minions.Minion;
import Models.Cards.Weapon.Weapon;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

public class MinionImages {
    @FXML
    private Ellipse ellipse;

    @FXML
    private Text hp;

    @FXML
    private Text attack;

    @FXML
    private ImageView tauntImage ;

    public void setCard(Card card) {
        if(card instanceof Minion) {
            if (((Minion) card).isTaunt()) {
                tauntImage.setVisible(true);
            } else {
                tauntImage.setVisible(false);
            }
        }
        try {
            ellipse.setFill(new ImagePattern(new Image(String.valueOf(Paths.get("./src/main/resources/cardsBackground/" + card.getName() + ".jpeg").toUri().toURL()))));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        hp.setText(String.valueOf(card.getHp()));
        attack.setText(String.valueOf(card.getAttack()));
    }

    public void setCard(Card card, int attack, int hp) {
        if(((Minion)card).isTaunt()) {
            tauntImage.setVisible(true) ;
        } else {
            tauntImage.setVisible(false) ;
        }
        try {
            ellipse.setFill(new ImagePattern(new Image(String.valueOf(Paths.get("./src/main/resources/cardsBackground/" + card.getName() + ".jpeg").toUri().toURL()))));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.hp.setText(String.valueOf(hp));
        this.attack.setText(String.valueOf(attack));
    }
}
