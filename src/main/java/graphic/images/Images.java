package graphic.images;

import Models.Cards.Card;
import Models.Heros.Hero;
import game.models.HeroGame;
import game.models.HeroPower;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

public class Images {
    private static Images instance;
    private ImageView gameBackground = null;
    private String gameBackgroundName = "night";
    private String cardBackName = "basic";
    private ImageView cardBack = null;

    public static Images getInstance() {
        if (instance == null) {
            instance = new Images();
        }
        return instance;
    }

    private Images() {
    }

    public void init() {
        setCardBack("basic");
        setGameBackground("day");
    }

    public Image getCardImage(Card card) {
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "cards" + File.separator + card.getName() + ".png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        return new Image(String.valueOf(url));
    }

    public ImageView getCardImageView(Card card) {
        return new ImageView(getCardImage(card));
    }

    public Image getHeroImage(Hero hero) {
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "heroes" + File.separator + hero.getName() + ".png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        return new Image(String.valueOf(url));
    }

    public ImageView getHeroImageView(Hero hero) {
        return new ImageView(getHeroImage(hero));
    }

    public Image getHeroPlaceImage(Hero hero) {
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "heroes" + File.separator + hero.getName() + "Place.png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        return new Image(String.valueOf(url));
    }

    public ImageView getHeroPlaceImageView(Hero hero) {
        return new ImageView(getHeroPlaceImage(hero));
    }

    public ImageView getGameBackground() {
        return gameBackground;
    }

    public void setGameBackground(String name) {
        //name is either day or night
        gameBackgroundName = name;
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "game" + File.separator + name + ".png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        gameBackground = new ImageView(new Image(String.valueOf(url)));
    }

    public ImageView getCardBack() {
        return cardBack;
    }

    public void setCardBack(String name) {
        cardBackName = name;
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "game" + File.separator + name + ".png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        cardBack = new ImageView(new Image(String.valueOf(url)));
    }

    public Image getManaImageView(String name) {
        //name is either Mana or offMana
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "game" + File.separator + name + ".png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        return new Image(String.valueOf(url));
    }

    public String getGameBackgroundName() {
        return gameBackgroundName;
    }

    public void setGameBackgroundName(String gameBackgroundName) {
        this.gameBackgroundName = gameBackgroundName;
    }

    public String getCardBackName() {
        return cardBackName;
    }

    public void setCardBackName(String cardBackName) {
        this.cardBackName = cardBackName;
    }

    public Image getHeroPowerImage(Hero hero) {
        String name = hero.getName();
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "game" + File.separator + name + "HeroPower.png").toUri().toURL();
        } catch (MalformedURLException ignore) {
        }
        return new Image(String.valueOf(url));
    }

    public Pane getSummonCard(Card card) {
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "images" + File.separator + "Minion.fxml").toUri().toURL();
            FXMLLoader loader = new FXMLLoader(url);
            Pane root = loader.load();
            MinionImages controller = loader.getController();
            controller.setCard(card);
            return root ;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Pane getSummonCard(Card card, int attack, int hp) {
        URL url = null;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "images" + File.separator + "Minion.fxml").toUri().toURL();
            FXMLLoader loader = new FXMLLoader(url);
            Pane root = loader.load();
            MinionImages controller = loader.getController();
            controller.setCard(card, attack, hp);
            return root ;
        } catch (IOException e) {
        }
        throw new NullPointerException() ;
    }

    public Pane getHeroGameImageView(Hero hero, int hp) {
        URL url = null ;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "images" + File.separator + "HeroGame.fxml").toUri().toURL();
            FXMLLoader loader = new FXMLLoader(url) ;
            Pane root = loader.load() ;
            HeroGameController controller = loader.getController() ;
            controller.getHero(hero, hp) ;
            return root ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new NullPointerException();
    }

    public Pane getHeroGameImageView(HeroGame heroGame) {
        URL url = null ;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "images" + File.separator + "HeroGame.fxml").toUri().toURL();
            FXMLLoader loader = new FXMLLoader(url) ;
            Pane root = loader.load() ;
            HeroGameController controller = loader.getController() ;
            controller.get(heroGame) ;
            return root ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new NullPointerException();
    }

    public Pane getHeroPower(Hero hero) {
        URL url = null ;
        try {
            url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "images" + File.separator + "HeroPower.fxml").toUri().toURL();
            FXMLLoader loader = new FXMLLoader(url) ;
            Pane root = loader.load() ;
            HeroPowerController controller = loader.getController() ;
            controller.setHero(hero) ;
            return root ;
        } catch (Exception e) { }
        throw new NullPointerException();
    }

}
