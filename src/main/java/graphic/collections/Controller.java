package graphic.collections;

import Models.Cards.Card;
import Models.Heros.Hero;
import Player.Deck;
import Player.Player;
import World.World;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import graphic.images.Images;
import graphic.shop.Shop;
import javafx.animation.PauseTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.util.Duration;
import main.Main;
import network.client.Client;
import network.json.MyJson;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;
import org.json.simple.JSONObject;
import org.w3c.dom.ls.LSOutput;

import javax.crypto.AEADBadTagException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.InvalidMarkException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller {

    private Deck selectedDeck = null;
    private int selectedDeckIndex = -1;
    private ArrayList<Card> curPageCards = new ArrayList<>();
    private ArrayList<ImageView> decksImages = new ArrayList<>();
    private ArrayList<Text> decksTexts = new ArrayList<>();
    private ArrayList<TextField> decksTextFields = new ArrayList<>();
    private ArrayList<JFXComboBox<String>> decksComboboxes = new ArrayList<>();
    private ArrayList<Deck> decks = new ArrayList<Deck>();
    private ArrayList<Card> filteredCards = new ArrayList<>();
    private int curPage = 0;
    private Card selectedCard = null;
    private int curManaFilter;
    private JSONObject jsonObject;
    private Player player;
    private ArrayList<Card> allCards;
    private ArrayList<Hero> allHeroes;
    private Client client;

    @FXML
    private Text messageText;

    @FXML
    private JFXButton shopPageButton;

    @FXML
    private JFXButton deleteDeckButton;

    @FXML
    private JFXButton addToDeckButton;

    @FXML
    private JFXButton removeFromDeckButton;

    @FXML
    private Pane primaryPane;

    @FXML
    private AnchorPane scrollPaneDecks;

    @FXML
    private TextField searchField;


    @FXML
    private JFXSlider manaFilter;


    @FXML
    private JFXComboBox<String> typeFilter;


    @FXML
    private JFXButton createNewDeckButton;


    @FXML
    private ImageView cardImage0;

    @FXML
    private ImageView cardImage1;

    @FXML
    private ImageView cardImage2;

    @FXML
    private ImageView cardImage3;

    @FXML
    private ImageView cardImage4;

    @FXML
    private ImageView cardImage5;

    @FXML
    private ImageView rightArrow;

    @FXML
    private ImageView leftArrow;

    @FXML
    private ImageView selectedCardImage;

    @FXML
    private JFXButton doneSelectingDeck;


    @FXML
    private JFXButton editSelectedDeckButton;


    @FXML
    private ImageView unselectCardImage;


    private void showMessage(String message) {
        messageText.setText(message);
        messageText.setVisible(true);
        PauseTransition wait = new PauseTransition(Duration.seconds(1));
        wait.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                messageText.setText(null);
                messageText.setVisible(false);
            }
        });
        wait.play();
    }

    private void setSelectedCard(int ind) {
        Card card = curPageCards.get(ind);
        selectedCard = card;
        unselectCardImage.setDisable(false);
        unselectCardImage.setVisible(true);

        selectedCardImage.setDisable(false);
        selectedCardImage.setVisible(true);
        GaussianBlur effect = new GaussianBlur();
        primaryPane.setEffect(effect);
        primaryPane.setDisable(true);
        selectedCardImage.setImage(Images.getInstance().getCardImage(card));

        //set buttons
        shopPageButton.setDisable(false);
        shopPageButton.setVisible(true);

        if (selectedDeck != null) {
            addToDeckButton.setVisible(true);
            addToDeckButton.setDisable(false);
            removeFromDeckButton.setDisable(false);
            removeFromDeckButton.setVisible(true);
        }
    }

    //Draw pages

    private void drawPage(int pageNumber) {
        curPage = pageNumber;
        curPageCards.clear();
        for (int i = pageNumber * 6; i < pageNumber * 6 + 6; ++i) {
            if (i < filteredCards.size()) {
                curPageCards.add(filteredCards.get(i));
            }
        }

        ArrayList<ImageView> imageViews = new ArrayList<>();
        imageViews.add(cardImage0);
        imageViews.add(cardImage1);
        imageViews.add(cardImage2);
        imageViews.add(cardImage3);
        imageViews.add(cardImage4);
        imageViews.add(cardImage5);
        for (ImageView image : imageViews) {
            image.setDisable(true);
            image.setImage(null);
        }


        for (int i = 0; i < curPageCards.size(); ++i) {
            Card card = curPageCards.get(i);
            ImageView image = imageViews.get(i);
            image.setDisable(false);
            image.setImage(Images.getInstance().getCardImage(card));
            if (!player.containCard(card)) {
                ColorAdjust colorAdjust = new ColorAdjust();
                colorAdjust.setSaturation(-1);
                image.setEffect(colorAdjust);
            } else {
                ColorAdjust colorAdjust = new ColorAdjust();
                image.setEffect(colorAdjust);
            }
        }


        if (pageNumber == 0) {
            leftArrow.setDisable(true);
            leftArrow.setVisible(false);
        } else {
            leftArrow.setDisable(false);
            leftArrow.setVisible(true);
        }
        int size = filteredCards.size();
        size /= 6;
        if (filteredCards.size() % 6 != 0)
            size++;
        if (pageNumber == size - 1) {
            rightArrow.setDisable(true);
            rightArrow.setVisible(false);
        } else {
            rightArrow.setDisable(false);
            rightArrow.setVisible(true);
        }
    }

    @FXML
    void drawNextPage(MouseEvent event) throws MalformedURLException {
        curPage++;
        drawPage(curPage);
    }

    @FXML
    void drawPreviousPage(MouseEvent event) throws MalformedURLException {
        curPage--;
        drawPage(curPage);
    }


    protected boolean isMatch(String userText, String cardName) {
        String userTextLower = userText.toLowerCase();
        String suggestionStr = cardName.toLowerCase();
        return suggestionStr.contains(userTextLower);
    }


    private void setFilteredCards() {
        if (selectedDeck != null) {
            filteredCards = new ArrayList<>(selectedDeck.getCards());
            if (typeFilter.getValue() == null) {
                typeFilter.setValue("Deck's cards");
            }
            if (typeFilter.getValue().equals("All cards")) {
                filteredCards = new ArrayList<>(allCards);
            } else if (typeFilter.getValue().equals("Not in deck cards")) {
                filteredCards = new ArrayList<>();
                for (Card card : allCards) {
                    if (!selectedDeck.contain(card))
                        filteredCards.add(card);
                }
            }
        } else {
            filteredCards = new ArrayList<>(allCards);
            if (typeFilter.getValue() == null) {
                typeFilter.setValue("All cards");
            }
            if (typeFilter.getValue().equals("My cards")) {
                filteredCards = new ArrayList<>(player.getCards());
            } else if (typeFilter.getValue().equals("Cards I don't have")) {
                filteredCards = new ArrayList<>();
                for (Card card : allCards) {
                    if (!player.containCard(card))
                        filteredCards.add(card);
                }
            } else if (!typeFilter.getValue().equals("All cards")) {
                filteredCards = new ArrayList<>();
                for (Card card : allCards) {
                    if (card.getHeroClass().equals(typeFilter.getValue()))
                        filteredCards.add(card);
                }
            }
        }


        //Search and mana filter

        ArrayList<Card> remove = new ArrayList<>();
        if (curManaFilter != -1) {
            for (Card card : filteredCards) {
                if (card.getMana() != curManaFilter) {
                    remove.add(card);
                }
            }
            for (Card card : remove)
                filteredCards.remove(card);
            remove.clear();
        }
        remove = new ArrayList<>();
        String userSearchText = searchField.getText();
        for (Card card : filteredCards) {
            if (!isMatch(userSearchText, card.getName())) {
                remove.add(card);
            }
        }
        for (Card card : remove)
            filteredCards.remove(card);
        remove.clear();

        curPage = 0;
        drawPage(0);
    }

    private Image getImage(String name) throws MalformedURLException {
        URL url = Paths.get("./src/main/resources/" + name).toUri().toURL();
        return new Image(String.valueOf(url));
    }

    private void setSelectedDeck(int ind) {
        if (selectedDeck != null)
            return;
        Deck deck = decks.get(ind);
        selectedDeckIndex = ind;
        editSelectedDeckButton.setVisible(true);
        editSelectedDeckButton.setDisable(false);
        deleteDeckButton.setVisible(true);
        deleteDeckButton.setDisable(false);
        GaussianBlur effect = new GaussianBlur();
        for (int j = 0; j < decks.size(); ++j) {
            if (ind == j) continue;
            decksImages.get(j).setEffect(effect);
            decksTexts.get(j).setEffect(effect);
        }
        doneSelectingDeck.setDisable(false);
        doneSelectingDeck.setVisible(true);
        selectedDeck = deck;
        ArrayList<String> cardsNames = new ArrayList<>();
        for (Card card : deck.getCards())
            cardsNames.add(card.getName());
        TextFields.bindAutoCompletion(searchField, cardsNames);
        typeFilter.getItems().remove(0, 3);
        typeFilter.getItems().add("Deck's cards");
        typeFilter.getItems().add("All cards");
        typeFilter.getItems().add("Not in deck cards");
        typeFilter.setPromptText("Deck's cards");
        searchField.setText("");
        manaFilter.setValue(-1);
        setFilteredCards();
        drawPage(0);
    }

    public void editSelectedDeck() {
        JFXButton button = editSelectedDeckButton;
        int ind = selectedDeckIndex;
        Deck deck = decks.get(ind);
        if (button.getText().equals("Edit Deck")) {
            button.setText("Save");
            TextField textField = decksTextFields.get(ind);
            textField.setText(deck.getName());
            textField.setVisible(true);
            textField.setDisable(false);
            Text text = decksTexts.get(ind);
            text.setVisible(false);
            textField.setLayoutX(text.getLayoutX());
            textField.setLayoutY(text.getLayoutY());
            doneSelectingDeck.setDisable(true);
            doneSelectingDeck.setVisible(false);
            textField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                    if (!player.canAddNewDeck(t1) && !textField.getText().equals(deck.getName())) {
                        textField.setStyle("-fx-text-fill: red;");
                    } else {
                        textField.setStyle("-fx-text-fill: black;");
                    }
                }
            });

            //change hero
            JFXComboBox<String> comboBox = decksComboboxes.get(ind);
            comboBox.setDisable(false);
            comboBox.setVisible(true);
            comboBox.getItems().clear();
            for (Hero hero : allHeroes) {
                if (deck.canChangeHero(hero)) {
                    comboBox.getItems().add(hero.getName());
                }
            }
            comboBox.setValue(deck.getHero().getName());
        } else {
            TextField textField = decksTextFields.get(ind);
            Text text = decksTexts.get(ind);
            text.setText(textField.getText());
            if (text.getText().isEmpty() || (!player.canAddNewDeck(text.getText()) && !text.getText().equals(deck.getName()))) {
                return;
            }
            player.changeNameOfDeck(deck.getName(), text.getText());
            JFXComboBox<String> comboBox = decksComboboxes.get(ind);
            player.changeHeroOfDeck(deck.getName(), new Hero(comboBox.getValue()));

            client.sendMessage(MyJson.create_json(
                    "type", "collections",
                    "query", "updateDeck",
                    "username", client.getUsername(),
                    "oldDeckName", deck.getName(),
                    "newDeckName", text.getText(),
                    "heroName", comboBox.getValue()));

            ImageView imageView = decksImages.get(ind);
            try {
                imageView.setImage(getImage(deck.getHero().getName() + ".jpeg"));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            comboBox.setDisable(true);
            comboBox.setVisible(false);
            button.setText("Edit Deck");
            textField.setDisable(true);
            textField.setVisible(false);
            text.setVisible(true);
            doneSelectingDeck.setDisable(false);
            doneSelectingDeck.setVisible(true);
        }
    }

    private void drawDecks() {
        decksTextFields.clear();
        decksComboboxes.clear();
        decksImages.clear();
        decksTexts.clear();
        scrollPaneDecks.getChildren().clear();
        for (int i = 0; i < decks.size(); ++i) {
            Deck deck = decks.get(i);
            ImageView deckImage = Images.getInstance().getHeroImageView(deck.getHero());
            deckImage.setFitWidth(351);
            deckImage.setFitHeight(124);
            deckImage.setLayoutY(i * 124);
            deckImage.setLayoutX(0);

            int finalI = i;
            deckImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                setSelectedDeck(finalI);
            });

            scrollPaneDecks.getChildren().add(deckImage);
            Text nameOfDeck = new Text(deck.getName());
            nameOfDeck.setLayoutX(39);
            nameOfDeck.setLayoutY(63 * (2 * i + 1));
            nameOfDeck.setFont(Font.font("Verdana", FontPosture.ITALIC, 32));
            nameOfDeck.setFill(Color.WHITE);
            scrollPaneDecks.getChildren().add(nameOfDeck);

            nameOfDeck.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                setSelectedDeck(finalI);
            });

            TextField textField = new TextField();
            textField.setFont(Font.font("Verdana", FontPosture.ITALIC, 32));
            textField.setDisable(true);
            textField.setVisible(false);
            scrollPaneDecks.getChildren().add(textField);
            decksTextFields.add(textField);

            JFXComboBox<String> comboBox = new JFXComboBox();
            comboBox.setLayoutX(1081);
            comboBox.setLayoutY(85);
            comboBox.setDisable(true);
            comboBox.setVisible(false);
            decksComboboxes.add(comboBox);
            comboBox.setStyle("-fx-font: 30px;");
            //TODO make it beautiful =)))
            primaryPane.getChildren().add(comboBox);

            decksImages.add(deckImage);
            decksTexts.add(nameOfDeck);
        }
    }

    private void doneSelectingDeck() {
        selectedDeckIndex = -1;
        GaussianBlur effect = new GaussianBlur(0);
        for (int j = 0; j < decks.size(); ++j) {
            decksImages.get(j).setEffect(effect);
            decksTexts.get(j).setEffect(effect);
        }
        doneSelectingDeck.setDisable(true);
        doneSelectingDeck.setVisible(false);
        selectedDeck = null;
        ArrayList<String> cardsName = new ArrayList<>();
        for (Card card : allCards)
            cardsName.add(card.getName());
        TextFields.bindAutoCompletion(searchField, cardsName);
        typeFilter.getItems().remove(0, 3 + allHeroes.size());
        typeFilter.getItems().add("All cards");
        typeFilter.getItems().add("My cards");
        typeFilter.getItems().add("Cards I don't have");
        for (Hero hero : allHeroes)
            typeFilter.getItems().add(hero.getName());
        typeFilter.setPromptText("All cards");
        editSelectedDeckButton.setDisable(true);
        editSelectedDeckButton.setVisible(false);
        deleteDeckButton.setDisable(true);
        deleteDeckButton.setVisible(false);
    }

    public void init() {
        // search cards
        ArrayList<String> cardsNames = new ArrayList<>();
        for (Card card : allCards)
            cardsNames.add(card.getName());
        TextFields.bindAutoCompletion(searchField, cardsNames);
        searchField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String before, String str) {
                setFilteredCards();
            }
        });


        Player curPlayer = player;
        decks = curPlayer.getDecks();
        drawDecks();


        //Done selecting a deck
        doneSelectingDeck.setDisable(true);
        doneSelectingDeck.setVisible(false);
        doneSelectingDeck.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            doneSelectingDeck();
        });


        //Mana Filter
        curManaFilter = -1;
        manaFilter.setValue(-1);
        manaFilter.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number old_value, Number new_value) {
                curManaFilter = (int) Math.floor((Double) new_value);
                setFilteredCards();
            }
        });


        unselectCardImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            selectedCardImage.setVisible(false);
            selectedCardImage.setDisable(true);
            GaussianBlur effect = new GaussianBlur(0);
            primaryPane.setEffect(effect);
            unselectCardImage.setDisable(true);
            unselectCardImage.setVisible(false);
            primaryPane.setDisable(false);
            shopPageButton.setDisable(true);
            shopPageButton.setVisible(false);
            addToDeckButton.setDisable(true);
            addToDeckButton.setVisible(false);
            removeFromDeckButton.setVisible(false);
            removeFromDeckButton.setDisable(true);
            setFilteredCards();

        });

        //Type Filter
        //TODO set filters
        typeFilter.getItems().add("All cards");
        typeFilter.getItems().add("My cards");
        typeFilter.getItems().add("Cards I don't have");
        for (Hero hero : allHeroes)
            typeFilter.getItems().add(hero.getName());
        typeFilter.setPromptText("All cards");
        typeFilter.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String before, String curType) {
                setFilteredCards();
            }
        });
        setFilteredCards();
        drawPage(0);

        //Selected card image config
        selectedCardImage.setDisable(true);
        selectedCardImage.setVisible(false);


        //Unselect card
        unselectCardImage.setDisable(true);
        unselectCardImage.setVisible(false);

        //selected mage
        editSelectedDeckButton.setDisable(true);
        editSelectedDeckButton.setVisible(false);
        deleteDeckButton.setVisible(false);
        deleteDeckButton.setDisable(true);
        editSelectedDeckButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            editSelectedDeck();
        });

        deleteDeckButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            curPlayer.deleteDeck(selectedDeck.getName());
            selectedDeck = null;
            showMessage("Deleted!");
            //TODO add repaint the scroll bar to fix this issue
            drawDecks();
            doneSelectingDeck();
        });


        //Create new deck
        createNewDeckButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            Deck deck = new Deck("", new Hero("Mage"));

            client.sendMessage(MyJson.create_json(
                    "type", "collections",
                    "query", "createNewDeck",
                    "deckName", "",
                    "heroName", "Mage"));
            int i = decks.size();

            decks.add(deck);

            ImageView deckImage = Images.getInstance().getHeroImageView(deck.getHero());
            deckImage.setFitWidth(351);
            deckImage.setFitHeight(124);
            deckImage.setLayoutY(i * 124);
            deckImage.setLayoutX(0);

            int finalI = i;
            deckImage.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
                setSelectedDeck(finalI);
            });

            scrollPaneDecks.getChildren().add(deckImage);
            Text nameOfDeck = new Text(deck.getName());
            nameOfDeck.setLayoutX(39);
            nameOfDeck.setLayoutY(63 * (2 * i + 1));
            nameOfDeck.setFont(Font.font("Verdana", FontPosture.ITALIC, 32));
            nameOfDeck.setFill(Color.WHITE);
            scrollPaneDecks.getChildren().add(nameOfDeck);

            nameOfDeck.addEventHandler(MouseEvent.MOUSE_CLICKED, e1 -> {
                setSelectedDeck(finalI);
            });

            TextField textField = new TextField();
            textField.setFont(Font.font("Verdana", FontPosture.ITALIC, 32));
            textField.setDisable(true);
            textField.setVisible(false);
            scrollPaneDecks.getChildren().add(textField);
            decksTextFields.add(textField);

            JFXComboBox<String> comboBox = new JFXComboBox();
            comboBox.setLayoutX(1081);
            comboBox.setLayoutY(85);
            comboBox.setDisable(true);
            comboBox.setVisible(false);
            decksComboboxes.add(comboBox);
            comboBox.getEditor().setFont(Font.font("Verdana", FontPosture.ITALIC, 20));
            //TODO make it beautiful =)))
            primaryPane.getChildren().add(comboBox);

            decksImages.add(deckImage);
            decksTexts.add(nameOfDeck);

            setSelectedDeck(i);
            editSelectedDeck();
        });


        cardImage0.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setSelectedCard(0);
        });

        cardImage1.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setSelectedCard(1);
        });

        cardImage2.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setSelectedCard(2);
        });

        cardImage3.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setSelectedCard(3);
        });

        cardImage4.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setSelectedCard(4);
        });

        cardImage5.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            setSelectedCard(5);
        });

        //add to deck button
        addToDeckButton.setDisable(true);
        addToDeckButton.setVisible(false);
        addToDeckButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {

            client.sendMessage(MyJson.create_json(
                    "type", "collections",
                    "query", "addCardToDeck",
                    "username", client.getUsername(),
                    "deckName", selectedDeck.getName(),
                    "cardName", selectedCard.getName()));


            if (curPlayer.canAddToDeck(selectedDeck.getName(), selectedCard)) {
                showMessage(selectedCard.getName() + "has been added to " + selectedDeck.getName());
                curPlayer.addToDeck(selectedDeck.getName(), selectedCard);
            } else {
                showMessage("You can't add this card to selected deck!");
            }
        });

        //remove from Button
        removeFromDeckButton.setVisible(false);
        removeFromDeckButton.setDisable(true);
        removeFromDeckButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {

            client.sendMessage(MyJson.create_json(
                    "type", "collections",
                    "query", "removeFromDeck",
                    "username", client.getUsername(),
                    "deckName", selectedDeck.getName(),
                    "cardName", selectedCard.getName()));


            if (selectedDeck.contain(selectedCard)) {
                curPlayer.removeFromDeck(selectedDeck.getName(), selectedCard);
                showMessage("removed!");
            } else {
                showMessage("You can't remove this card from this deck");
            }
        });

        //go to shop page
        shopPageButton.setDisable(true);
        shopPageButton.setVisible(false);
        shopPageButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            Scene curScene = client.getStage().getScene();
            Shop.show(curScene, selectedCard, null, client);
        });
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setJson(JSONObject jsonObject) {
        if (jsonObject == null)
            return;
        this.jsonObject = jsonObject;
        Gson gson = new Gson();
        this.player = (Player) gson.fromJson(((String) jsonObject.get("player")), Player.class);
        this.allCards = (ArrayList<Card>) gson.fromJson((String) jsonObject.get("allCards"), new TypeToken<ArrayList<Card>>(){}.getType()) ;
        this.allHeroes = (ArrayList<Hero>) gson.fromJson((String) jsonObject.get("allHeroes"), new TypeToken<ArrayList<Hero>>(){}.getType()) ;
        String error = (String) jsonObject.get("error");
        showMessage(error);
        init();
    }
}
/*TODO
3 - unique filtered cards
4 - make errors more specific ( by using exceptions maybe! )
 */