package graphic.collections;

import graphic.menu.Menu;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import main.Main;
import network.client.Client;
import network.json.MyJson;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class Collections {
    private static Stage stage;

    public static void show(Client client, JSONObject jsonObject) {
        Platform.runLater(() -> {
            stage = client.getStage();
            if (jsonObject == null) {
                client.sendMessage(MyJson.create_json(
                        "type", "collections",
                        "username", client.getUsername(),
                        "query", "show"));
                return;
            }
            try {
                URL url = Paths.get("." + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "graphic" + File.separator + "collections" + File.separator + "collections.fxml").toUri().toURL();
                FXMLLoader fxmlLoader = new FXMLLoader(url);
                Parent root = fxmlLoader.load();
                Controller controller = fxmlLoader.getController();
                controller.setClient(client);
                controller.setJson(jsonObject);
                Scene scene = new Scene(root, 1280, 720);
                scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent keyEvent) {
                        if (keyEvent.getCode() == KeyCode.ESCAPE) {
                            Menu.show(client, MyJson.create_json("username", client.getUsername()));
                        }
                    }
                });
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
