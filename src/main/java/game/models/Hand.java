package game.models;

import Models.Cards.Card;
import graphic.images.Images;
import javafx.scene.image.ImageView;

import javax.crypto.AEADBadTagException;
import java.util.ArrayList;

public class Hand {
    private ArrayList<Card> cards ;
    private ArrayList<ImageView> images ;
    private int curIndHand ;

    public Hand(ArrayList<Card> cards, boolean hidden) {
        images = new ArrayList<>() ;
        curIndHand = 0 ;
        for(Card card : cards) {
            if(!hidden)
                images.add(Images.getInstance().getCardImageView(card)) ;
            else
                images.add(Images.getInstance().getCardBack()) ;
        }
    }

    public ArrayList<ImageView> getImages() {
        if(images.size() < 4) {
            curIndHand = 0 ;
            return images ;
        } else {
            ArrayList<ImageView> ret = new ArrayList<>() ;
            curIndHand += images.size() ;
            curIndHand %= images.size() ;
            for(int cnt = 0 ; cnt < 4 ; ++cnt) {
                int i = (cnt + curIndHand) % images.size() ;
                ret.add(images.get(i)) ;
            }
            return ret ;
        }
    }

    public void addInd() {
        curIndHand++ ;
    }

    public void redInd() {
        curIndHand-- ;
    }

    public int getInd() { return curIndHand; }
}
