package game.models;

import Models.Cards.Card;
import Models.Heros.Hero;
import game.GameState;
import game.actions.Attack;
import game.actions.abilities.heroPowers.MageHeroPower;
import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class HeroGame extends Character {
    private Hero hero;
    private boolean first = false;
    private int durability;
    private transient Pane weaponPane;
    private Card weapon;

    public HeroGame() {
    }

    public HeroGame(Hero hero, GamePlayer player) {
        this.player = player;
        disable = false;
        this.hero = hero.copy();
        removeEventHandler();
        setHp((int) this.hero.getHp());
        setId(-1);
    }

    public void clickEventHandler() {
        Attack attack = player.getAttack();
        if (!getSelected()) {
            //select card
            if (attack.getAttacker() == null && getDurability() > 0) {
                attack.setAttacker(this);
                setSelected(true);
            } else {
                attack.setDefender(this);
            }
        } else {
            //unselect card
            attack.setAttacker(null);
            setSelected(false);
        }
    }

    public void deleteEventHandler() {
        if (pane != null && eventHandler != null) {
            pane.removeEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
        }
    }

    public Image getImage() {
        return Images.getInstance().getHeroPlaceImage(hero);
    }

    public Image getHeroPowerImage() {
        return Images.getInstance().getHeroPowerImage(hero);
    }

    public void setHpText(Text hpText) {
        pane.getChildren().add(Images.getInstance().getHeroGameImageView(this.hero, getHp()));
        setHp(getHp());
    }

    @Override
    public void updatePane() {
        Platform.runLater(() -> {
            if (player.isGraphical()) {
                if (pane != null) {
                    pane.getChildren().clear();
                    pane.getChildren().add(Images.getInstance().getHeroGameImageView(this));
                }
            } else {
                player.updateGameGraphic();
                if (player.getEnemy() != null)
                    player.getEnemy().updateGameGraphic();
            }
        });
    }

    private void updateWeapon() {
        Platform.runLater(() -> {
            if(player.isGraphical()) {
                weaponPane.getChildren().clear();
                if (weapon != null)
                    weaponPane.getChildren().add(Images.getInstance().getSummonCard(weapon));
            } else {
                player.updateGameGraphic();
                if(player.getEnemy() != null)
                    player.getEnemy().updateGameGraphic();
            }
        });
    }

    public void restoreHeal(int value) {
        if (getHp() + value >= hero.getHp())
            setHp((int) hero.getHp());
        else {
            setHp(getHp() + value);
        }
    }

    @Override
    public void setDisable(boolean value) {
        disable = value;
//        if (pane != null) {
//            if (value)
//                removeEventHandler();
//            else
//                addEventHandler();
//        }
    }

    public void setPane(Pane pane) {
        if (pane == null) {
            updatePane();
            return;
        }
        deleteEventHandler();
        this.pane = pane;
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                if(getDurability() > 0) {
                    clickEventHandler();
                    return;
                }
                if (disable || !player.isGraphical())
                    return;
                clickEventHandler();
            }
        };
        addEventHandler();
        updatePane();
        first = true;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    @Override
    public double getX() {
        return pane.getLayoutX();
    }

    @Override
    public double getY() {
        return pane.getLayoutY();
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
        weapon.setHp((long) durability);
        updateWeapon();
        if (durability <= 0) {
            weaponPane.getChildren().clear();
            setAttack(0);
            weapon = null;
        }
    }


    public Pane getWeaponPane() {
        return weaponPane;
    }

    public void setWeaponPane(Pane weaponPane) {
        this.weaponPane = weaponPane;
        updateWeapon();
    }

    public Card getWeapon() {
        return weapon;
    }

    public void setWeapon(Card weapon) {
        this.weapon = weapon;
        setAttack((int) weapon.getAttack());
        setDurability((int) weapon.getHp());
    }
}