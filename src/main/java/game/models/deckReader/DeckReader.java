package game.models.deckReader;

import Models.Cards.Card;
import Models.Cards.Minions.Minion;
import Models.Cards.Quest.Quest;
import Models.Cards.Spells.Spell;
import Models.Cards.Weapon.Weapon;
import Player.Deck;
import World.World;
import com.google.gson.Gson;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

public class DeckReader {
    private static DeckReader instance;

    public static DeckReader getInstance() {
        if (instance == null) {
            instance = new DeckReader();
        }
        return instance;
    }

    public ArrayList<Card> getCards(ArrayList<String> names) {
        ArrayList<Card> friend = new ArrayList<>() ;
        for(String name : names) {
            if(name.equals("spell")) {
                Random random = new Random() ;
                Card card = World.getWorld().getSpells().get(random.nextInt(World.getWorld().getSpells().size())) ;
                friend.add(card) ;
                continue ;
            } else if(name.equals("minion")) {
                Random random = new Random() ;
                Card card = World.getWorld().getMinions().get(random.nextInt(World.getWorld().getMinions().size())) ;
                friend.add(card) ;
                continue ;
            }
            try {
                Minion card = new Minion(name) ;
                friend.add(card) ;
            } catch (Exception e) { }

            try {
                Spell card = new Spell(name) ;
                friend.add(card) ;
            } catch (Exception e) { }

            try {
                Weapon card = new Weapon(name) ;
                friend.add(card) ;
            } catch (Exception e) { }

            try {
                Quest card = new Quest(name) ;
                friend.add(card) ;
            } catch (Exception e) { }
        }
        return friend ;
    }

    public ArrayList<Card> friend() throws Exception {
        Gson gson = new Gson();
        DeckFormat deckFormat = gson.fromJson(new FileReader("./src/main/java/game/models/deckReader/deck.json"), DeckFormat.class) ;
        ArrayList<String> names = deckFormat.getFriend() ;
        return getCards(names) ;
    }

    public ArrayList<Card> enemy() throws Exception {
        Gson gson = new Gson();
        DeckFormat deckFormat = gson.fromJson(new FileReader("./src/main/java/game/models/deckReader/deck.json"), DeckFormat.class) ;
        ArrayList<String> names = deckFormat.getEnemy() ;
        return getCards(names) ;
    }
}
