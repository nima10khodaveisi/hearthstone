package game.models.deckReader;

import java.util.ArrayList;

public class DeckFormat {
    private ArrayList<String> friend ;
    private ArrayList<String> enemy ;

    public DeckFormat() { }

    public ArrayList<String> getFriend() {
        return friend;
    }

    public void setFriend(ArrayList<String> friend) {
        this.friend = friend;
    }

    public ArrayList<String> getEnemy() {
        return enemy;
    }

    public void setEnemy(ArrayList<String> enemy) {
        this.enemy = enemy;
    }
}
