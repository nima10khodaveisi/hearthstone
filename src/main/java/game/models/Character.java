package game.models;

import Models.Cards.Minions.Minion;
import game.players.gamePlayer.GamePlayer;
import javafx.event.EventHandler;
import javafx.scene.effect.Bloom;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public abstract class Character {
    protected transient GamePlayer player;
    protected transient Pane pane;
    protected boolean disable;
    protected int summonedTurn;
    private int hp;
    private int attack;
    protected transient EventHandler eventHandler;
    protected boolean selected;
    protected int id;


    public Character() {
    }


    public void attack(Character target) {
        setSummonedTurn(player.getTurn());
        if (this instanceof SummonCard && ((SummonCard) (this)).isDivineShield()) {
            ((SummonCard) (this)).setDivineShield(false);
        } else {
            damageHp(target.getAttack());
        }
        if (target instanceof SummonCard && ((SummonCard) (target)).isDivineShield()) {
            ((SummonCard) (target)).setDivineShield(false);
        } else {
            target.damageHp(getAttack());
        }
    }

    public void healHp(int value) {
        damageHp(-value);
    }

    public void damageHp(int value) {
        if (value > 0) {
            if (this instanceof SummonCard) {
                SummonCard summonCard = (SummonCard) (this);
                if (summonCard.getCard().getName().equals("Security Rover")) {
                    Minion minion = new Minion("Dreadscale");
                    SummonCard card = new SummonCard(minion, getPlayer());
                    card.setTaunt(true);
                    card.setHp(3);
                    card.setAttack(2);
                    player.summonCard(card);
                }
            }
        }
        setHp(getHp() - value);
    }

    public abstract void updatePane();

    public abstract void setDisable(boolean value);

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
        if (this instanceof HeroGame && this.hp <= 0) {
            player.getGameState().win(player.getEnemy());
        }
        updatePane();
    }

    public abstract double getX();

    public abstract double getY();

    public abstract void clickEventHandler();

    public abstract void restoreHeal(int value);

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
        updatePane();
    }

    public void removeEventHandler() {
        if (pane != null && eventHandler != null)
            pane.removeEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
    }

    public void addEventHandler() {
        if (pane != null && eventHandler != null) {
            pane.setOnMouseClicked(eventHandler) ;
        }
    }

    public Pane getPane() {
        return pane;
    }

    public void setPane(Pane pane) {
        this.pane = pane;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if(pane != null) {
            if (getSelected()) {
                Bloom bloom = new Bloom();
                this.pane.setEffect(bloom);
            } else {
                this.pane.setEffect(null);
            }
            setPane(null);
        }
    }

    public boolean getSelected() {
        return selected;
    }


    public int getSummonedTurn() {
        return summonedTurn;
    }

    public void setSummonedTurn(int summonedTurn) {
        this.summonedTurn = summonedTurn;
    }

    public GamePlayer getPlayer() {
        return player;
    }

    public void setPlayer(GamePlayer player) {
        this.player = player;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
