package game.models;

import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.application.Platform;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.Collections;

public class Summoned {
    private transient GamePlayer player;
    private ArrayList<SummonCard> cards;
    private transient HBox hBox;

    public Summoned() {
    }

    public Summoned(boolean value) {
        cards = new ArrayList<>();
    }

    public void sethBox(HBox hBox) {
        Platform.runLater(()-> {
            this.hBox = hBox;
            updateHbox();
        }) ;
    }

    private void check() {
        ArrayList<SummonCard> remove = new ArrayList<>();
        for (SummonCard card : cards) {
            if (card.getHp() <= 0) {
                remove.add(card);
            }
        }
        for (SummonCard card : remove)
            cards.remove(card);
        remove.clear();
    }


    public void updateHbox() {
        if (player.isGraphical()) {
            if (hBox == null)
                return;
            this.hBox.getChildren().clear();
            check();
            int cnt = 0;
            for (SummonCard card : cards) {
                card.setGraphical();
                addCardToHbox(card);
                card.setId(cnt);
                cnt++;
            }
        } else {
            player.updateGameGraphic();
            if (player.getEnemy() != null)
                player.getEnemy().updateGameGraphic();
        }
    }

    public void setCards(ArrayList<SummonCard> cards) {
        Platform.runLater(()-> {
            this.cards = cards;
            updateHbox();
        });
    }

    public void addCard(SummonCard card) {
        Platform.runLater(()-> {
            cards.add(card);
            Collections.sort(cards);
            card.setDropX(card.getX());
            updateHbox();
            card.setDropX(card.getX());
        });
    }

    public void setDisable(boolean value) {
        for (SummonCard card : cards)
            card.setDisable(value);
    }

    public void addCardToHbox(SummonCard card) {
        if(hBox != null)
           hBox.getChildren().add(card.getPane());
    }

    public int size() {
        return cards.size();
    }

    public ArrayList<SummonCard> getCards() {
        return cards;
    }

    public HBox gethBox() {
        return hBox;
    }

    public GamePlayer getPlayer() {
        return player;
    }

    public void setPlayer(GamePlayer player) {
        this.player = player;
        for (SummonCard summonCard : getCards()) {
            summonCard.setPlayer(player);
        }
    }

    @Override
    public String toString() {
        String ret = "";
        for (SummonCard card : cards) {
            ret += "summoncard : " + card.getCard().getName();
            ret += '\n';
        }
        return ret;
    }
}
