package game.models;

import Models.Cards.Minions.Minion;
import Models.Heros.Hero;
import game.GameState;
import game.actions.Attack;
import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class SummonCard extends Character implements Comparable<SummonCard> {
    private Minion card;
    private double dropX;
    //actions
    private boolean taunt;
    private boolean rush;
    private boolean divineShield;

    public SummonCard() { }

    public SummonCard(Minion card, GamePlayer gamePlayer) {
        this.player = gamePlayer ;
        disable = false;
        this.card = (Minion) card.copy();
        setAttack((int) this.card.getAttack());
        setHp((int) this.card.getHp());
        setPane(Images.getInstance().getSummonCard(card));
        setTaunt(card.isTaunt());
        setRush(card.isRush());
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                if (disable || !player.isGraphical())
                    return;
                clickEventHandler();
            }
        };
    }

    public SummonCard(Minion card, double dropX, GamePlayer gamePlayer) {
        this(card, gamePlayer);
        setDropX(dropX);
    }

    public void setGraphical() {
        setPane(Images.getInstance().getSummonCard(card, getAttack(), getHp()));
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                if (disable || !player.isGraphical())
                    return;
                clickEventHandler();
            }
        };
        pane.setOnMouseClicked(eventHandler) ;
    }

    public void clickEventHandler() {
        Attack attack = player.getAttack();
        if (!getSelected()) {
            //select card
            if (attack.getAttacker() == null && getSummonedTurn() != player.getTurn()) {
                attack.setAttacker(this);
                setSelected(true);
            } else {
                attack.setDefender(this);
            }
        } else {
            //unselect card
            attack.setAttacker(null);
            setSelected(false);
        }
    }

    @Override
    public void updatePane() {
        Platform.runLater(()-> {
            if (player.isGraphical()) {
                setPane(Images.getInstance().getSummonCard(this.card, getAttack(), getHp()));
            } else {
                player.updateGameGraphic();
                player.getEnemy().updateGameGraphic();
            }
        }) ;
    }

    //actions

    @Override
    public void setDisable(boolean value) {
        disable = value;
    }

    public Minion getCard() {
        return card;
    }

    public void setCard(Minion card) {
        this.card = card;
    }

    public Pane getPane() {
        if(pane == null) {
            setPane(Images.getInstance().getSummonCard(this.card, getAttack(), getHp()));
        }
        return pane;
    }

    public void setPane(Pane pane) {
        if (pane != null) {
            this.pane = pane;
            setDisable(disable);
        }
        if(!player.isGraphical()) {
            if (player != null)
                player.updateGameGraphic();
            if (player.getEnemy() != null)
                player.getEnemy().updateGameGraphic();
        }
        //  GameState.getInstance().updateSummons();
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isDisable() {
        return disable;
    }

    public void restoreHeal() {
        setHp((int) this.card.getHp());
    }

    public void restoreHeal(int value) {
        if (getHp() + value >= this.card.getHp())
            restoreHeal();
        else {
            setHp(getHp() + value);
        }
    }

    public double getDropX() {
        return dropX;
    }

    public void setDropX(double dropX) {
        this.dropX = dropX;
    }

    public boolean isTaunt() {
        return taunt;
    }

    public void setTaunt(boolean taunt) {
        this.taunt = taunt;
        this.card.setTaunt(taunt);
        updatePane();
    }

    public boolean isRush() {
        return rush;
    }

    public void setRush(boolean rush) {
        this.rush = rush;
    }

    @Override
    public double getX() {
        Bounds bounds = pane.localToScene(pane.getBoundsInLocal());
        return bounds.getMinX();
    }

    @Override
    public double getY() {
        Bounds bounds = pane.localToScene(pane.getBoundsInLocal());
        return bounds.getMinY();
    }

    @Override
    public int compareTo(SummonCard summonCard) {
        if (this.getDropX() < summonCard.getDropX())
            return -1;
        else if (this.getDropX() > summonCard.getDropX())
            return 1;
        return 0;
    }

    public boolean isDivineShield() {
        return divineShield;
    }

    public void setDivineShield(boolean divineShield) {
        this.divineShield = divineShield;
    }
}