package game.models;

import game.players.gamePlayer.GamePlayer;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public abstract class HeroPower {
    protected transient GamePlayer player ;
    protected int mana ;
    protected int turn ;
    protected String name ;
    protected transient ImageView imageView ;
    protected transient EventHandler eventHandler ;

    public HeroPower() { }

    public abstract void run() ;

    public abstract void updatePane() ;

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public ImageView getPane() {
        return imageView;
    }

    public void addEventHandler() {
        imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler) ;
    }

    public void removeEventHandler() {
        if(imageView != null)
            imageView.removeEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler) ;
    }

    public void setPane(ImageView imageView) {
        removeEventHandler();
        this.imageView = imageView ;
        addEventHandler();
        updatePane() ;
    }

    public GamePlayer getPlayer() {
        return player;
    }

    public void setPlayer(GamePlayer player) {
        this.player = player;
    }
}
