package game.actions;

import game.actions.abilities.Selectable;
import game.models.Character;
import game.players.gamePlayer.GamePlayer;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.effect.SepiaTone;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;

public class Select extends Thread {
    private ArrayList<Character> targets;
    private ArrayList<EventHandler> eventHandlers;
    private Character selected;
    private Selectable action;
    private GamePlayer gamePlayer ;

    public Select(ArrayList<Character> targets, Selectable action, GamePlayer gamePlayer) {
        this.targets = targets;
        this.action = action;
        this.gamePlayer = gamePlayer ;
    }

    @Override
    public void run() {
        eventHandlers = new ArrayList<>();
        for (Character character : targets) {
            character.removeEventHandler();
            EventHandler eventHandler = new EventHandler() {
                @Override
                public void handle(Event event) {
                    try {
                        select(character);
                    } catch (Exception e) {
                    }
                }
            };
            character.getPane().addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
            eventHandlers.add(eventHandler);
            character.getPane().setDisable(false);
            character.getPane().setEffect(new SepiaTone()) ;
        }
    }

    public void select(Character character) throws Exception {
        selected = character;
        for (int i = 0; i < targets.size(); ++i) {
            Character target = targets.get(i);
            target.getPane().removeEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlers.get(i));
            target.addEventHandler();
            target.getPane().setEffect(null);
        }
        action.selected(character) ;
    }

    public ArrayList<Character> getTargets() {
        return targets;
    }

    public void setTargets(ArrayList<Character> targets) {
        this.targets = targets;
    }

    public Character getSelected() {
        return selected;
    }

}
