package game.actions.abilities;

import game.models.Character;

public interface Selectable {
    public void selected(Character character) ;
}
