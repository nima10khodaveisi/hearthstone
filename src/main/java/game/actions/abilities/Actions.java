package game.actions.abilities;

import game.players.gamePlayer.GamePlayer;

import java.lang.reflect.InvocationTargetException;

public abstract class Actions {

    protected GamePlayer player ;

    public Actions(GamePlayer player) {
        this.player = player ;
    }

    public abstract void BattleCry() throws InterruptedException;

    public abstract void DeathRattle() ;

    public static Actions getActions(String name, GamePlayer player) {
        String str = "" ;
        for(int i = 0 ; i < name.length(); ++i) {
            char c = name.charAt(i) ;
            if(c == ' ' || c == '\'') continue ;
            str += c ;
        }
        try {
            Class c = Class.forName("game.actions.abilities.cards." + str) ;
            Object o = c.getConstructor(GamePlayer.class).newInstance(player) ;
            return (Actions)o ;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null ;
    }

}
