package game.actions.abilities.heroPowers;

import Models.Heros.Hero;
import game.actions.Select;
import game.actions.abilities.Selectable;
import game.models.Character;
import game.models.HeroPower;
import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.event.Event;
import javafx.event.EventHandler;

import java.util.Random;

public class PriestHeroPower extends HeroPower {

    public PriestHeroPower(GamePlayer player) {
        this.player = player ;
        mana = 2 ;
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                run();
            }
        } ;
    }

    @Override
    public void run() {
        if(player.getMana() < mana || player.getTurn() == getTurn())
            return;
        setTurn(player.getTurn()) ;
        player.setMana(player.getMana() - mana) ;
        Random random = new Random() ;
        selected(player.getCharacters().get(random.nextInt(player.getCharacters().size()))) ;
    }

    @Override
    public void updatePane() {
        imageView.setImage(Images.getInstance().getHeroPowerImage(new Hero("Priest"))) ;
    }

    public void selected(Character character) {
        character.restoreHeal(4) ;
    }
}
