package game.actions.abilities.heroPowers;

import Models.Heros.Hero;
import game.actions.Select;
import game.actions.abilities.Selectable;
import game.actions.abilities.cards.ShatteredSunCleric;
import game.models.Character;
import game.models.HeroPower;
import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.event.Event;
import javafx.event.EventHandler;
import network.json.MyJson;

import java.util.Random;

public class MageHeroPower extends HeroPower {
    public MageHeroPower(GamePlayer player) {
        this.player = player ;
        mana = 2 ;
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                run();
            }
        } ;
    }


    @Override
    public void updatePane() {
        imageView.setImage(Images.getInstance().getHeroPowerImage(new Hero("Mage"))) ;
    }


    public void selected(Character character) {
        player.getClientHandler().getSender().sendMessage(MyJson.create_json(
                "type", "cardAction",
                "username", character.getPlayer().getPlayer().getUsername(),
                "actionCard", ShatteredSunCleric.class.getName(),
                "character", character.getId()).toJSONString()
        );
    }


    public void doAction(Character character) {
        character.damageHp(1) ;
    }

    @Override
    public void run() {
        if(player.getMana() < mana || player.getTurn() == getTurn())
            return;
        setTurn(player.getTurn()) ;
        player.setMana(player.getMana() - mana) ;
        Random random = new Random() ;
        selected(player.getEnemy().getCharacters().get(random.nextInt(player.getEnemy().getCharacters().size()))) ;
    }
}
