package game.actions.abilities.heroPowers;

import Models.Heros.Hero;
import game.models.HeroPower;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.event.Event;
import javafx.event.EventHandler;

import java.util.Random;

public class WarlockHeroPower extends HeroPower {

    public WarlockHeroPower(GamePlayer player) {
        this.player = player ;
        mana = 0 ;
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                run();
            }
        } ;
    }

    @Override
    public void run() {
        if(player.getMana() < mana || player.getTurn() == getTurn())
            return;
        setTurn(player.getTurn()) ;
        player.setMana(player.getMana() - mana) ;
        player.getHero().damageHp(2) ;
        Random random = new Random() ;
        if(random.nextInt() % 2 == 0 && player.getSummoned().size() > 0) {
            SummonCard summonCard = player.getSummoned().getCards().get(0) ;
            summonCard.setAttack(summonCard.getAttack() + 1) ;
            summonCard.setHp(summonCard.getHp() + 1) ;
        } else {
            player.draw();
        }
    }

    @Override
    public void updatePane() {
        imageView.setImage(Images.getInstance().getHeroPowerImage(new Hero("Warlock"))) ;
    }
}
