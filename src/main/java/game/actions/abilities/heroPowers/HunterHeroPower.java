package game.actions.abilities.heroPowers;

import game.actions.abilities.quest.QuestAndRewardActions;
import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class HunterHeroPower extends QuestAndRewardActions {
    public HunterHeroPower(GamePlayer player) {
        super(player);
    }

    @Override
    public boolean quest() {
        return false;
    }

    @Override
    public void reward() {

    }

    @Override
    public void update(Object o) {
        SummonCard summonCard = (SummonCard) o ;
        summonCard.setHp(summonCard.getHp() - 1) ;
    }

    @Override
    public void start() {
        Listeners.getListener(player, "OpponentPlaysMinion").addCard(this) ;
    }
}
