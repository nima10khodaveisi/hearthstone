package game.actions.abilities.heroPowers;

import Models.Cards.Card;
import Models.Heros.Hero;
import game.models.HeroPower;
import game.players.gamePlayer.GamePlayer;
import graphic.images.Images;
import javafx.event.Event;
import javafx.event.EventHandler;

public class RogueHeroPower extends HeroPower {

    public RogueHeroPower(GamePlayer player) {
        this.player = player ;
        mana = 3 ;
        eventHandler = new EventHandler() {
            @Override
            public void handle(Event event) {
                run();
            }
        } ;
    }

    @Override
    public void run() {
        if(player.getMana() < mana || player.getTurn() == getTurn())
            return;
        setTurn(player.getTurn()) ;
        player.setMana(player.getMana() - mana) ;
        GamePlayer enemy = player.getEnemy() ;
        Card card = enemy.getDeck().get(0) ;
        enemy.getDeck().removeCard(card) ;
        try {
            ((GamePlayer)(player)).addToHand(card) ;
        } catch (Exception e) {
        }
    }

    @Override
    public void updatePane() {
        imageView.setImage(Images.getInstance().getHeroPowerImage(new Hero("Rogue"))) ;
    }
}
