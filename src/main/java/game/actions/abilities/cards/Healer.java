package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Healer extends Actions {

    public Healer(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        for(SummonCard card : player.getSummoned().getCards())
            card.restoreHeal();
    }

    @Override
    public void DeathRattle() {

    }
}
