package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.util.Random;

public class PharaohsBlessing extends Actions {

    public PharaohsBlessing(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        Random random = new Random() ;
        SummonCard card = player.getSummoned().getCards().get(random.nextInt(player.getSummoned().size())) ;
        card.setTaunt(true) ;
        card.setHp(card.getHp() + 4) ;
        card.setAttack(card.getAttack() + 4) ;
        card.setDivineShield(true) ;
    }

    @Override
    public void DeathRattle() {

    }
}
