package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.util.ArrayList;
import java.util.Random;

public class Polymorph extends Actions {

    public Polymorph(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        ArrayList<SummonCard> all = new ArrayList<>();
        for (SummonCard card : player.getSummoned().getCards())
            all.add(card);
        for (SummonCard card : player.getEnemy().getSummoned().getCards())
            all.add(card);
        if (!all.isEmpty()) {
            Random random = new Random();
            SummonCard card = all.get(random.nextInt(all.size()));
            card.setAttack(1);
            card.setHp(1);
        }
    }

    @Override
    public void DeathRattle() {

    }
}
