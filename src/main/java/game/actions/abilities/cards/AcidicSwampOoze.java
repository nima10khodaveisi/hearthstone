package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.players.gamePlayer.GamePlayer;

public class AcidicSwampOoze extends Actions {
    private GamePlayer player ;

    public AcidicSwampOoze(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        GamePlayer enemy = player.getEnemy() ;
        enemy.getHero().setDurability(0) ;
        enemy.getHero().setAttack(0) ;
    }

    @Override
    public void DeathRattle() {
        //empty
    }
}
