package game.actions.abilities.cards;

import Models.Cards.Card;
import Models.Cards.Minions.Minion;
import game.actions.Select;
import game.actions.abilities.Actions;
import game.actions.abilities.Selectable;
import game.models.Character;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;
import network.json.MyJson;

import java.util.ArrayList;
import java.util.Random;

public class Sathrovarr extends Actions {

    public Sathrovarr(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        Random random = new Random() ;
        selected(player.getSummoned().getCards().get(random.nextInt(player.getSummoned().size()))) ;
    }

    public void selected(Character character) {


        player.getClientHandler().getSender().sendMessage(MyJson.create_json(
                "type", "cardAction",
                "username", character.getPlayer().getPlayer().getUsername(),
                "actionCard", ShatteredSunCleric.class.getName(),
                "character", character.getId()).toJSONString()
        );
    }

    public void doAction(Character character) {
        GamePlayer gamePlayer = (GamePlayer) (player);
        Card card = ((SummonCard) (character)).getCard();
        try {
            gamePlayer.addToHand(card);
        } catch (Exception e) {
        }
        gamePlayer.getDeck().addCard(card);
        SummonCard summonCard = new SummonCard((Minion) card, player);
        if (player.getSummoned().size() < player.MAX_SUMMON_SIZE) {
           /* player.getSummoned().addCard(summonCard);
            player.getCharacters().add(summonCard);
            summonCard.setDisable(true);
            summonCard.setSummonedTurn(player.getTurn());*/
            player.summonCard(summonCard) ;
        }
    }

    @Override
    public void DeathRattle() {

    }
}
