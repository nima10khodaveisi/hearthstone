package game.actions.abilities.cards;

import Models.Cards.Minions.Minion;
import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Tombwarden extends Actions {

    public Tombwarden(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        Minion minion = new Minion("Tomb warden");
        SummonCard card = new SummonCard(minion, player);
        if (player.getSummoned().size() < player.MAX_SUMMON_SIZE) {
            player.summonCard(card);
        }
    }

    @Override
    public void DeathRattle() {

    }
}
