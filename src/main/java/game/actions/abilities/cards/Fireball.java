package game.actions.abilities.cards;

import game.actions.Select;
import game.actions.abilities.Actions;
import game.actions.abilities.Selectable;
import game.models.Character;
import game.players.gamePlayer.GamePlayer;
import network.json.MyJson;

import java.util.ArrayList;
import java.util.Random;

public class Fireball extends Actions  {

    public Fireball(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() throws InterruptedException {
        //deal 6 damage to enemy's hero
        Random random = new Random() ;
        selected(player.getEnemy().getCharacters().get(random.nextInt(player.getEnemy().getCharacters().size()))) ;
    }

    public void selected(Character character) {
        //character.damageHp(6) ;
        player.getClientHandler().getSender().sendMessage(MyJson.create_json(
                "type", "cardAction",
                "username", character.getPlayer().getPlayer().getUsername(),
                "actionCard", ShatteredSunCleric.class.getName(),
                "character", character.getId()).toJSONString()
        );
    }


    public void doAction(Character character) {
        character.damageHp(6) ;
    }

    @Override
    public void DeathRattle() {

    }
}
