package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class HolyNova extends Actions {

    public HolyNova(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        GamePlayer enemy = player.getEnemy() ;
        for(SummonCard card : enemy.getSummoned().getCards()) {
            card.damageHp(2) ;
        }
        int value = 2 ;
        if(player.getHero().getHero().getName().equals("Priest"))
            value *= 2 ;
        for(SummonCard card : player.getSummoned().getCards())
            card.restoreHeal(value) ;
        player.getHero().restoreHeal(value) ;
    }

    @Override
    public void DeathRattle() {

    }
}
