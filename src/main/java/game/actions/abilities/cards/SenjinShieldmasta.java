package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.players.gamePlayer.GamePlayer;

public class SenjinShieldmasta extends Actions {

    public SenjinShieldmasta(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        //empty
    }

    @Override
    public void DeathRattle() {
        //empty
    }
}
