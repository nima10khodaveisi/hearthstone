package game.actions.abilities.cards;

import Models.Cards.Minions.Minion;
import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Swarmoflocusts extends Actions {

    public Swarmoflocusts(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        for(int i = 0 ; i < 7 ; ++i) {
            if(player.getSummoned().size() < player.MAX_SUMMON_SIZE) {
                Minion minion = new Minion("Bonemare");
                SummonCard summonCard = new SummonCard(minion, player);
                summonCard.setAttack(1);
                summonCard.setHp(1);
                player.summonCard(summonCard) ;
            }
        }
    }

    @Override
    public void DeathRattle() {

    }
}
