package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.players.gamePlayer.GamePlayer;

public class Currmon extends Actions {

    public Currmon(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        //TODO you can use hero power again
        player.getHeroPower().setTurn(player.getHeroPower().getTurn() - 1) ;
        player.getHeroPower().addEventHandler();
    }

    @Override
    public void DeathRattle() {

    }
}
