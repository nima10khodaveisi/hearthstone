package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.util.ArrayList;

public class Sina extends Actions {
    public Sina(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        //TODO swap cards in battlefield
        ArrayList<SummonCard> friend = player.getSummoned().getCards() ;
        ArrayList<SummonCard> enemy = player.getEnemy().getSummoned().getCards() ;
        player.getSummoned().setCards(enemy) ;
        player.getEnemy().getSummoned().setCards(friend) ;
        player.updateGameGraphic();
        if(player.getEnemy() != null)
            player.getEnemy().updateGameGraphic();
    }

    @Override
    public void DeathRattle() {

    }
}
