package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.util.ArrayList;

public class TheAmazingReno extends Actions {

    public TheAmazingReno(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        player.getSummoned().setCards(new ArrayList<SummonCard>());
        player.getEnemy().getSummoned().setCards(new ArrayList<SummonCard>());
        player.updateGameGraphic();
        if (player.getEnemy() != null)
            player.getEnemy().updateGameGraphic();
    }

    @Override
    public void DeathRattle() {

    }
}
