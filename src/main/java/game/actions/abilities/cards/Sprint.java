package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.players.gamePlayer.GamePlayer;

public class Sprint extends Actions {

    public Sprint(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        for(int i = 0 ; i < 4 ; ++i)
            player.draw();
    }

    @Override
    public void DeathRattle() {

    }
}
