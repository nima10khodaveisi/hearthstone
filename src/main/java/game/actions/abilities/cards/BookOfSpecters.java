package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.players.gamePlayer.GamePlayer;

public class BookOfSpecters extends Actions {

    public BookOfSpecters(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        for(int i = 0 ; i < 3 ; ++i) {
            player.draw() ;
        }
        //TODO discard any spells drawn
    }

    @Override
    public void DeathRattle() {

    }
}
