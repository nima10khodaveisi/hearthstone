package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.util.Random;

public class Bonemare extends Actions {

    public Bonemare(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        if(player.getSummoned().size() > 0) {
            Random random = new Random() ;
            SummonCard card = player.getSummoned().getCards().get(random.nextInt(player.getSummoned().size())) ;
            card.setAttack(card.getAttack() + 4) ;
            card.setHp(card.getHp() + 4) ;
            if(!card.isTaunt())
                card.setTaunt(true) ;
        }
    }

    @Override
    public void DeathRattle() {
        //empty
    }
}
