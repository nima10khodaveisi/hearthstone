package game.actions.abilities.cards;

import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Bolster extends Actions {

    public Bolster(GamePlayer player) {
        super(player) ;
    }

    @Override
    public void BattleCry() {
        //give your taunts minions +2/+2
        for(SummonCard card : player.getSummoned().getCards()) {
            if(card.isTaunt()) {
                card.setHp(card.getHp() + 2) ;
                card.setAttack(card.getAttack() + 2) ;
            }
        }
    }

    @Override
    public void DeathRattle() {
        //empty
    }
}
