package game.actions.abilities.cards;

import game.actions.Select;
import game.actions.abilities.Actions;
import game.actions.abilities.Selectable;
import game.models.Character;
import game.players.gamePlayer.GamePlayer;
import network.json.MyJson;

import java.util.ArrayList;
import java.util.Random;

public class ShatteredSunCleric extends Actions  {

    public ShatteredSunCleric(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        Random random = new Random() ;
        selected(player.getSummoned().getCards().get(random.nextInt(player.getSummoned().size()))) ;
    }

    public void selected(Character character) {
       // character.setAttack(character.getAttack() + 1);
       // character.setHp(character.getHp() + 1);

        player.getClientHandler().getSender().sendMessage(MyJson.create_json(
                "type", "cardAction",
                "username", character.getPlayer().getPlayer().getUsername(),
                "actionCard", ShatteredSunCleric.class.getName(),
                "character", character.getId()).toJSONString()
        );
    }

    @Override
    public void DeathRattle() {

    }
}
