package game.actions.abilities.cards;

import Models.Cards.Minions.Minion;
import game.actions.abilities.Actions;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class SaroniteChainGang extends Actions {

    public SaroniteChainGang(GamePlayer player) {
        super(player);
    }

    @Override
    public void BattleCry() {
        if(player.getSummoned().size() < player.MAX_SUMMON_SIZE) {
            Minion minion = new Minion("Saronite Chain Gang");
            SummonCard card = new SummonCard(minion, player);
            player.summonCard(card) ;
        }
    }

    @Override
    public void DeathRattle() {

    }
}
