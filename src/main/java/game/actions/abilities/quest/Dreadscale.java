package game.actions.abilities.quest;

import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Dreadscale extends QuestAndRewardActions {

    private SummonCard summonCard ;

    public Dreadscale(GamePlayer player, SummonCard summonCard) {
        super(player) ;
        this.summonCard = summonCard ;
    }

    @Override
    public boolean quest() {
        return (summonCard.getHp() <= 0);
    }

    @Override
    public void reward() {
        //empty
    }

    @Override
    public void update(Object o) {
        GamePlayer enemy = player.getEnemy() ;
        for(SummonCard defender : enemy.getSummoned().getCards()) {
            if(summonCard.getHp() > 0) {
                defender.setHp(defender.getHp() - 1) ;
            }
        }
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, "EndTurn") ;
        listener.addCard(this) ;
    }
}
