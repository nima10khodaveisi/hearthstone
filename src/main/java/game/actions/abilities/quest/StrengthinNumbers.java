package game.actions.abilities.quest;

import Models.Cards.Card;
import Models.Cards.CardType;
import Models.Cards.Minions.Minion;
import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class StrengthinNumbers extends QuestAndRewardActions {

    public StrengthinNumbers(GamePlayer player, SummonCard summonCard) {
        super(player) ;
    }

    private int spendMana = 0;
    private int need = 10;
    private String type = "SpendManaOnMinions";

    @Override
    public boolean quest() {
        if (spendMana >= need)
            return true;
        return false;
    }

    @Override
    public void reward() {
        if(player.getDeck().size() > 0) {


            for(Card card : player.getDeck().getCards()) {
                if(card.getName().equals("Security Rover")) {
                    Minion minion = (Minion) card ;
                    SummonCard summonCard = new SummonCard(minion, player) ;
                    player.getSummoned().addCard(summonCard) ;
                    player.getCharacters().add(summonCard) ;
                    summonCard.setDisable(true) ;
                    summonCard.setSummonedTurn(player.getTurn()) ;
                    player.getDeck().removeCard(card) ;
                    player.updateGameGraphic();
                    if(player.getEnemy() != null)
                        player.getEnemy().updateGameGraphic();
                    return ;
                }
            }

            for(Card card : player.getDeck().getCards()) {
                if(card.getType() == CardType.MINION) {
                    Minion minion = (Minion) card ;
                    SummonCard summonCard = new SummonCard(minion, player) ;
                    player.getSummoned().addCard(summonCard) ;
                    player.getCharacters().add(summonCard) ;
                    summonCard.setDisable(true) ;
                    summonCard.setSummonedTurn(player.getTurn()) ;
                    player.getDeck().removeCard(card) ;
                    player.updateGameGraphic();
                    if(player.getEnemy() != null)
                      player.getEnemy().updateGameGraphic();
                    return ;
                }
            }
        }
    }

    @Override
    public void update(Object o) {
        int value = (Integer) o ;
        if (quest())
            return;
        spendMana += value;
        if (quest()) {
            reward();
        }
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, type) ;
        listener.addCard(this) ;
    }
}
