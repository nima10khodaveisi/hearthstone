package game.actions.abilities.quest;

import Models.Cards.Minions.Minion;
import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class LearnDraconic extends QuestAndRewardActions {

    public LearnDraconic(GamePlayer player, SummonCard summonCard) {
        super(player);
    }

    private int spendMana = 0;
    private int need = 8;
    private String type = "SpendManaOnSpells";

    @Override
    public boolean quest() {
        if (spendMana >= need)
            return true;
        return false;
    }

    @Override
    public void reward() {
        Minion minion = new Minion("Saronite Chain Gang");
        minion.setHp(6);
        minion.setAttack(6);
        SummonCard summonCard = new SummonCard(minion, player);
        if (player.getSummoned().size() < player.MAX_SUMMON_SIZE) {
            player.getSummoned().addCard(summonCard);
            player.getCharacters().add(summonCard);
            summonCard.setSummonedTurn(player.getTurn());
            summonCard.setDisable(true);
        }
    }

    @Override
    public void update(Object o) {
        int value = (Integer) o ;
        if (quest())
            return;
        spendMana += value;
        if (quest()) {
            reward();
        }
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, type) ;
        listener.addCard(this) ;
    }
}
