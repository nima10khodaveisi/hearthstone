package game.actions.abilities.quest;

import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.lang.reflect.InvocationTargetException;

public abstract class QuestAndRewardActions {

    public QuestAndRewardActions(GamePlayer player) {
        this.player = player ;
    }

    protected GamePlayer player ;

    public abstract boolean quest() ;

    public abstract void reward() ;

    public abstract void update(Object o) ;

    public abstract void start() ;


    public static QuestAndRewardActions getQuest(String name, GamePlayer player, SummonCard summonCard) {
        String str = "" ;
        for(int i = 0 ; i < name.length(); ++i) {
            char c = name.charAt(i) ;
            if(c == ' ' || c == '\'') continue ;
            str += c ;
        }
        try {
            Class c = Class.forName("game.actions.abilities.quest." + str) ;
            Object o = c.getConstructor(GamePlayer.class, SummonCard.class).newInstance(player, summonCard) ;
            return (QuestAndRewardActions) o ;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null ;
    }
    public static QuestAndRewardActions getQuest(String name, GamePlayer player) {
        String str = "" ;
        for(int i = 0 ; i < name.length(); ++i) {
            char c = name.charAt(i) ;
            if(c == ' ' || c == '\'') continue ;
            str += c ;
        }
        try {
            Class c = Class.forName("game.actions.abilities.quest." + str) ;
            Object o = c.getConstructor(GamePlayer.class).newInstance(player) ;
            return (QuestAndRewardActions) o ;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null ;
    }
}
