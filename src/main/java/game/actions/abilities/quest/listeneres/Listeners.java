package game.actions.abilities.quest.listeneres;

import game.actions.abilities.quest.QuestAndRewardActions;
import game.players.gamePlayer.GamePlayer;

import java.util.ArrayList;

public abstract class Listeners {
    protected String type;
    protected ArrayList<QuestAndRewardActions> cards;

    public void addCard(QuestAndRewardActions card) {
        cards.add(card);
    }

    public abstract void update(Object o);

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public static Listeners getListener(GamePlayer player, String type) {
        for (Listeners listeners : player.getListeners())
            if (listeners.getType().equals(type))
                return listeners;
        return null;
    }
}
