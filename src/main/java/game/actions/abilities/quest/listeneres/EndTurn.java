package game.actions.abilities.quest.listeneres;

import Models.Cards.Card;
import game.actions.abilities.quest.LearnDraconic;
import game.actions.abilities.quest.QuestAndRewardActions;
import game.actions.abilities.quest.Swampkingdred;
import game.models.SummonCard;

import java.util.ArrayList;

public class EndTurn extends Listeners{


    public EndTurn() {
        type = "EndTurn" ;
        cards = new ArrayList<>() ;
    }

    @Override
    public void update(Object o) { // object should be null
        ArrayList<QuestAndRewardActions> remove = new ArrayList<>() ;
        for(QuestAndRewardActions card : cards) {
            card.update(null) ;
            if(card.quest())
                remove.add(card) ;
        }
        for(QuestAndRewardActions card : remove)
            cards.remove(card) ;
    }
}
