package game.actions.abilities.quest.listeneres;

import game.actions.abilities.quest.LearnDraconic;
import game.actions.abilities.quest.QuestAndRewardActions;
import game.actions.abilities.quest.Swampkingdred;
import game.models.SummonCard;

import java.util.ArrayList;

public class OpponentPlaysMinion extends Listeners{

    public OpponentPlaysMinion() {
        type = "OpponentPlaysMinion" ;
        cards = new ArrayList<>() ;
    }

    @Override
    public void update(Object o) {
        SummonCard summonCard = (SummonCard) o ;
        ArrayList<QuestAndRewardActions> remove = new ArrayList<>() ;
        for(QuestAndRewardActions quest : cards) {
            quest.update(summonCard) ;
            if(quest.quest())
                remove.add(quest) ;
        }
        for(QuestAndRewardActions card : remove)
            cards.remove(card) ;
    }
}
