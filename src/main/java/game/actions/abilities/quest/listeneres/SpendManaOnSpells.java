package game.actions.abilities.quest.listeneres;

import game.actions.abilities.quest.LearnDraconic;
import game.actions.abilities.quest.QuestAndRewardActions;

import java.util.ArrayList;

public class SpendManaOnSpells extends Listeners{

    public SpendManaOnSpells() {
        type = "SpendManaOnSpells" ;
        cards = new ArrayList<>() ;
    }

    @Override
    public void update(Object o) {
        Integer value = (Integer) o ;
        ArrayList<QuestAndRewardActions> remove = new ArrayList<>() ;
        for(QuestAndRewardActions card : cards) {
            card.update(value.intValue());
            if (card.quest())
                remove.add(card);
        }
        for(QuestAndRewardActions card : remove)
            cards.remove(card) ;
    }
}
