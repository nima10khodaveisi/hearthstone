package game.actions.abilities.quest;

import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

import java.util.Random;

public class CobaltScalebone extends QuestAndRewardActions {

    private SummonCard summonCard ;

    public CobaltScalebone(GamePlayer player, SummonCard summonCard) {
        super(player) ;
        this.summonCard = summonCard ;
    }

    @Override
    public boolean quest() {
        return (summonCard.getHp() <= 0);
    }

    @Override
    public void reward() {
        //empty
    }

    @Override
    public void update(Object o) {
        if(quest())
            return;
        if(player.getSummoned().size() > 0) {
            Random random = new Random() ;
            SummonCard card = player.getSummoned().getCards().get(random.nextInt(player.getSummoned().size())) ;
            card.setAttack(card.getAttack() + 3) ;
        }
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, "EndTurn") ;
        listener.addCard(this) ;
    }
}
