package game.actions.abilities.quest;

import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class CurioCollector extends QuestAndRewardActions {

    private SummonCard summonCard ;

    public CurioCollector(GamePlayer player, SummonCard summonCard) {
        super(player) ;
        this.summonCard = summonCard ;
    }

    @Override
    public boolean quest() {
        return (summonCard.getHp() <= 0) ;
    }

    @Override
    public void reward() {
        //empty
    }

    @Override
    public void update(Object o) {
        summonCard.setAttack(summonCard.getAttack() + 1) ;
        summonCard.setHp(summonCard.getHp() + 1) ;
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, "DrawCard") ;
        listener.addCard(this) ;
    }
}
