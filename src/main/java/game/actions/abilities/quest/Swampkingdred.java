package game.actions.abilities.quest;

import game.actions.Attack;
import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Swampkingdred extends QuestAndRewardActions {

    private SummonCard summonCard ;

    public Swampkingdred(GamePlayer player, SummonCard summonCard) {
        super(player) ;
        this.summonCard = summonCard ;
    }

    @Override
    public boolean quest() {
        return (summonCard.getHp() <= 0) ;
    }

    @Override
    public void reward() {
        //empty
    }

    @Override
    public void update(Object o) {
        if(summonCard.getHp() <= 0)
            return;
        SummonCard defender = (SummonCard) o ;
        int attackerId = -1 ;
        int defenderId = -1 ;
        for(int i = 0 ; i < player.getSummoned().size() ; ++i) {
            if(player.getSummoned().getCards().get(i) == summonCard)
                attackerId = i ;
        }

        for(int i = 0 ; i < player.getEnemy().getSummoned().size() ; ++i) {
            if(player.getEnemy().getSummoned().getCards().get(i) == defender)
                defenderId = i ;
        }
        player.getGameState().attackAction(attackerId, defenderId) ;
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, "OpponentPlaysMinion") ;
        listener.addCard(this) ;
    }
}
