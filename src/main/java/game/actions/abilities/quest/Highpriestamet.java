package game.actions.abilities.quest;

import game.actions.abilities.quest.listeneres.Listeners;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;

public class Highpriestamet extends QuestAndRewardActions {

    private SummonCard summonCard ;

    public Highpriestamet(GamePlayer player, SummonCard summonCard) {
        super(player);
        this.summonCard = summonCard ;
    }

    private String type = "SummonMinion";

    @Override
    public boolean quest() {
        return (summonCard.getHp() <= 0) ;
    }

    @Override
    public void reward() {
        //empty
    }

    @Override
    public void update(Object o) {
        SummonCard summonCard = (SummonCard) o ;
        summonCard.setHp(this.summonCard.getHp()) ;
    }

    @Override
    public void start() {
        Listeners listener = Listeners.getListener(player, type) ;
        listener.addCard(this) ;
    }
}
