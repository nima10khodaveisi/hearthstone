package game.actions;

import game.GameState;
import game.models.Character;
import game.models.SummonCard;
import game.players.gamePlayer.GamePlayer;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import network.client.Client;
import network.json.MyJson;

public class Attack {
    private Character attacker;
    private Character defender;
    private GameState gameState;
    private GamePlayer gamePlayer;
    private Client client ;

    public Attack() {
    }

    public Attack(GamePlayer gamePlayer, Client client) {
        this.gamePlayer = gamePlayer;
        this.client = client ;
    }

    public Attack(GameState gameState) {
        this.gameState = gameState;
    }

    private void graphicExecute() {
        TranslateTransition transition = new TranslateTransition();
        transition.setToX(defender.getX() - attacker.getX());
        transition.setToY(defender.getY() - attacker.getY());
        transition.setDuration(new Duration(1000));
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        transition.setNode(attacker.getPane());
        transition.play();
        transition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                client.getGameSender().sendMessage(MyJson.create_json(
                        "type", "attackAction",
                        "attacker", attacker.getId(),
                        "defender", defender.getId()).toJSONString()) ;
                attacker.setSelected(false) ;
                attacker = null ;
                defender = null ;
            }
        });
    }

    private void execute() {
        graphicExecute();
    }

    public Character getAttacker() {
        return attacker;
    }

    public void setAttacker(Character attacker) {
        if (attacker != null && attacker.getAttack() <= 0)
            return;
        if (attacker == null) {
//            gameState.enableOtherPlayerCharacters(true);
//            gameState.enableOtherPlayerCharacters(false);
            GamePlayer myPlayer = this.attacker.getPlayer() ;
            GamePlayer otherPlayer = myPlayer.getEnemy() ;
            for(Character character : myPlayer.getCharacters())
                character.setDisable(false) ;
            for(Character character : otherPlayer.getCharacters())
                character.setDisable(true) ;
        } else {
//            gameState.enableCurPlayerSummonCards(false);
//            gameState.enableOtherPlayerCharacters(true);
            GamePlayer myPlayer = attacker.getPlayer() ;
            GamePlayer otherPlayer = myPlayer.getEnemy() ;
            for(Character character : myPlayer.getCharacters()) {
                character.setDisable(true);
            }
            attacker.setDisable(false) ;
            for(Character character : otherPlayer.getCharacters()) {
                character.setDisable(false);
            }
        }
        this.attacker = attacker ;
    }

    public Character getDefender() {
        return defender;
    }

    public void setDefender(Character defender) {
        if (defender == getAttacker())
            return;
        boolean taunt = false;
        GamePlayer enemy = defender.getPlayer();
        for (SummonCard summonCard : enemy.getSummoned().getCards()) {
            taunt |= summonCard.isTaunt();
        }
        if (taunt) {
            if (defender instanceof SummonCard) {
                if (!((SummonCard) (defender)).isTaunt()) {
                    return;
                }
            } else {
                return;
            }
        }
        this.defender = defender;
        try {
            execute();
        } catch (Exception e) {
        }
    }
}
