package game.players.gamePlayer;

import Models.Cards.Card;
import Models.Cards.CardType;
import Models.Cards.Minions.Minion;
import Models.Cards.Spells.Spell;
import Models.Heros.Hero;
import Player.Player;
import Player.Deck;
import com.google.gson.Gson;
import game.GameState;
import game.InfoPassive;
import game.actions.Attack;
import game.actions.abilities.Actions;
import game.actions.abilities.heroPowers.*;
import game.actions.abilities.quest.QuestAndRewardActions;
import game.actions.abilities.quest.listeneres.*;
import game.models.*;
import game.models.Character;
import game.models.deckReader.DeckReader;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import network.json.MyJson;
import network.server.ClientHandler;

import java.util.ArrayList;
import java.util.Random;

public class GamePlayer {
    protected transient GamePlayer enemy;
    private transient GameState gameState ;
    protected ArrayList<Card> hand;
    protected transient ArrayList<Character> characters;
    protected Summoned summoned;
    protected Deck deck;
    protected int mana;
    protected HeroGame hero;
    protected int turn;
    protected InfoPassive infoPassive;
    protected transient HeroPower heroPower;
    protected transient ArrayList<Listeners> listeners;
    public final int MAX_HAND_SIZE = 12;
    public final int MAX_SUMMON_SIZE = 7;
    public int DRAW_TURN = 1;
    public int INIT_MANA = 1;
    public int INIT_HAND_SIZE = 3;
    private Player player;
    private transient ClientHandler clientHandler;
    private boolean randomDeck;
    boolean isMyTurn; //TODO update isMyTurn in GameState
    boolean isGraphical;
    private transient Attack attackInstance;
    private String gameType ;

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public GamePlayer() {
    }

    public GamePlayer(Player player, boolean friend, ClientHandler clientHandler, GameState gameState) {
        this.gameType = gameState.getType() ;
        this.gameState = gameState ;
        this.clientHandler = clientHandler;
        this.mana = 0;
        hand = new ArrayList<>();
        characters = new ArrayList<>();
        this.player = player;
        this.deck = player.getCurDeck().copy();
        randomDeck = true;
        try {
            if (friend) {
                this.deck.setCards(DeckReader.getInstance().friend());
            } else
                this.deck.setCards(DeckReader.getInstance().enemy());
            randomDeck = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.turn = 1;
        this.summoned = new Summoned(true);
        this.summoned.setPlayer(this);

        //add listenees
        listeners = new ArrayList<>();
        listeners.add(new DrawCard());
        listeners.add(new EndTurn());
        listeners.add(new OpponentPlaysMinion());
        listeners.add(new SpendManaOnMinions());
        listeners.add(new SpendManaOnSpells());
        listeners.add(new SummonMinion());
        setHero(new HeroGame(this.deck.getHero(), this));
        characters.add(this.hero);

    }

    public void init() {
        for (int i = 0; i < INIT_HAND_SIZE; ++i) {
            draw();
        }
        addMana();
    }

    public void start_turn() {
        Listeners.getListener(enemy, "EndTurn").update(null);
        addTurn();
        addMana();
        for (int i = 0; i < DRAW_TURN; ++i)
            draw();
    }

    public void setInfoPassive(InfoPassive infoPassive) {
        this.infoPassive = infoPassive;
        if (infoPassive == InfoPassive.TWICE_DRAW) {
            DRAW_TURN = 2;
        } else if (infoPassive == InfoPassive.MANA_JUMP) {
            INIT_MANA = 1;
            addTurn();
            addMana();
        }
    }

    public Card drawCard() throws Exception {
        if (deck.size() == 0) {
            throw new Exception("Deck is empty");
        } else {
            if (randomDeck) {
                Random rnd = new Random();
                Card ret = deck.get(rnd.nextInt(deck.size()));
                deck.removeCard(ret);
                return ret;
            } else {
                Card ret = deck.get(0);
                deck.removeCard(ret);
                return ret;
            }
        }
    }

    public void draw() {
        try {
            Card card = drawCard();
            if (card instanceof Spell && this.getHero().getHero().getName().equals("Mage")) {
                card.setMana(card.getMana() - 2);
                if (card.getMana() < 0)
                    card.setMana(0);
            }
            if (this.getHero().getHero().getName().equals("Rogue") && (card.getHeroClass().equals("Rogue") || card.getHeroClass().equals("Natural"))) {
                card.setMana(card.getMana() - 2);
                if (card.getMana() < 0)
                    card.setMana(0);
            }
            if (this.getInfoPassive() == InfoPassive.OFF_CARDS) {
                card.setMana(card.getMana() - 1);
                if (card.getMana() < 0)
                    card.setMana(0);
            }
            Listeners.getListener(this, "DrawCard").update(null);
            try {
                addToHand(card);
            } catch (Exception e) {
                //   GameGraphic.getInstance().drawer.setErrorText("your hand is full") ;
            }
        } catch (Exception e) {
            // GameGraphic.getInstance().drawer.setErrorText("your deck is empty") ;
        }
    }

    public void addToHand(Card card) throws Exception {
        if (hand.size() < MAX_HAND_SIZE) {
            hand.add(card);
            updateGameGraphic();
            if (enemy != null)
                enemy.updateGameGraphic();
        } else {
            throw new Exception("Hand is full");
        }
    }

    private void removeFromHand(Card card) {
        hand.remove(card);
        updateGameGraphic();
        enemy.updateGameGraphic();
    }

    public void summonCard(Card card, double dropX) {
        if (summoned.size() < MAX_SUMMON_SIZE || !(card instanceof Minion)) {
            if (mana >= card.getMana()) {
                removeFromHand(card);
                setMana((int) (getMana() - card.getMana()));
                try {
                    Actions actions = Actions.getActions(card.getName(), this);
                    actions.BattleCry();
                } catch (Exception e) {
                    try {
                        QuestAndRewardActions quest = QuestAndRewardActions.getQuest(card.getName(), this);
                        quest.start();
                    } catch (Exception ex) {
                    }
                }
                if (card.getType() == CardType.MINION) {
                    SummonCard summonCard = new SummonCard((Minion) card, this);
                    try {
                        QuestAndRewardActions quest = QuestAndRewardActions.getQuest(card.getName(), this, summonCard);
                        quest.start();
                    } catch (Exception e) {
                    }
                    summonCard.setDropX(dropX);
                    characters.add(summonCard);
                    summonCard.setSummonedTurn(turn);
                    summonCard.setDisable(true);

                    //hunter special power
                    if (getHero().getHero().getName().equals("Hunter")) {
                        summonCard.setRush(true);
                    }

                    if (summonCard.isRush()) {
                        summonCard.setSummonedTurn(turn - 1);
                        summonCard.setDisable(false);
                    }
                    summoned.addCard(summonCard);
                    Listeners.getListener(this, "SpendManaOnMinions").update((int) card.getMana());
                    Listeners.getListener(enemy, "OpponentPlaysMinion").update(summonCard);
                    Listeners.getListener(this, "SummonMinion").update(summonCard);
                } else if (card.getType() == CardType.SPELL) {
                    Listeners.getListener(this, "SpendManaOnSpells").update((int) card.getMana());
                } else if (card.getType() == CardType.WEAPON) {
                    getHero().setWeapon(card.copy());
                }
                updateGameGraphic();
                if (enemy != null)
                    enemy.updateGameGraphic();
            } else {
                //GameGraphic.getInstance().drawer.setErrorText("you don't have enough mana");
            }
        } else {
            //GameGraphic.getInstance().drawer.setErrorText("you can summon 7 card only");
        }
    }

    public void summonCard(int ind, double dropX) {
        ind %= getHand().size();
        Card card = getHand().get(ind);
        summonCard(card, dropX);
    }

    public void summonCard(SummonCard summonCard) {
        if (summoned.size() >= MAX_SUMMON_SIZE)
            return;
        characters.add(summonCard);
        summoned.addCard(summonCard);
        summonCard.setSummonedTurn(turn);
        summonCard.setDisable(true);
        updateGameGraphic();
        enemy.updateGameGraphic();
    }

    public void addMana() {
        this.mana = getTurn();
        if (mana > 10)
            mana = 10;
        setMana(mana);
    }

    public void setMana(int mana) {
        this.mana = mana;
        updateGameGraphic();
        enemy.updateGameGraphic();
    }


    public void updateGameGraphic() {
        if(gameType.equals("MultiPlayer") && !isMyTurn)
            return;
        Gson gson = new Gson();
        clientHandler.getSender().sendMessage(MyJson.create_json(
                "type", "updateGameGraphic",
                "myPlayer", gson.toJson(this),
                "enemyPlayer", gson.toJson(enemy),
                "isMyTurn", isMyTurn,
                "watcher", false

        ).toJSONString());
        if(gameType.equals("OnlineGame")) {
            gameState.updateWatchers();
        }
    }

    public void win() {
        Platform.runLater(()-> {
            player.addCup(+1);

            Stage stage = new Stage();
            Pane pane = new Pane();
            Label label = new Label("YOU WON!");
            pane.getChildren().add(label);
            Scene scene = new Scene(pane, 407, 208);
            stage.setScene(scene);
            stage.show();

            clientHandler.getSender().sendMessage(MyJson.create_json(
                    "type", "menu").toJSONString());
        });
    }

    public void lose() {
        Platform.runLater(()-> {
            player.addCup(-1);

            Stage stage = new Stage();
            Pane pane = new Pane();
            Label label = new Label("YOU LOST!");
            pane.getChildren().add(label);
            Scene scene = new Scene(pane, 407, 208);
            stage.setScene(scene);
            stage.show();
            clientHandler.getSender().sendMessage(MyJson.create_json(
                    "type", "menu").toJSONString());
        }) ;
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isRandomDeck() {
        return randomDeck;
    }

    public void setRandomDeck(boolean randomDeck) {
        this.randomDeck = randomDeck;
    }

    public boolean isMyTurn() {
        return isMyTurn;
    }

    public void setMyTurn(boolean myTurn) {
        isMyTurn = myTurn;
    }

    public boolean isGraphical() {
        return isGraphical;
    }

    public void setGraphical(boolean graphical) {
        isGraphical = graphical;
    }

    public Attack getAttack() {
        return attackInstance;
    }

    public void setAttack(Attack attackInstance) {
        this.attackInstance = attackInstance;
    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    public void setClientHandler(ClientHandler clientHandler) {
        this.clientHandler = clientHandler;
    }

    //functions
    public void addTurn() {
        turn++;
    }


    //setter and getter

    public GamePlayer getEnemy() {
        return enemy;
    }

    public void setEnemy(GamePlayer enemy) {
        this.enemy = enemy;
    }

    public ArrayList<Card> getHand() {
        return hand;
    }

    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }

    public ArrayList<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(ArrayList<Character> characters) {
        this.characters = characters;
    }

    public Summoned getSummoned() {
        return summoned;
    }

    public void setSummoned(Summoned summoned) {
        this.summoned = summoned;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public int getMana() {
        return mana;
    }

    public HeroGame getHero() {
        return hero;
    }

    public void setHero(HeroGame hero) {
        this.hero = hero;
        initHeropwer(hero) ;
    }

    public void initHeropwer(HeroGame hero) {
        String name = hero.getHero().getName();
        if (name.equals("Mage"))
            heroPower = new MageHeroPower((GamePlayer) this);
        else if (name.equals("Rogue"))
            heroPower = new RogueHeroPower((GamePlayer) this);
        else if (name.equals("Warlock"))
            heroPower = new WarlockHeroPower((GamePlayer) this);
        else if (name.equals("Hunter")) {
            new HunterHeroPower((GamePlayer) this).start();
        } else if (name.equals("Priest"))
            heroPower = new PriestHeroPower((GamePlayer) this);
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public InfoPassive getInfoPassive() {
        return infoPassive;
    }


    public HeroPower getHeroPower() {
        return heroPower;
    }

    public void setHeroPower(HeroPower heroPower) {
        this.heroPower = heroPower;
    }

    public ArrayList<Listeners> getListeners() {
        return listeners;
    }

    public void setListeners(ArrayList<Listeners> listeners) {
        this.listeners = listeners;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
}
