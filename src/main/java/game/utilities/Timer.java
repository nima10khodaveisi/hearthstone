package game.utilities;


import game.GameState;
import graphic.game.Drawer;
import graphic.game.GameGraphic;

public class Timer extends Thread {

    private final int notif = 8 ;
    private final int period = 10 ;
    private int turn ;
    private Drawer drawer ;

    public Timer(Drawer drawer) {
        this.drawer = drawer ;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis() ;
        try {
            sleep(notif * 1000) ;
        } catch (InterruptedException e) {
        }
        //TODO add audio to notif
        for(int i = 0 ; i <= notif ; ++i) {
            if(drawer.isEnded())
                return;
            drawer.setTimerText(notif - i) ;
            try {
                sleep(1000) ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        drawer.endTimeTurn();
        /*
        for(int i = 0 ; i <= notif ; ++i) {
            if(GameState.getInstance().getCurPlayer().getTurn() + GameState.getInstance().getOtherPlayer().getTurn() != getTurn())
                return ;
            try {
                GameGraphic.getInstance().drawer.setTimerText(notif - i) ;
                sleep(1000) ;
            } catch (Exception e) {
            }
        }
        GameState.getInstance().start_turn();*/
    }
}
