package game;

import Models.Heros.Hero;
import com.google.gson.Gson;
import game.models.HeroGame;
import game.players.gamePlayer.GamePlayer;
import game.models.Character;
import game.models.SummonCard;
import game.utilities.Timer;
import network.json.MyJson;
import network.server.ClientHandler;
import network.server.game.GameServer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class GameState extends Thread {

    private GameServer gameServer;
    private GamePlayer curPlayer;
    private GamePlayer otherPlayer;
    private GamePlayer player1;
    private GamePlayer player2;
    private Timer timer;
    public String type = "MultiPlayer";
    private int turn = 1;
    private ArrayList<ClientHandler> watchers ;
    private boolean hasEnded ;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GameState(GameServer gameServer, String type, ClientHandler clientHandler1, ClientHandler clientHandler2) {
        this.gameServer = gameServer;
        watchers = new ArrayList<>() ;
        setType(type);
        if (type.equals("MultiPlayer")) {
            player1 = new GamePlayer(clientHandler2.getPlayer(), true, clientHandler1, this);
            player2 = new GamePlayer(clientHandler2.getPlayer(), false, clientHandler1, this);
        } else if (type.equals("OnlineGame")) {
            player1 = new GamePlayer(clientHandler1.getPlayer(), true, clientHandler1, this );
            player2 = new GamePlayer(clientHandler2.getPlayer(), false, clientHandler2, this);
        }
        player2.setGameType(type) ;
        player1.setGameType(type) ;
        player1.setGraphical(false);
        player2.setGraphical(false);
    }

    @Override
    public void run() {
        curPlayer = player1;
        otherPlayer = player2;
        player1.setEnemy(player2);
        player2.setEnemy(player1);
        curPlayer.setMyTurn(true);
        otherPlayer.setMyTurn(false);
        curPlayer.init();
        otherPlayer.init();
        enableCurPlayerSummonCards(true);
        if(type.equals("MultiPlayer")) {
            curPlayer.updateGameGraphic();
        } else {
            curPlayer.updateGameGraphic();
            otherPlayer.updateGameGraphic();
        }

        while(true) {
            if(!curPlayer.getPlayer().isOnline()) {
                win(otherPlayer) ;
            }
            if(!otherPlayer.getPlayer().isOnline()) {
                win(curPlayer) ;
            }
            try {
                sleep(5000) ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private GamePlayer getPlayer(int index) {
        return (index == 0 ? curPlayer : otherPlayer);
    }

    private void setTimer() {
        /*try {
            GameGraphic.getInstance().drawer.setTimerText(60);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    private void swapPlayers() {
        if (curPlayer == player1) {
            curPlayer = player2;
            otherPlayer = player1;
        } else {
            curPlayer = player1;
            otherPlayer = player2;
        }
        curPlayer.setMyTurn(true);
        otherPlayer.setMyTurn(false);
        curPlayer.start_turn();
        enableCurPlayerSummonCards(true);
        enableOtherPlayerCharacters(false);
    }

    public void start_turn() {
        swapPlayers();
        //   setTimer();
    }

    public void attackAction(int attackerId, int defenderId) {
        Character attacker, defender;
        if (attackerId == -1) {
            attacker = curPlayer.getHero();
        } else {
            attacker = curPlayer.getSummoned().getCards().get(attackerId);
        }
        if (defenderId == -1) {
            defender = otherPlayer.getHero();
        } else {
            defender = otherPlayer.getSummoned().getCards().get(defenderId);
        }
        attacker.attack(defender);
        enableCurPlayerSummonCards(true) ;
        enableOtherPlayerCharacters(false) ;
        attacker.setSelected(false);
        if (attacker instanceof HeroGame) {
            ((HeroGame) attacker).setDurability(((HeroGame) attacker).getDurability() - 1);
        }
    }

    public void selectCardAction(String username, String className, int characterId) {
        Character character ;
        GamePlayer gamePlayer = player2 ;
        if(player1.getPlayer().getUsername().equals(username))
            gamePlayer = player1 ;
        if(characterId == -1) {
            character = gamePlayer.getHero() ;
        } else {
            character = gamePlayer.getSummoned().getCards().get(characterId) ;
        }
        try {
            Class c = Class.forName(className) ;
            Method method = c.getMethod("doAction", Character.class) ;
            method.invoke(character) ;
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void summonCard(String username, int ind, double dropX) {
        if(type.equals("MultiPlayer")) {
            curPlayer.summonCard(ind, dropX) ;
            return;
        }
        if(player1.getClientHandler().getPlayer().getUsername().equals(username)) {
            player1.summonCard(ind, dropX) ;
        } else {
            player2.summonCard(ind, dropX) ;
        }
    }

    //actions
    public void enableCurPlayerSummonCards(boolean value) {
        for (SummonCard summonCard : curPlayer.getSummoned().getCards()) {
            if (summonCard.getSummonedTurn() == curPlayer.getTurn()) {
                summonCard.setDisable(true);
                continue;
            }
            summonCard.setDisable(!value);
        }
        curPlayer.getHero().setDisable(false);
        if(type.equals("MultiPlayer")) {
            curPlayer.updateGameGraphic();
        } else {
            player1.updateGameGraphic();
            player2.updateGameGraphic();
        }
    }

    public void enableOtherPlayerCharacters(boolean value) {
        for (Character character : otherPlayer.getCharacters()) {
            character.setDisable(!value);
        }
        if(type.equals("MultiPlayer")) {
            curPlayer.updateGameGraphic();
        } else {
            player1.updateGameGraphic();
            player2.updateGameGraphic();
        }
    }

    public void updateWatchers() {
        for(ClientHandler watcher : watchers) {
            Gson gson = new Gson();
            watcher.getSender().sendMessage(MyJson.create_json(
                    "type", "updateGameGraphic",
                    "myPlayer", gson.toJson(player1),
                    "enemyPlayer", gson.toJson(player2),
                    "isMyTurn", false,
                    "watcher", true

            ).toJSONString());
        }
    }

    public void addWatcher(ClientHandler clientHandler) {
        watchers.add(clientHandler) ;
        Gson gson = new Gson() ;
        clientHandler.getSender().sendMessage(MyJson.create_json(
                "type", "updateGameGraphic",
                "myPlayer", gson.toJson(player1),
                "enemyPlayer", gson.toJson(player2),
                "isMyTurn", false,
                "watcher", true

        ).toJSONString());
    }

    public void win(GamePlayer winner) {
        setHasEnded(true) ;
        if(type.equals("OnlineGame")) {
            winner.win() ;
            GamePlayer loser = player1 ;
            if(loser == winner)
                loser = player2 ;
            loser.lose() ;
            for(ClientHandler clientHandler : watchers) {
                clientHandler.getSender().sendMessage(MyJson.create_json(
                        "type", "menu").toJSONString());
            }
        } else {
            player2.win();
        }
    }

    public GamePlayer getCurPlayer() {
        return curPlayer;
    }

    public GamePlayer getOtherPlayer() {
        return otherPlayer;
    }

    public ArrayList<ClientHandler> getWatchers() {
        return watchers;
    }

    public void setWatchers(ArrayList<ClientHandler> watchers) {
        this.watchers = watchers;
    }

    public boolean isHasEnded() {
        return hasEnded;
    }

    public void setHasEnded(boolean hasEnded) {
        this.hasEnded = hasEnded;
    }
}
