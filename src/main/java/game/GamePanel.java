package game ;

import audio.AudioPlayer;
import chatroom.server.ChatServer;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import network.json.MyJson;
import network.server.ClientHandler;
import network.server.Server;
import network.server.ServerMain;
import network.server.game.GameServer;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Timer;
import java.util.TimerTask;

public class GamePanel extends Thread {

    private transient GameState gameState ;
    private transient GameServer gameServer ;
    private transient String type ;
    private String player1Username ;
    private String player2Username ;
    private int serverPort ;
    private int chatPort ;

    public GamePanel(String type, ClientHandler clientHandler0, ClientHandler clientHandler1) {
        this.type = type ;
        gameServer = new GameServer(type, clientHandler0, clientHandler1, this) ;

        if(type.equals("OnlineGame")) {
            //start chat room
            ChatServer chatServer = new ChatServer(true, 0) ;
            this.serverPort =  gameServer.getServerPort() ;
            this.chatPort = chatServer.getServerPort() ;
            chatServer.start();
            clientHandler0.getSender().sendMessage(MyJson.create_json(
                    "type", "chatroom",
                    "query", "join",
                    "serverIP", ServerMain.serverIP,
                    "port", chatServer.getServerPort()).toJSONString()) ;

            clientHandler1.getSender().sendMessage(MyJson.create_json(
                    "type", "chatroom",
                    "query", "join",
                    "serverIP", ServerMain.serverIP,
                    "port", chatServer.getServerPort()).toJSONString()) ;
        } else {

        }

    }

    @Override
    public void run() {
        gameServer.start();
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
        player1Username = gameState.getCurPlayer().getPlayer().getUsername();
        player2Username = gameState.getOtherPlayer().getPlayer().getUsername() ;
    }

    public GameServer getGameServer() {
        return gameServer;
    }

    public void setGameServer(GameServer gameServer) {
        this.gameServer = gameServer;
    }

    public String getPlayer1Username() {
        return player1Username;
    }

    public void setPlayer1Username(String player1Username) {
        this.player1Username = player1Username;
    }

    public String getPlayer2Username() {
        return player2Username;
    }

    public void setPlayer2Username(String player2Username) {
        this.player2Username = player2Username;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getChatPort() {
        return chatPort;
    }

    public void setChatPort(int chatPort) {
        this.chatPort = chatPort;
    }
}
