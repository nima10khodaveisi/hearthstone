package chatroom.server;

import Player.Player;
import World.World;
import chatroom.client.group.Group;
import chatroom.client.group.Message;
import com.google.gson.Gson;
import network.Handleable;
import network.client.Client;
import network.json.MyJson;
import network.server.ClientHandler;
import network.server.ServerMain;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ChatServer extends Thread implements Handleable {
    private ServerSocket serverSocket;
    private ArrayList<ClientHandler> clientHandlers;
    private int serverPort;
    private boolean isGame;
    private Group group;

    public ChatServer(boolean isGame, int port) {
        clientHandlers = new ArrayList<>();
        try {
            if (port == 0) {
                Group group = new Group(true);
                this.group = group;
            }
            serverSocket = new ServerSocket(port);
            serverPort = serverSocket.getLocalPort();
            group.setServerPort(serverPort);
            this.isGame = isGame;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ChatServer(Group group, boolean isGame) {
        this.group = group ;
        clientHandlers = new ArrayList<>();
        try {
            int port = group.getServerPort() ;
            serverSocket = new ServerSocket(port);
            serverPort = serverSocket.getLocalPort();
            group.setServerPort(serverPort);
            this.isGame = isGame;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void join(ClientHandler clientHandler) {
        if (clientHandler.getPlayer() != null)
            group.addUser(clientHandler.getPlayer().getUsername());
        clientHandler.getSender().sendMessage(MyJson.create_json(
                "type", "chatroom",
                "query", "join",
                "serverIP", ServerMain.serverIP,
                "port", serverPort
                ).toJSONString()
        );
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();
                ClientHandler clientHandler = new ClientHandler(this, socket);
                clientHandler.start();
                clientHandlers.add(clientHandler);
                //Thread.sleep(200);
                Gson gson = new Gson();
                sendMessage(clientHandler, MyJson.create_json(
                        "type", "chatroom",
                        "query", "update",
                        "group", gson.toJson(group)
                ).toJSONString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleMessage(ClientHandler clientHandler, String message) {
        System.out.println("chat server handle message :\n" + message);
        try {
            JSONObject json = (JSONObject) new JSONParser().parse(message);
            Method method = this.getClass().getMethod((String) json.get("type"), ClientHandler.class, JSONObject.class);
            method.invoke(this, clientHandler, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("===================================");
    }

    @Override
    public void sendMessage(ClientHandler clientHandler, String message) {
        System.out.println("chat server is sending message\n" + message);
        clientHandler.getSender().sendMessage(message);
        System.out.println("++++++++++++++++++++++++++++++++");
    }

    @Override
    public void sendMessage(ClientHandler clientHandler, JSONObject jsonObject) {
        sendMessage(clientHandler, jsonObject.toJSONString());
    }

    //actions

    public void joined(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        group.addUser(username);
    }

    public void addMessage(ClientHandler clientHandler, JSONObject jsonObject) {
        String username = (String) jsonObject.get("username");
        String message = (String) jsonObject.get("message");
        group.addMessage(new Message(username, message));

        Gson gson = new Gson();

        update();
    }

    public void addUser(ClientHandler clientHandler, JSONObject jsonObject) {
        String myUsername = (String) jsonObject.get("myUsername");
        String user = (String) jsonObject.get("user");
        Player myPlayer = World.getWorld().getPlayerByUsername(myUsername);
        if (myPlayer.isFriend(user)) {
            group.addUser(user);
            Player otherPlayer = World.getWorld().getPlayerByUsername(user);
            otherPlayer.addGroup(group);
            update();
        }
    }

    public void update() {
        for (ClientHandler clientHandler : clientHandlers) {
            Gson gson = new Gson();
            clientHandler.getSender().sendMessage(MyJson.create_json(
                    "type", "chatroom",
                    "query", "update",
                    "group", gson.toJson(group)
            ).toJSONString());
        }
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }
}
