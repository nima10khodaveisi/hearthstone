package chatroom.client.group;

import Player.Player;
import World.World;

import java.util.ArrayList;

public class Group {
    public static int lastPort = 9127;
    private String name ;
    private ArrayList<String> users ;
    private ArrayList<Message> messages ;
    private int serverPort = -1 ;
    private boolean isGame = false ;


    public Group(String name, String player) {
        this.name = name ;
        users = new ArrayList<>() ;
        messages = new ArrayList<>() ;
        addUser(player) ;
        setServerPort();
    }

    public Group(String name) {
        this.name = name ;
        users = new ArrayList<>() ;
        messages = new ArrayList<>() ;
        setServerPort();
    }

    public Group(boolean isGame) {
        this.isGame = isGame;
        users = new ArrayList<>() ;
        messages = new ArrayList<>() ;
        if(!isGame)
            setServerPort() ;
    }

    public Group() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<String> users) {
        this.users = users;
        updateAll();
    }

    public void addUser(String player) {
        users.add(player) ;
        updateAll();
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
        updateAll();
    }

    public void addMessage(Message message) {
        messages.add(message) ;
        updateAll();
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort() {
        if(!isGame) {
            serverPort = Group.lastPort++;
        }
        updateAll();
    }

    public void setServerPort(int serverPort) { this.serverPort = serverPort ;}

    public void updateAll() {
        if(isGame) return;
        for(String user : users) {
            Player player = World.getWorld().getPlayerByUsername(user) ;
            player.updateJsonFile();
        }
    }
}
