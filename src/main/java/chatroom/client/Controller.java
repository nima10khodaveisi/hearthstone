package chatroom.client;

import chatroom.client.group.Group;
import chatroom.client.group.Message;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import network.client.Client;
import network.json.MyJson;

import java.util.ArrayList;

public class Controller {

    private Client client ;
    private boolean isGame ;
    private Group group ;

    @FXML
    private ScrollPane chatScroll ;

    @FXML
    private ScrollPane usersScroll ;

    @FXML
    private TextField usernameToAdd ;

    @FXML
    private Button friendReq ;

    @FXML
    private Button addUser ;

    @FXML
    private TextField messageField ;

    @FXML
    private Button sendMessage ;


    private VBox chatBox = new VBox(10) ;

    private VBox usersBox = new VBox(5) ;


    public void init() {
        if(isGame) {
            addUser.setVisible(false) ;
            usernameToAdd.setVisible(false) ;
        } else {
            friendReq.setVisible(false) ;
        }

        ArrayList<Message> messages = group.getMessages() ;
        ArrayList<String> users = group.getUsers() ;

        //chat
        chatScroll.setContent(chatBox) ;
        for(Message message : messages) {
            Label label = new Label(message.getSender() + ":  " + message.getMessage()) ;
            chatBox.getChildren().add(label) ;
        }

        //users
        usersScroll.setContent(usersBox) ;
        for(String username : users) {
            Label label = new Label(username) ;
            usersBox.getChildren().add(label) ;
        }

        //send message
        sendMessage.setOnMouseClicked(event -> {
            String message = messageField.getText() ;
            if(message != null && !message.isEmpty()) {
                client.chatSendMessage(MyJson.create_json(
                        "type", "addMessage",
                        "username", client.getUsername(),
                        "message", message
                )) ;
            }
        }) ;

        //add user to group
        if(!isGame) {
            addUser.setOnMouseClicked(event -> {
                String username = usernameToAdd.getText() ;
                if(username != null && !username.isEmpty()) {
                    client.chatSendMessage(MyJson.create_json(
                            "type", "addUser",
                            "myUsername", client.getUsername(),
                            "user", username).toJSONString()) ;
                }
            }) ;
        }
    }




    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public boolean isGame() {
        return isGame;
    }

    public void setGame(boolean game) {
        isGame = game;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
