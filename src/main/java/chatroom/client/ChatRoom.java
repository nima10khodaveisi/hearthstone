package chatroom.client;

import chatroom.client.group.Group;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import network.client.Client;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class ChatRoom {
    public static void show(Client client, boolean isGame, Group group) {
        Stage stage = client.getChatStage() ;
        URL url = null;
        try {
            url = Paths.get("./src/main/java/chatroom/client/chatroom.fxml").toUri().toURL();
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            Parent root = fxmlLoader.load();
            Controller controller = fxmlLoader.getController();
            controller.setClient(client);
            controller.setGame(isGame) ;
            controller.setGroup(group) ;
            controller.init();
            Scene scene = new Scene(root, 1280, 720);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
